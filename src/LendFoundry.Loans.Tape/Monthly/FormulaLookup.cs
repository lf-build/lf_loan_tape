﻿using LendFoundry.Foundation.Date;
using LendFoundry.Loans.Tape.Configuration;
using LendFoundry.Loans.Tape.Monthly.Formulas;
using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Tape.Monthly
{
    public class FormulaLookup
    {
        private IDictionary<Type, object> Formulas = new Dictionary<Type, object>();

        public FormulaLookup(ITenantTime tenantTime, TapeServiceConfiguration configuration)
        {
            AddFormula<IAverageDailyBalanceFormula>(new AverageDailyBalanceFormula(tenantTime));
            AddFormula<IBeginningPrincipalBalanceFormula>(new BeginningPrincipalBalanceFormula(tenantTime));
            AddFormula<IChargeOffFormula>(new ChargeOffFormula(configuration));
            AddFormula<IEndingPrincipalBalanceFormula>(new EndingPrincipalBalanceFormula(tenantTime));
            AddFormula<IFeesAssessedFormula>(new FeesAssessedFormula(tenantTime));
            AddFormula<IInterestAccruedFormula>(new InterestAccruedFormula(Get<IAverageDailyBalanceFormula>()));
            AddFormula<ILoanAgeFormula>(new LoanAgeFormula());
            AddFormula<IInterestPaidFormula>(new InterestPaidFormula(tenantTime));
            AddFormula<INetInterestAccruedFormula>(new NetInterestAccruedFormula(Get<IInterestAccruedFormula>(), Get<IInterestPaidFormula>(), tenantTime));
            AddFormula<IPastDueAmountFormula>(new PastDueAmountFormula());
            AddFormula<IPrincipalPaidFormula>(new PrincipalPaidFormula(tenantTime));
            AddFormula<ITotalBorrowerPaymentFormula>(new TotalBorrowerPaymentFormula(tenantTime));
        }

        public void AddFormula<T>(T formula)
        {
            Formulas.Add(typeof(T), formula);
        }

        public T Get<T>()
        {
            var key = typeof(T);

            if (Formulas.ContainsKey(key))
                return (T) Formulas[key];

            throw new ArgumentException($"Formula not found: {key}");
        }
    }
}
