﻿using System;

namespace LendFoundry.Loans.Tape.Monthly
{
    public interface ILoanTapeFormula
    {
        double FundedAmount { get; }
        double OriginationFeePercent { get; }
        double OriginationFeeAmount { get; }
        DateTimeOffset? LastPaymentDueDate { get; }
        double FeesThisMonth { get; }
        double CumulativeInterestPaid { get; }
        DateTimeOffset? SchedulePaymentDue { get; }
        DateTimeOffset? NextPaymentDueDate { get; }
        double MonthlyPayment { get; }
        DateTimeOffset? LastPaymentReceivedDate { get; }
        string MonthOfServicingReport { get; }
        double PrincipalPaidDuringThePeriod { get; }
        double InterestPaidDuringThePeriod { get; }
        double FeesPaidDuringThePeriod { get; }
        double SchedulePaymentDueInPeriod { get; }
        int DaysDelinquent { get; }
        DateTimeOffset? MaturityDate { get; }
    }
}
