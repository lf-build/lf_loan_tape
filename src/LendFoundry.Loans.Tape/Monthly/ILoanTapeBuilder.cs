﻿using System;

namespace LendFoundry.Loans.Tape.Monthly
{
    public interface ILoanTapeBuilder
    {
        ILoanTape BuildTape(string loanReferenceNumber, DateTimeOffset referenceDate, ILoanTape previousTape);
    }
}
