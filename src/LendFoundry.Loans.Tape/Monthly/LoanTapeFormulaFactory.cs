﻿using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.Loans.Tape.Monthly
{
    public class LoanTapeFormulaFactory : ILoanTapeFormulaFactory
    {
        public ILoanTapeFormula Create(ILoanDetails loanDetails, DateTimeOffset referenceDate, LoanTapeConfiguration configuration, ITenantTime tenantTime)
        {
            return new LoanTapeFormula
            (
                loanDetails,
                referenceDate,
                configuration,
                tenantTime
            );
        }
    }
}
