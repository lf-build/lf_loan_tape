﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Tape.Configuration;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Tape.Monthly
{
    public class LoanTapeEventHandler : EventHandler
    {
        public LoanTapeEventHandler
        (
            TenantInfo tenant,
            TapeServiceConfiguration serviceConfiguration,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            IConfigurationServiceFactory configurationServiceFactory,
            ILoanTapeServiceFactory loanTapeServiceFactory,
            ILoggerFactory loggerFactory) : base(tenant.Id, loggerFactory.CreateLogger())
        {
            Tenant = tenant;
            Events = serviceConfiguration.MonthlyLoanTape.Events ?? new EventMapping[] { };
            Logger = loggerFactory.CreateLogger().Scope(this);
            TokenHandler = tokenHandler;
            TokenReader = CreateTokenReader(tokenHandler, tenant.Id);
            ConfigurationServiceFactory = configurationServiceFactory;
            LoanTapeService = loanTapeServiceFactory.Create(TokenReader);
            tenantTime = tenantTimeFactory.Create(ConfigurationServiceFactory, TokenReader);

            if(!Events.Any())
            {
                Logger.Warn("No events will be handled: configuration does not specify a list of events to listen for");
            }
        }
        
        private TenantInfo Tenant { get; }
        private EventMapping[] Events { get; }
        private ITokenHandler TokenHandler { get; }
        private ITokenReader TokenReader { get; }
        private IConfigurationServiceFactory ConfigurationServiceFactory { get; }
        private ILoanTapeService LoanTapeService { get; }
        private ITenantTime tenantTime { get; }
        private ILogger Logger { get; }

        public override IEnumerable<string> AcceptedEvents => Events.Select(e => e.Name);
        
        protected override void Handle(EventInfo eventInfo)
        {
            var @event = Events.Single(e => e.Name == eventInfo.Name);
            Handle(@event, eventInfo);
        }

        private void Handle(EventMapping @event, EventInfo eventInfo)
        {
            var referenceDate = GetReferenceDate(@event, eventInfo, tenantTime);
            var loanReferenceNumber = @event.ReferenceNumber.FormatWith(eventInfo);
            var tapeLogger = Logger.ForLoanTape(loanReferenceNumber, referenceDate);

            tapeLogger.Info("Handling event {eventName}", new { eventName = eventInfo.Name });

            try
            {
                LoanTapeService.GenerateTape(loanReferenceNumber, referenceDate);
            }
            catch (Exception exception)
            {
                tapeLogger.Error("Error while generating tape", exception);
            }
        }

        private static DateTimeOffset GetReferenceDate(EventMapping @event, EventInfo eventInfo, ITenantTime tenantTime)
        {
            var referenceDateValue = @event.ReferenceDate.FormatWith(eventInfo);
            return !string.IsNullOrWhiteSpace(referenceDateValue)
                ? tenantTime.FromDate(DateTime.Parse(referenceDateValue))
                : tenantTime.Today;
        }

        private static ITokenReader CreateTokenReader(ITokenHandler tokenHandler, string tenantId)
        {
            var token = tokenHandler.Issue(tenantId, Settings.ServiceName);
            return new StaticTokenReader(token.Value);
        }
    }
}