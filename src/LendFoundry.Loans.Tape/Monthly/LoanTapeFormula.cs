﻿using LendFoundry.Foundation.Date;
using System;
using System.Globalization;
using System.Linq;

namespace LendFoundry.Loans.Tape.Monthly
{
    public class LoanTapeFormula : ILoanTapeFormula
    {
        public LoanTapeFormula(
            ILoanDetails loanDetails,
            DateTimeOffset referenceDate,
            LoanTapeConfiguration configuration,
            ITenantTime tenantTime
        )
        {
            ReferenceDate = referenceDate;
            LoanDetails = loanDetails;
            Configuration = configuration;
            TenantTime = tenantTime;
        }

        private LoanTapeConfiguration Configuration { get; }
        private DateTimeOffset ReferenceDate { get; }
        private ILoanDetails LoanDetails { get; }
        private ILoan Loan { get { return LoanDetails.LoanInfo.Loan; } }
        private IPaymentSchedule PaymentSchedule { get { return LoanDetails.PaymentSchedule; } }
        private ITenantTime TenantTime { get; }

        private DateTimeOffset FirstDayReferenceDate => ReferenceDate.AddDays(-(ReferenceDate.Day - 1));
        private DateTimeOffset LastDayReferenceDate => FirstDayReferenceDate.AddMonths(1).AddDays(-1);
        public double FundedAmount => Loan.Terms.LoanAmount - Loan.Terms.Fees.Sum(x => x.Amount);

        public double OriginationFeePercent
        {
            // Find the fee with `Code = "Origination Fee"` in `Loan.Terms.Fees` and determine both 
            // the percent and the amount by using the `IFee.To()` method
            // To find the origination fee in the loan terms, instead of looking for a fee with `Code = "Origination Fee"`, 
            // look for a fee with `Code = "142"
            get
            {
                //todo - move fixed originationFeeCode = "142" to configuration

                // By default try to get information from transactions if not resolves get it from fee.    
                var originationFeeCode = "142";
                var transaction = LoanDetails.Transactions.FirstOrDefault(t => t.Code == originationFeeCode);
                if (transaction != null)
                {
                    var amountCalc = transaction.Amount / Loan.Terms.LoanAmount;
                    var percentValue = Math.Round(amountCalc * 100.0, 2, MidpointRounding.AwayFromZero);
                    return percentValue;
                }

                var fee = Loan.Terms.Fees.FirstOrDefault(f => f.Code == originationFeeCode);
                return fee != null ? fee.To(FeeType.Percent, Loan.Terms.LoanAmount).Amount : 0;
            }
        }

        public double OriginationFeeAmount
        {
            // Find the fee with `Code = "Origination Fee"` in `Loan.Terms.Fees` and determine both 
            // the percent and the amount by using the `IFee.To()` method
            // To find the origination fee in the loan terms, instead of looking for a fee with `Code = "Origination Fee"`, 
            // look for a fee with `Code = "142"
            get
            {
                // By default try to get information from transactions if not resolves get it from fee.
                var originationFeeCode = "142";
                var transaction = LoanDetails.Transactions.FirstOrDefault(t => t.Code == originationFeeCode);
                if (transaction != null)
                    return transaction.Amount;

                var fee = Loan.Terms.Fees.FirstOrDefault(f => f.Code == originationFeeCode);
                return fee != null ? fee.To(FeeType.Fixed, Loan.Terms.LoanAmount).Amount : 0;
            }
        }

        public double FeesThisMonth
        {
            // It's the sum of all transactions that occurred in the current month where 
            // the transaction code is one of the following:
            get
            {
                var formulaResult = LoanDetails.Transactions
                                        .Where(tx =>
                                            Configuration.FeeTransactions.Any(f => f.Code == tx.Code) &&
                                            tx.Date.ToString("yyyy-MM") == ReferenceDate.ToString("yyyy-MM"))
                                        .Sum(tx => tx.Amount);
                return formulaResult;
            }
        }

        public DateTimeOffset? LastPaymentDueDate
        {
            // It's the due date of the latest installment relative to the last day of the current month
            get
            {
                var latestInstallment = PaymentSchedule.Installments
                                         .OrderBy(x => x.DueDate)
                                         .Where(x => x.DueDate <= LastDayReferenceDate)
                                         .LastOrDefault();
                return latestInstallment != null ? latestInstallment.DueDate : null;
            }
        }

        public double CumulativeInterestPaid
        {
            // Sum amout of all interest transactions
            get
            {
                //todo: validate if manoj is right in the setence above
                // Go to installments and get all interest paid to lastDayOFCurrentMonth

                // Where code 124 is "Principal and Interest"
                var totalInterestPaid = LoanDetails.Transactions
                                        .Where(tx => tx.Code == "124")
                                        .Sum(tx => tx.Amount);
                return totalInterestPaid;
            }
        }

        public DateTimeOffset? SchedulePaymentDue
        {
            // Is the due date of the scheduled payment for the month            
            get
            {
                var scheduledPayment = PaymentSchedule.Installments
                                        .FirstOrDefault(i => i.DueDate.Value.ToString("yyyy-MM") == ReferenceDate.ToString("yyyy-MM"));
                return scheduledPayment?.DueDate.Value;
            }
        }

        public DateTimeOffset? NextPaymentDueDate
        {
            // Due date of the next installment relative to the last day of the current month.
            get
            {
                var nextInstallment = PaymentSchedule.Installments
                                         .OrderBy(x => x.DueDate)
                                         .Where(x => x.DueDate > LastDayReferenceDate)
                                         .FirstOrDefault();
                return nextInstallment != null ? nextInstallment.DueDate : null;
            }
        }

        public DateTimeOffset? LastPaymentReceivedDate
        {
            // It's the `PaymentDate` of the last paid installment (status=Completed) where the 
            // `PaymentDate` is less than or equal to the last day of the current month.
            get
            {
                var lastPaidInstallment = PaymentSchedule.Installments
                                            .OrderBy(i => i.PaymentDate)
                                            .Where(i =>
                                               i.PaymentDate <= LastDayReferenceDate &&
                                               i.Status == InstallmentStatus.Completed)
                                            .LastOrDefault();
                return lastPaidInstallment != null ? lastPaidInstallment.PaymentDate : null;
            }
        }

        public double MonthlyPayment => Loan.Terms.PaymentAmount;
        public string MonthOfServicingReport => CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ReferenceDate.ToString("MMM-yy"));

        [Obsolete("Not being used")]
        public double PrincipalPaidDuringThePeriod
        {
            // Sum of the `Principal` of all ​*completed*​ installments (w/ status = `Status = Completed`) 
            // due in the current month
            get
            {
                var installments = PaymentSchedule.Installments
                                    .Where(i =>
                                       i.PaymentDate <= LastDayReferenceDate &&
                                       i.Status == InstallmentStatus.Completed);
                return installments != null ? installments.Sum(x => x.Principal) : 0.0;
            }
        }

        [Obsolete("Not being used")]
        public double InterestPaidDuringThePeriod
        {
            // Sum of the `Interest` of all ​*completed*​ installments (w/ status = `Status = Completed`) 
            // due in the current month
            get
            {
                var installments = PaymentSchedule.Installments
                                    .OrderBy(i => i.PaymentDate)
                                    .Where(i =>
                                       i.PaymentDate <= LastDayReferenceDate &&
                                       i.Status == InstallmentStatus.Completed);
                return installments != null ? installments.Sum(x => x.Interest) : 0.0;
            }
        }

        public double FeesPaidDuringThePeriod
        {
            // Sum of transactions (`Amount`) w/ `Code` = (fees) during the current month
            get
            {
                var feesPaid = LoanDetails.Transactions
                                        .Where(tx =>
                                            Configuration.FeeTransactions.Any(f => f.Code == tx.Code) &&
                                            tx.Date <= LastDayReferenceDate)
                                        .Sum(tx => tx.Amount);
                return feesPaid;
            }
        }

        public double SchedulePaymentDueInPeriod
        {
            // Sum of payment amounts from installments where `Type = Scheduled` in the current month
            get
            {
                var installments = PaymentSchedule.Installments
                                        .Where(i =>
                                            i.DueDate <= LastDayReferenceDate &&
                                            i.Status == InstallmentStatus.Scheduled);
                return installments != null ? installments.Sum(x => x.PaymentAmount) : 0.0;
            }
        }

        public int DaysDelinquent => LoanDetails.LoanInfo.Summary.DaysPastDue;
        public DateTimeOffset? MaturityDate => LoanDetails.PaymentSchedule.Installments.LastOrDefault()?.DueDate;
    }
}
