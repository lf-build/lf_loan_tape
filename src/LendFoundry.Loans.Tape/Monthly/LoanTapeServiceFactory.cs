﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Tape.Monthly
{
    public class LoanTapeServiceFactory : ILoanTapeServiceFactory
    {
        public LoanTapeServiceFactory(
            ILoanTapeBuilderFactory loanTapeBuilderFactory,
            ILoanTapeRepositoryFactory loanTapeRepositoryFactory,
            IConfigurationServiceFactory configurationServiceFactory,
            ITenantTimeFactory tenantTimeFactory,
            ILoggerFactory loggerFactory)
        {
            LoanTapeBuilderFactory = loanTapeBuilderFactory;
            LoanTapeRepositoryFactory = loanTapeRepositoryFactory;
            ConfigurationServiceFactory = configurationServiceFactory;
            TenantTimeFactory = tenantTimeFactory;
            LoggerFactory = loggerFactory;
        }

        private ILoanTapeBuilderFactory LoanTapeBuilderFactory { get; }
        private ILoanTapeRepositoryFactory LoanTapeRepositoryFactory { get; }
        private IConfigurationServiceFactory ConfigurationServiceFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ILoggerFactory LoggerFactory { get; }

        public ILoanTapeService Create(ITokenReader reader)
        {
            return new LoanTapeService
            (
                LoanTapeBuilderFactory.Create(reader),
                LoanTapeRepositoryFactory.Create(reader),
                TenantTimeFactory.Create(ConfigurationServiceFactory, reader),
                LoggerFactory.CreateLogger()
            );
        }
    }
}
