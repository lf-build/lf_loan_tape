﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Tape.Configuration;
using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;
using System;

namespace LendFoundry.Loans.Tape.Monthly
{
    public class LoanTapeBuilderFactory : ILoanTapeBuilderFactory
    {
        public LoanTapeBuilderFactory(
            IConfigurationServiceFactory configurationFactory,
            IConfigurationServiceFactory<TapeServiceConfiguration> serviceConfigurationFactory,
            ITenantTimeFactory tenantTimeFactory,
            ILoanTapeRepositoryFactory loanTapeRepositoryFactory,
            ILoanServiceFactory loanServiceFactory,
            ILoggerFactory loggerFactory)
        {
            ConfigurationFactory = configurationFactory;
            ServiceConfigurationFactory = serviceConfigurationFactory;
            TenantTimeFactory = tenantTimeFactory;
            LoanTapeRepositoryFactory = loanTapeRepositoryFactory;
            LoanServiceFactory = loanServiceFactory;
            LoggerFactory = loggerFactory;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IConfigurationServiceFactory<TapeServiceConfiguration> ServiceConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private ILoanTapeRepositoryFactory LoanTapeRepositoryFactory { get; }
        private ILoanServiceFactory LoanServiceFactory { get; }
        private ILoggerFactory LoggerFactory { get; }

        public ILoanTapeBuilder Create(ITokenReader reader)
        {
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var loanTapeFormulaFactory = new LoanTapeFormulaFactory();
            var configuration = ServiceConfigurationFactory.Create(reader).Get();
            var investorConfiguration = ConfigurationFactory.Create<InvestorConfiguration>("investors", reader).Get();

            return new LoanTapeBuilder
            (
                new FormulaLookup(tenantTime, configuration),
                new LoanTapeFormulaFactory(),
                LoanTapeRepositoryFactory.Create(reader),
                LoanServiceFactory.Create(reader),
                investorConfiguration,
                configuration.MonthlyLoanTape,
                tenantTime,
                LoggerFactory.CreateLogger()
            );
        }
    }
}
