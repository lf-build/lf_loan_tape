﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Tape.Monthly
{
    public interface ILoanTapeBuilderFactory
    {
        ILoanTapeBuilder Create(ITokenReader reader);
    }
}
