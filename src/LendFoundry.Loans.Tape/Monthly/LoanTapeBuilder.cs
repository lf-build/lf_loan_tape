﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Tape.Configuration;
using LendFoundry.Loans.Tape.Monthly.Formulas;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Tape.Monthly
{
    public class LoanTapeBuilder : ILoanTapeBuilder
    {
        public LoanTapeBuilder(
            FormulaLookup formulas,
            ILoanTapeFormulaFactory loanTapeFormulaFactory,
            ILoanTapeRepository repository,
            ILoanService loanService,
            InvestorConfiguration investorConfiguration,
            LoanTapeConfiguration tapeConfiguration,
            ITenantTime tenantTime,
            ILogger logger)
        {
            LoanTapeFormulaFactory = loanTapeFormulaFactory;
            LoanService = loanService;
            Formulas = formulas;
            InvestorConfiguration = investorConfiguration;
            TapeConfiguration = tapeConfiguration;
            TenantTime = tenantTime;
            Logger = logger.Scope(this);
        }

        private ILoanTapeFormulaFactory LoanTapeFormulaFactory { get; }
        private FormulaLookup Formulas { get; }
        private LoanTapeConfiguration TapeConfiguration { get; }
        private InvestorConfiguration InvestorConfiguration { get; }
        private ILoanService LoanService { get; }
        private ITenantTime TenantTime { get; }
        private ILogger Logger { get; }

        public ILoanTape BuildTape(string loanReferenceNumber, DateTimeOffset referenceDate, ILoanTape previousTape)
        {
            var tapeLogger = Logger.ForLoanTape(loanReferenceNumber, referenceDate);
            var loanDetails = LoanService.GetLoanDetails(loanReferenceNumber);
            var info = loanDetails.LoanInfo;
            var terms = loanDetails.LoanInfo.Loan.Terms;
            var schedule = loanDetails.PaymentSchedule;
            var transactions = loanDetails.Transactions;
            var borrower = GetBorrower(loanReferenceNumber, referenceDate, loanDetails, tapeLogger);
            var address = borrower.Addresses.FirstOrDefault(a => a.IsPrimary);
            var score = GetScore(loanReferenceNumber, referenceDate, info, tapeLogger);
            var investor = GetInvestor(loanReferenceNumber, referenceDate, info, tapeLogger);
            var chargeOff = Formulas.Get<IChargeOffFormula>().Apply(transactions);
            var tapeFormula = LoanTapeFormulaFactory.Create(loanDetails, referenceDate, TapeConfiguration, TenantTime);

            var tape = new LoanTape
            {
                LoanReferenceNumber = loanReferenceNumber,
                CreationDate = new TimeBucket(referenceDate)
            };

            tape.OriginationDate = info.Loan.Terms.OriginationDate;
            tape.FundingDate = info.Loan.Terms.FundedDate;
            tape.MaturityDate = tapeFormula.MaturityDate;
            tape.EntityId = investor.Id;
            tape.EntityName = InvestorConfiguration.FindById(investor.Id)?.Name;
            tape.PurchaseDate = investor.LoanPurchaseDate;
            tape.BorrowerLastName = borrower.LastName;
            tape.BorrowerFirstName = borrower.FirstName;
            tape.State = address.State;
            tape.ZipCode = address.ZipCode;
            tape.OriginationFicoScore = score?.InitialScore;
            tape.UpdatedFicoScore = score?.UpdatedScore;
            tape.OriginationFeePercent = tapeFormula.OriginationFeePercent;
            tape.OriginationFeeAmount = tapeFormula.OriginationFeeAmount;
            tape.LoanAmount = info.Loan.Terms.LoanAmount;
            tape.FundedAmount = tapeFormula.FundedAmount;
            tape.InterestRate = info.Loan.Terms.Rate;
            tape.Term = info.Loan.Terms.Term;
            tape.MonthlyPayment = tapeFormula.MonthlyPayment;
            tape.RiskGrade = info.Loan.Grade;
            tape.DTI = info.Loan.PostCloseDti;
            tape.MonthlyIncome = info.Loan.MonthlyIncome;
            tape.LoanPurpose = info.Loan.Purpose;
            tape.HomeOwnership = info.Loan.HomeOwnership;
            tape.CampaignCode = info.Loan.CampaignCode;
            tape.Principal = Formulas.Get<IPrincipalPaidFormula>().Apply(referenceDate, terms, schedule);
            tape.Interest = Formulas.Get<IInterestPaidFormula>().Apply(referenceDate, terms, schedule);
            tape.FeesThisMonth = tapeFormula.FeesThisMonth;
            tape.SchedulePaymentDue = tapeFormula.SchedulePaymentDue;
            tape.LastPaymentReceivedDate = tapeFormula.LastPaymentReceivedDate;
            tape.LoanStatus = info.Loan.Status.Code;
            tape.DaysPastDue = info.Summary.DaysPastDue;
            tape.LoanAge = Formulas.Get<ILoanAgeFormula>().GetLoanAge(referenceDate, terms);
            tape.LastPaymentDueDate = tapeFormula.LastPaymentDueDate;
            tape.NextPaymentDueDate = tapeFormula.NextPaymentDueDate;
            tape.PastDueAmount = Formulas.Get<IPastDueAmountFormula>().Apply(loanDetails.LoanInfo.Summary);
            tape.AverageDailyBalance = Formulas.Get<IAverageDailyBalanceFormula>().Apply(referenceDate, terms, schedule, previousTape);
            tape.SimpleInterestAccrued = Formulas.Get<IInterestAccruedFormula>().Apply(referenceDate, terms, schedule);
            tape.SimpleInterestAccruedLastMonth = Formulas.Get<IInterestAccruedFormula>().Apply(referenceDate.AddMonths(-1), terms, schedule);
            tape.CumulativeInterestPaid = tapeFormula.CumulativeInterestPaid;
            tape.MonthOfServicingReport = tapeFormula.MonthOfServicingReport;
            tape.BeginningPrincipalBalance = Formulas.Get<IBeginningPrincipalBalanceFormula>().Apply(referenceDate, terms, schedule, previousTape);
            tape.EndingPrincipalBalance = Formulas.Get<IEndingPrincipalBalanceFormula>().Apply(referenceDate, terms, schedule);
            tape.PrincipalPaidDuringThePeriod = tapeFormula.PrincipalPaidDuringThePeriod;
            tape.InterestPaidDuringThePeriod = tapeFormula.InterestPaidDuringThePeriod;
            tape.FeesPaidDuringThePeriod = tapeFormula.FeesPaidDuringThePeriod;
            tape.SchedulePaymentDueInPeriod = tapeFormula.SchedulePaymentDueInPeriod;
            tape.DateOfLastPaymentReceived = tapeFormula.LastPaymentReceivedDate;
            tape.DaysDelinquent = tapeFormula.DaysDelinquent;
            tape.TotalBorrowerPayment = Formulas.Get<ITotalBorrowerPaymentFormula>().Apply(schedule, referenceDate);
            tape.NetInterestAccrued = Formulas.Get<INetInterestAccruedFormula>().Apply(referenceDate, terms, schedule);
            tape.LateFeeAssessed = Formulas.Get<IFeesAssessedFormula>().Apply(referenceDate, transactions, GetTransactionCodes(TapeConfiguration.LateFeeTransactions));
            tape.LateFeePaid = tape.LateFeeAssessed;
            tape.NSFFeeAssessed = Formulas.Get<IFeesAssessedFormula>().Apply(referenceDate, transactions, GetTransactionCodes(TapeConfiguration.NsfFeeTransactions));
            tape.NSFFeePaid = tape.NSFFeeAssessed;
            tape.CheckProcessingFee = Formulas.Get<IFeesAssessedFormula>().Apply(referenceDate, transactions, GetTransactionCodes(TapeConfiguration.CheckProcessingFeeTransactions));
            tape.ChargeOffDate = chargeOff.Date;
            tape.ChargeOffAmount = chargeOff.Amount;

            tape.PrincipalRecovery = null;
            tape.IsLoanModified = null;
            tape.ServicingFeeAccrued = null;
            tape.FraudAmount = 0.0;
            tape.PrincipalAdjustment = 0.0;
            tape.InterestAdjustment = 0.0;
            tape.TrustInterestPurchased = null;
            tape.ReportDate = tape.CreationDate.Time.ToString("MMM-yy");
            tape.SubPoolId = string.Empty;
            tape.BeginningSubpoolId = string.Empty;
            tape.ConfirmOfFraudDate = null;
            tape.BoardingAmount = null;
            tape.OwnershipTransfers = string.Empty;
            tape.ErrorCheck = string.Empty;

            return tape;
        }

        private ILoanInvestor GetInvestor(string loanReferenceNumber, DateTimeOffset referenceDate, ILoanInfo info, ILogger tapeLogger)
        {
            tapeLogger.Debug("Loading investor {investorId}", new { investorId = info.Loan.Investor.Id });
            var investor = info.Loan.Investor;
            if (investor == null)
                tapeLogger.Warn("Investor not found: {investorId}", new { investorId = info.Loan.Investor.Id });
            return investor;
        }

        private IScore GetScore(string loanReferenceNumber, DateTimeOffset referenceDate, ILoanInfo info, ILogger tapeLogger)
        {
            tapeLogger.Debug("Finding score with name {FicoScoreName}", new { TapeConfiguration.FicoScoreName });
            var score = info.Loan.Scores.FirstOrDefault(s => s.Name == TapeConfiguration.FicoScoreName);
            if (score == null)
                tapeLogger.Warn("Score not found with name {FicoScoreName}", new { TapeConfiguration.FicoScoreName });
            return score;
        }

        private IBorrower GetBorrower(string loanReferenceNumber, DateTimeOffset referenceDate, ILoanDetails loanDetails, ILogger tapeLogger)
        {
            tapeLogger.Debug("Determining primary borrower");
            var borrower = loanDetails.LoanInfo.Loan.Borrowers.FirstOrDefault(b => b.IsPrimary);
            if (borrower == null)
                tapeLogger.Warn("No primary borrower found");
            return borrower;
        }

        private IEnumerable<string> GetTransactionCodes(IEnumerable<TransactionInfo> transactions)
        {
            return transactions.Select(t => t.Code);
        }
    }
}
