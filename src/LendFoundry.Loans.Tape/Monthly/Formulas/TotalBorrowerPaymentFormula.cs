﻿using LendFoundry.Foundation.Date;
using System;
using System.Linq;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public class TotalBorrowerPaymentFormula : ITotalBorrowerPaymentFormula
    {
        private ITenantTime TenantTime;

        public TotalBorrowerPaymentFormula(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        public double Apply(IPaymentSchedule schedule, DateTimeOffset referenceDate)
        {
            var start = TenantTime.BeginningOfMonth(referenceDate);
            var end = TenantTime.EndOfMonth(referenceDate);

            var installments = schedule.Installments
                .Where(i => i.Status == InstallmentStatus.Completed)
                .Where(i => i.Type != InstallmentType.ChargeOff)
                .Where(i => i.PaymentDate >= start)
                .Where(i => i.PaymentDate <= end);

            var total = installments.Sum(i => i.PaymentAmount);

            return Math.Round(total, 2, MidpointRounding.AwayFromZero);
        }
    }
}
