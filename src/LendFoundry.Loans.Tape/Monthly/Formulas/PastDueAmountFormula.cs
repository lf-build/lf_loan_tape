﻿using LendFoundry.Loans.Schedule;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public class PastDueAmountFormula : IPastDueAmountFormula
    {
        public double Apply(IPaymentScheduleSummary summary)
        {
            return summary.DaysPastDue > 0 ? summary.CurrentDue : 0.0;
        }
    }
}
