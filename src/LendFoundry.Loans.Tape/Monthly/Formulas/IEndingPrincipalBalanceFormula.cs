﻿using System;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public interface IEndingPrincipalBalanceFormula
    {
        double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule);
    }
}
