﻿using LendFoundry.Loans.Tape.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public class ChargeOffFormula : IChargeOffFormula
    {
        private TapeServiceConfiguration Configuration { get; }

        public ChargeOffFormula(TapeServiceConfiguration configuration)
        {
            if (configuration == null) throw new ArgumentNullException(nameof(configuration));
            if (configuration.TransactionCodes == null) throw new ArgumentNullException(nameof(configuration.TransactionCodes));
            if (string.IsNullOrWhiteSpace(configuration.TransactionCodes.ChargeOff)) throw new ArgumentNullException(nameof(configuration.TransactionCodes.ChargeOff));

            Configuration = configuration;
        }

        public ChargeOff Apply(IEnumerable<ITransaction> transactions)
        {
            var transaction = transactions.SingleOrDefault(t => t.Code == Configuration.TransactionCodes.ChargeOff);

            if (transaction == null)
               return ChargeOff.Null;

            return new ChargeOff(transaction.Date, transaction.Amount);
        }
    }
}
