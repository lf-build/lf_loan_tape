﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public class AverageDailyBalanceFormula : IAverageDailyBalanceFormula
    {
        internal const int MaxDaysInMonth = 30; //TODO: should be provided by the tenant's calendar type

        public AverageDailyBalanceFormula(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        private ITenantTime TenantTime { get; }

        public double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule)
        {
            return Apply(referenceDate, terms, schedule, null);
        }

        public double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule, ILoanTape previousTape)
        {
            var adb = 0.0;
            var intervals = new PrincipalBalanceIntervals(previousTape, schedule, terms, referenceDate.Year, referenceDate.Month, TenantTime);
            while (intervals.HasNext)
            {
                var interval = intervals.Next();
                adb += interval.Balance * interval.Days / MaxDaysInMonth;
            }
            return Math.Round(adb, 2, MidpointRounding.AwayFromZero);
        }

        internal class PrincipalBalanceIntervals
        {
            private IPaymentSchedule Schedule { get; }
            private ILoanTerms Terms { get; }
            private int Year { get; }
            private int Month { get; }
            private ITenantTime TenantTime { get; }
            private int Index { get; set; }
            private DateTimeOffset PeriodStart => TenantTime.Create(Year, Month, 1);
            private DateTimeOffset PeriodEnd => TenantTime.Create(Year, Month, DateTime.DaysInMonth(Year, Month));
            private List<BalanceChange> BalanceChanges { get; }
            private ILoanTape PreviousTape { get; }

            public PrincipalBalanceIntervals(ILoanTape previousTape, IPaymentSchedule schedule, ILoanTerms terms, int year, int month, ITenantTime tenantTime)
            {
                PreviousTape = previousTape;
                Schedule = schedule;
                Terms = terms;
                Year = year;
                Month = month;
                TenantTime = tenantTime;
                BalanceChanges = GetBalanceChanges().ToList();
            }

            private static DateTimeOffset DateOf(IInstallment installment)
            {
                if (IsEarlyPayment(installment) || IsLatePayment(installment))
                    return installment.PaymentDate.Value;

                return (installment.AnniversaryDate ?? installment.PaymentDate).Value;
            }

            private static bool IsEarlyPayment(IInstallment installment)
            {
                return 
                    installment.PaymentDate != null &&
                    installment.AnniversaryDate != null &&
                    installment.PaymentDate < installment.AnniversaryDate;
            }

            private static bool IsLatePayment(IInstallment installment)
            {
                return 
                    installment.PaymentDate != null &&
                    installment.AnniversaryDate != null &&
                    
                    //TODO: use a proper method to determine when it's a late payment

                    //As of now we don't have any means to know when a payment was made late,
                    //so we had to check if the payment date is past the grace period using
                    //a hardcoded grace period of 10 days. This should be fixed as soon as
                    //we add late payment information to the installment.
                    installment.PaymentDate > installment.DueDate.Value.AddDays(10);
            }

            public bool HasNext => Index < BalanceChanges.Count;

            public BalanceInterval Next()
            {
                if (!HasNext) throw new Exception("No more intervals");

                var current = BalanceChanges[Index];
                var next = Index < BalanceChanges.Count - 1 ? BalanceChanges[Index + 1] : null;

                Index++;

                return new BalanceInterval(current, next);
            }

            private IEnumerable<BalanceChange> GetBalanceChanges()
            {
                var changes = new List<BalanceChange>();

                changes.Add(new BalanceChange(PeriodStart, OpeningBalance));

                if (Terms.OriginationDate > PeriodStart && Terms.OriginationDate <= PeriodEnd)
                    changes.Add(new BalanceChange(Terms.OriginationDate, Terms.LoanAmount));

                changes.AddRange(Installments.Select(i => new BalanceChange(BalanceChangeDateOf(i), i.EndingBalance)));

                return changes
                    .OrderByDescending(c => c.Balance)
                    .OrderBy(c => c.Date);
            }

            private int BalanceChangeDateOf(IInstallment installment)
            {
                var shouldBeAnniversaryDate =
                    installment.Type == InstallmentType.Scheduled &&
                    !IsEarlyPayment(installment) &&
                    !IsLatePayment(installment);

                return shouldBeAnniversaryDate ?
                    Terms.OriginationDate.Day :
                    DateOf(installment).Day;
            }

            private IEnumerable<IInstallment> Installments
            {
                get
                {

                    var all = Schedule.Installments
                        .Where(i => i.Status == InstallmentStatus.Completed)
                        .Where(i => DateOf(i) >= PeriodStart)
                        .Where(i => DateOf(i) <= PeriodEnd);

                    var byDate = all.GroupBy(i => DateOf(i));

                    return byDate.Select(g => g.OrderBy(i => i.EndingBalance).First());
                }
            }

            public double OpeningBalance
            {
                get
                {
                    if (PreviousTape != null)
                        return PreviousTape.EndingPrincipalBalance;

                    var lastPayment = LastPaymentBefore(PeriodStart);

                    if (lastPayment != null)
                        return lastPayment.EndingBalance;

                    if (Terms.OriginationDate <= PeriodStart)
                        return Terms.LoanAmount;

                    return 0.0;
                }
            }

            private IInstallment LastPaymentBefore(DateTimeOffset date)
            {
                return Schedule.Installments
                    .Where(i => DateOf(i) < date)
                    .Where(i => i.Status == InstallmentStatus.Completed)
                    .OrderBy(i => i.EndingBalance)
                    .FirstOrDefault();
            }

            internal class BalanceInterval
            {
                public BalanceInterval(BalanceChange current, BalanceChange next)
                {
                    Balance = current.Balance;
                    Days = DaysBetween(current, next);
                }

                private int DaysBetween(BalanceChange current, BalanceChange next)
                {
                    var startDay = Math.Min(30, current.Date);
                    var endDay = next != null ? Math.Min(30, next.Date) : AverageDailyBalanceFormula.MaxDaysInMonth + 1;
                    return endDay - startDay;
                }

                public double Balance { get; }
                public int Days { get; }
            }

            internal class BalanceChange
            {
                public BalanceChange(DateTimeOffset date, double balance) : this(date.Day, balance)
                {
                }

                public BalanceChange(int date, double balance)
                {
                    Date = date;
                    Balance = balance;
                }

                public int Date { get; }
                public double Balance { get; }
            }
        }
    }
}
