﻿using System;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public interface ILoanAgeFormula
    {
        int GetLoanAge(DateTimeOffset date, ILoanTerms terms);
    }
}
