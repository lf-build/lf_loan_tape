﻿using System;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public interface IInterestAccruedFormula
    {
        double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule);
    }
}
