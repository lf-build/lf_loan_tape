﻿using System;
using LendFoundry.Foundation.Date;
using System.Linq;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public class PrincipalPaidFormula : IPrincipalPaidFormula
    {
        public PrincipalPaidFormula(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        private ITenantTime TenantTime { get; }

        public double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule)
        {
            var start = TenantTime.BeginningOfMonth(referenceDate);
            var end = TenantTime.EndOfMonth(referenceDate);
            var principalPaid = schedule.Installments
                .Where(i => i.Status == InstallmentStatus.Completed)
                .Where(i => i.Type != InstallmentType.ChargeOff)
                .OrderBy(i => i.PaymentDate)
                .Where(i => i.PaymentDate >= start)
                .Where(i => i.PaymentDate <= end)
                .Sum(i => i.Principal);

            return Math.Round(principalPaid, 2, MidpointRounding.AwayFromZero);
        }
    }
}
