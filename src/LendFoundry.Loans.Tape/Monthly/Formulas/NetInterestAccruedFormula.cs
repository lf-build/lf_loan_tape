﻿using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public class NetInterestAccruedFormula : INetInterestAccruedFormula
    {
        public NetInterestAccruedFormula(IInterestAccruedFormula interestAccruedFormula, IInterestPaidFormula interestPaidFormula, ITenantTime tenantTime)
        {
            InterestAccruedFormula = interestAccruedFormula;
            InterestPaidFormula = interestPaidFormula;
            TenantTime = tenantTime;
        }

        private IInterestAccruedFormula InterestAccruedFormula { get; }
        private IInterestPaidFormula InterestPaidFormula { get; }
        private ITenantTime TenantTime { get; }

        public double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule)
        {
            var interestAccrued = InterestAccruedFormula.Apply(referenceDate, terms, schedule);
            var netInterestAccruedPreviousMonth = NetInterestAccruedPreviousMonth(referenceDate, terms, schedule);
            var interestPaid = InterestPaidFormula.Apply(referenceDate, terms, schedule);
            var netInterestAccrued = Math.Round(interestAccrued + netInterestAccruedPreviousMonth - interestPaid, 2, MidpointRounding.AwayFromZero);
            return Math.Max(0.0, netInterestAccrued);
        }

        private double NetInterestAccruedPreviousMonth(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule)
        {
            if (IsBeforeOriginationMonth(referenceDate, terms))
                return 0.0;

            return Apply(TenantTime.EndOfPreviousMonth(referenceDate), terms, schedule);
        }

        private bool IsBeforeOriginationMonth(DateTimeOffset date, ILoanTerms terms)
        {
            var month = TenantTime.BeginningOfMonth(date);
            var originationMonth = TenantTime.BeginningOfMonth(terms.OriginationDate);
            return month < originationMonth;
        }
    }
}
