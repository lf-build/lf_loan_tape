﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public interface IChargeOffFormula
    {
        ChargeOff Apply(IEnumerable<ITransaction> transactions);
    }

    public class ChargeOff
    {
        public static readonly ChargeOff Null = new ChargeOff();

        private ChargeOff()
        {
        }

        public ChargeOff(DateTimeOffset date, double amount)
        {
            Date = date;
            Amount = amount;
        }

        public DateTimeOffset? Date { get; }
        public double Amount { get; }
    }
}
