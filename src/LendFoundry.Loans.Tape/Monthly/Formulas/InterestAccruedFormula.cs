﻿using System;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public class InterestAccruedFormula : IInterestAccruedFormula
    {
        public InterestAccruedFormula(IAverageDailyBalanceFormula averageDailyBalanceFormula)
        {
            AverageDailyBalanceFormula = averageDailyBalanceFormula;
        }

        private IAverageDailyBalanceFormula AverageDailyBalanceFormula { get; }

        public double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule)
        {
            var adb = AverageDailyBalanceFormula.Apply(referenceDate, terms, schedule);
            var rate = terms.Rate / 100;
            var monthlyRate = rate / 12;
            return Math.Round(adb * monthlyRate, 2, MidpointRounding.AwayFromZero);
        }
    }
}
