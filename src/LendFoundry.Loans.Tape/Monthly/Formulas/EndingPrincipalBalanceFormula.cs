﻿using LendFoundry.Foundation.Date;
using System;
using System.Linq;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public class EndingPrincipalBalanceFormula : IEndingPrincipalBalanceFormula
    {
        public EndingPrincipalBalanceFormula(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        private ITenantTime TenantTime { get; }

        public double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule)
        {
            var period = TenantTime.Period(referenceDate);

            var lastPayment = schedule.Installments
                .Where(i => i.Status == InstallmentStatus.Completed)
                .OrderByDescending(i => i.EndingBalance)
                .Where(i => i.PaymentDate <= period.End)
                .LastOrDefault();

            return lastPayment?.EndingBalance ?? terms.LoanAmount;
        }
    }
}
