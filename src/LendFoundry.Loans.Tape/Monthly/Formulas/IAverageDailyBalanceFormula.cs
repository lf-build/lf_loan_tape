﻿using System;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public interface IAverageDailyBalanceFormula
    {
        double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule);
        double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule, ILoanTape previousTape);
    }
}
