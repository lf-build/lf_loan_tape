﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public interface IFeesAssessedFormula
    {
        double Apply(DateTimeOffset referenceDate, IEnumerable<ITransaction> transactions, IEnumerable<string> feeTransactionCodes);
    }
}
