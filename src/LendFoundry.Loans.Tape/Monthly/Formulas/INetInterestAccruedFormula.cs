﻿using System;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public interface INetInterestAccruedFormula
    {
        double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule);
    }
}
