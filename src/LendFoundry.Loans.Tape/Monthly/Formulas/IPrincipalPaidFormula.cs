﻿using System;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public interface IPrincipalPaidFormula
    {
        double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule);
    }
}
