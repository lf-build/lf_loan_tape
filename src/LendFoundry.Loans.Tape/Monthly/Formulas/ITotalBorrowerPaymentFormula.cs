﻿using System;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public interface ITotalBorrowerPaymentFormula
    {
        double Apply(IPaymentSchedule schedule, DateTimeOffset referenceDate);
    }
}
