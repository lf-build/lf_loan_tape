﻿using LendFoundry.Foundation.Date;
using System;
using System.Linq;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public class BeginningPrincipalBalanceFormula : IBeginningPrincipalBalanceFormula
    {
        public BeginningPrincipalBalanceFormula(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        private ITenantTime TenantTime { get; }

        public double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule)
        {
            return Apply(referenceDate, terms, schedule, null);
        }

        public double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule, ILoanTape previousTape)
        {
            if (previousTape != null)
                return previousTape.EndingPrincipalBalance;

            var period = TenantTime.Period(referenceDate);

            var lastPayment = schedule.Installments
                .Where(i => i.Status == InstallmentStatus.Completed)
                .OrderByDescending(i => i.EndingBalance)
                .Where(i => i.PaymentDate < period.Start)
                .LastOrDefault();

            if (lastPayment != null)
                return lastPayment.EndingBalance;

            if (terms.OriginationDate < period.Start)
                return terms.LoanAmount;

            return 0.0;
        }
    }
}
