﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using System.Linq;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public class FeesAssessedFormula : IFeesAssessedFormula
    {
        public FeesAssessedFormula(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        private ITenantTime TenantTime { get; }

        public double Apply(DateTimeOffset referenceDate, IEnumerable<ITransaction> transactions, IEnumerable<string> feeTransactionCodes)
        {
            var start = TenantTime.BeginningOfMonth(referenceDate);
            var end = TenantTime.EndOfMonth(referenceDate);

            var feesAssessed = transactions
                .Where(t => t.Date >= start)
                .Where(t => t.Date <= end)
                .Where(t => feeTransactionCodes.Contains(t.Code))
                .Sum(t => t.Amount);

            return Math.Round(feesAssessed, 2, MidpointRounding.AwayFromZero);
        }
    }
}
