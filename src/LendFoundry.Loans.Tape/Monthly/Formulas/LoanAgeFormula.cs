﻿using System;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public class LoanAgeFormula : ILoanAgeFormula
    {
        public int GetLoanAge(DateTimeOffset date, ILoanTerms terms)
        {
            var origination = terms.OriginationDate;

            for (var age = 1; ; age++)
            {
                var anniversary = origination.AddMonths(age);
                if (anniversary > date)
                    return age - 1;
            }

            throw new Exception("Should never reach this point");
        }
    }
}
