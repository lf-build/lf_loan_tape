﻿using System;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public interface IInterestPaidFormula
    {
        double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule);
    }
}
