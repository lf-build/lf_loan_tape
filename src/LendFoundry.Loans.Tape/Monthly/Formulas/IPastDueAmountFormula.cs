﻿using System;
using System.Collections.Generic;
using LendFoundry.Loans.Schedule;

namespace LendFoundry.Loans.Tape.Monthly.Formulas
{
    public interface IPastDueAmountFormula
    {
        double Apply(IPaymentScheduleSummary summary);
    }
}
