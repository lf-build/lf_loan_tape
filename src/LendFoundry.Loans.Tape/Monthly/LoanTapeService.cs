﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using System;
using System.Globalization;

namespace LendFoundry.Loans.Tape.Monthly
{
    public class LoanTapeService : ILoanTapeService
    {
        public LoanTapeService(
            ILoanTapeBuilder tapeFactory,
            ILoanTapeRepository repository,
            ITenantTime tenantTime,
            ILogger logger)
        {
            TapeFactory = tapeFactory;
            Repository = repository;
            TenantTime = tenantTime;
            Logger = logger.Scope(this);
        }

        private ILoanTapeBuilder TapeFactory { get; }
        private ILoanTapeRepository Repository { get; }
        private ITenantTime TenantTime { get; }
        private ILogger Logger { get; }

        public void GenerateTape(string loanReferenceNumber, DateTimeOffset referenceDate)
        {
            var tapeLogger = Logger.ForLoanTape(loanReferenceNumber, referenceDate);

            tapeLogger.Info("Generating tape");

            var previousTape = Repository.GetByLoan(loanReferenceNumber, TenantTime.EndOfPreviousMonth(referenceDate).ToString("yyyy-MM"));

            var tape = TapeFactory.BuildTape(loanReferenceNumber, referenceDate, previousTape);

            if (HasLoanBeenPaidOff(tape, referenceDate))
                tapeLogger.Debug("Tape will not be generate because the loan has been paid off before the reference period");
            else
                Repository.AddOrUpdate(tape);
        }

        private bool HasLoanBeenPaidOff(ILoanTape tape, DateTimeOffset referenceDate)
        {
            var periodStart = TenantTime.BeginningOfMonth(referenceDate);
            return
                tape.BeginningPrincipalBalance == 0.0 &&
                tape.EndingPrincipalBalance == 0.0 &&
                tape.OriginationDate < periodStart;
        }

        public ILoanTape[] GetByLoan(string referenceNumber)
        {
            if (string.IsNullOrWhiteSpace(referenceNumber))
                throw new ArgumentNullException($"#{nameof(referenceNumber)} cannot be null");

            return Repository.GetByLoan(referenceNumber);
        }

        public ILoanTape GetByLoan(string referenceNumber, string yearAndMonth)
        {
            if (string.IsNullOrWhiteSpace(referenceNumber))
                throw new ArgumentNullException($"#{nameof(referenceNumber)} cannot be null");

            if (string.IsNullOrWhiteSpace(yearAndMonth))
                throw new ArgumentNullException($"#{nameof(yearAndMonth)} cannot be null");

            return Repository.GetByLoan(referenceNumber, yearAndMonth);
        }

        public ILoanTape[] GetByYearMonth(string yearAndMonth)
        {
            if (string.IsNullOrWhiteSpace(yearAndMonth))
                throw new ArgumentNullException($"#{nameof(yearAndMonth)} cannot be null");

            return Repository.GetByYearMonth(yearAndMonth);
        }

        public string GenerateFileName(string yearAndMonth, string sufixName)
        {
            string fileName = string.Empty;
            try
            {
                int year = int.Parse(yearAndMonth.Split('-')[0]);
                int month = int.Parse(yearAndMonth.Split('-')[1]);
                var abbrMonth = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(month);
                abbrMonth = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(abbrMonth);
                fileName = $"{abbrMonth}-{year} {sufixName}.csv";
            }
            catch { fileName = $"{sufixName}.csv"; }
            return fileName;
        }
    }
}