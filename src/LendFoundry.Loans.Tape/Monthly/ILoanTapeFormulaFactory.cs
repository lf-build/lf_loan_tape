﻿using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.Loans.Tape.Monthly
{
    public interface ILoanTapeFormulaFactory
    {
        ILoanTapeFormula Create(ILoanDetails loanDetails, DateTimeOffset referenceDate, LoanTapeConfiguration configuration, ITenantTime tenantTime);
    }
}
