﻿using LendFoundry.Configuration.Client;
using LendFoundry.Loans.Tape.Configuration;
using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;
using System;

namespace LendFoundry.Loans.Tape.Monthly
{
    public static class LoanTapeConfigurationServiceExtensions
    {
        public static TapeServiceConfiguration GetTapeServiceConfiguration(this IServiceProvider provider)
        {
            return GetTapeServiceConfiguration(provider, provider.GetService<ITokenReader>());
        }

        public static LoanTapeConfiguration GetMonthlyLoanTapeConfiguration(this IServiceProvider provider)
        {
            return provider.GetService<TapeServiceConfiguration>().MonthlyLoanTape;
        }

        public static TransactionTapeConfiguration GetDailyTransactionTapeConfiguration(this IServiceProvider provider)
        {
            return provider.GetService<TapeServiceConfiguration>().DailyTransactionTape;
        }

        public static InvestorConfiguration GetInvestorConfiguration(this IServiceProvider provider)
        {
            return GetInvestorConfiguration(provider, provider.GetService<ITokenReader>());
        }

        public static TapeServiceConfiguration GetTapeServiceConfiguration(this IServiceProvider provider, ITokenReader tokenReader)
        {
            var factory = provider.GetService<IConfigurationServiceFactory>();
            return factory.Create<TapeServiceConfiguration>(Settings.ServiceName, tokenReader).Get();
        }

        public static InvestorConfiguration GetInvestorConfiguration(this IServiceProvider provider, ITokenReader tokenReader)
        {
            var factory = provider.GetService<IConfigurationServiceFactory>();
            return factory.Create<InvestorConfiguration>("investors", tokenReader).Get();
        }
    }
}
