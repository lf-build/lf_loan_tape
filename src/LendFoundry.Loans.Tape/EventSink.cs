﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Tape.Configuration;
using LendFoundry.Loans.Tape.Daily;
using LendFoundry.Loans.Tape.Daily.InvestorFeed;
using LendFoundry.Loans.Tape.Daily.Transactions;
using LendFoundry.Loans.Tape.Monthly;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Tape
{
    public class EventSink
    {
        public EventSink
        (
            ITokenHandler tokenHandler,
            ILoggerFactory loggerFactory,
            IEventHubClientFactory eventHubFactory,
            ITenantServiceFactory tenantServiceFactory,
            ILoanTapeServiceFactory loanTapeServiceFactory,
            ITransactionTapeServiceFactory transactionTapeServiceFactory,
            IInvestorFeedServiceFactory investorFeedServiceFactory,
            IConfigurationServiceFactory configurationFactory,
            IConfigurationServiceFactory<TapeServiceConfiguration> tapeServiceConfigurationFactory,
            ITenantTimeFactory tenantTimeFactory
        )
        {
            LoggerFactory = loggerFactory;
            Logger = loggerFactory.CreateLogger().Scope(this);
            TokenHandler = tokenHandler;
            EventHubFactory = eventHubFactory;
            TenantServiceFactory = tenantServiceFactory;
            LoanTapeServiceFactory = loanTapeServiceFactory;
            TransactionTapeServiceFactory = transactionTapeServiceFactory;
            InvestorFeedServiceFactory = investorFeedServiceFactory;
            ConfigurationFactory = configurationFactory;
            TapeServiceConfigurationFactory = tapeServiceConfigurationFactory;
            TenantTimeFactory = tenantTimeFactory;
        }

        private ITokenReader EmptyTokenReader { get; } = new StaticTokenReader(string.Empty);
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ILogger Logger { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ILoanTapeServiceFactory LoanTapeServiceFactory { get; }
        private ITransactionTapeServiceFactory TransactionTapeServiceFactory { get; }
        private IInvestorFeedServiceFactory InvestorFeedServiceFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IConfigurationServiceFactory<TapeServiceConfiguration> TapeServiceConfigurationFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }

        public void Start()
        {
            try
            {
                var tenantService = TenantServiceFactory.Create(EmptyTokenReader);
                var tenants = tenantService.GetActiveTenants();

                Logger.Info($"{tenants.Count} tenant(s) found");

                var eventHandlers = CreateEventHandlers(tenants);
                var eventHub = EventHubFactory.Create(EmptyTokenReader);

                foreach(var eventName in EventsHandledBy(eventHandlers))
                {
                    Logger.Info("Subscribing to event {eventName}", new { eventName });

                    eventHub.On(eventName, @event =>
                    {
                        Logger.Info("Event {event.Name} received", new { @event });

                        foreach (var handler in eventHandlers)
                        {
                            handler.Process(@event);
                        }
                    });
                }

                eventHub.StartAsync();
            }
            catch (Exception exception)
            {
                Logger.Error("Error while subscribing to events", exception);
            }
        }

        private IEnumerable<EventHandler> CreateEventHandlers(IEnumerable<TenantInfo> tenants)
        {
            var handlers = new List<EventHandler>();

            foreach(var tenant in tenants)
            {
                var configuration = GetConfiguration(tenant);
                var transactionFactory = new TransactionFactory(TenantTimeFactory.Create(ConfigurationFactory, TokenReaderFor(tenant)));

                handlers.Add(new LoanTapeEventHandler(tenant, configuration, TokenHandler, TenantTimeFactory, ConfigurationFactory, LoanTapeServiceFactory, LoggerFactory));
                handlers.Add(new TransactionTapeEventHandler(tenant, configuration, TokenHandler, TransactionTapeServiceFactory, transactionFactory, LoggerFactory));
                handlers.Add(new InvestorFeedEventHandler(tenant, configuration, TokenHandler, InvestorFeedServiceFactory, transactionFactory, LoggerFactory));
            }

            return handlers;
        }

        private IEnumerable<string> EventsHandledBy(IEnumerable<EventHandler> eventHandlers)
        {
            var events = new HashSet<string>();

            foreach(var eventHandler in eventHandlers)
            {
                foreach(var eventName in eventHandler.AcceptedEvents)
                {
                    events.Add(eventName);
                }
            }

            return events;
        }

        private TapeServiceConfiguration GetConfiguration(TenantInfo tenant)
        {
            var configuration = TapeServiceConfigurationFactory.Create(TokenReaderFor(tenant)).Get();

            if (configuration == null)
                throw new ArgumentException($"Configuration not found for tenant: {tenant.Id}");

            return configuration;
        }

        private ITokenReader TokenReaderFor(TenantInfo tenant)
        {
            var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName);
            return new StaticTokenReader(token.Value);
        }
    }
}
