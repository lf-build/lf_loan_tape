﻿using System;
using System.Threading.Tasks;

namespace LendFoundry.Loans.Tape
{
    public static class TaskExtensions
    {
        public static T Await<T>(this Task<T> task)
        {
            task.Wait();

            if (task.IsCompleted)
                return task.Result;

            throw new Exception("Task failed", task.Exception);
        }
    }
}
