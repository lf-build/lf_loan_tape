﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace LendFoundry.Loans.Tape
{
    public abstract class EventHandler
    {
        public EventHandler(string tenantId, ILogger logger)
        {
            TenantId = tenantId;
            Logger = logger.Scope(this);

            Worker = new Thread(Work);
            Worker.IsBackground = true;
            Worker.Start();
        }

        private string TenantId { get; }
        private ILogger Logger { get; }
        private Thread Worker { get; }
        private Queue<EventInfo> EventQueue { get; } = new Queue<EventInfo>();

        public abstract IEnumerable<string> AcceptedEvents { get; }

        protected abstract void Handle(EventInfo @event);

        private bool Accepts(EventInfo @event)
        {
            return @event.TenantId == TenantId && AcceptedEvents.Contains(@event.Name);
        }

        public void Process(EventInfo @event)
        {
            if (Accepts(@event))
            {
                lock (EventQueue)
                {
                    EventQueue.Enqueue(@event);
                    Monitor.Pulse(EventQueue);
                }
            }
        }

        private void Work()
        {
            while (true)
            {
                EventInfo @event = null;

                lock (EventQueue)
                {
                    if (EventQueue.Count == 0)
                    {
                        Monitor.Wait(EventQueue);
                    }

                    @event = EventQueue.Dequeue();
                }

                if (@event != null)
                {
                    try
                    {
                       Handle(@event);
                    }
                    catch(Exception e)
                    {
                        Logger.Error("Error handling event {EventInfo.Name}", e, new { EventInfo = @event });
                    }
                }
            }
        }
    }
}
