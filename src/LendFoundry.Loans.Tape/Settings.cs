﻿using LendFoundry.Foundation.Services.Settings;

namespace LendFoundry.Loans.Tape
{
    public static class Settings
    {
        private const string Prefix = "LOAN_TAPE";

        public static string ServiceName { get; } = "loan-tape";

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "loan-tape");

    }
}
