﻿using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Tape
{
    public class InvestorConfiguration
    {
        public List<Investor> Investors { get; set; }

        public Investor FindById(string id)
        {
            return Investors.FirstOrDefault(i => i.Id == id);
        }
    }
}
