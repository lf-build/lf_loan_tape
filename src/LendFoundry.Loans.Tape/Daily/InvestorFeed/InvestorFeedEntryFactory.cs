﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Tape.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using TransactionInfo = LendFoundry.Loans.TransactionLog.Client.TransactionInfo;

namespace LendFoundry.Loans.Tape.Daily.InvestorFeed
{
    public class InvestorFeedEntryFactory : IInvestorFeedEntryFactory
    {
        public InvestorFeedEntryFactory(TapeServiceConfiguration configuration, IInvestorFeedRepository repository, IEnumerable<Configuration.Investor> investors, ILogger logger)
        {
            Configuration = configuration.DailyInvestorFeed;
            Repository = repository;
            TransactionCodes = configuration.TransactionCodes;
            Investors = investors;
            Logger = logger.Scope(this);
        }

        private InvestorFeedConfiguration Configuration { get; }
        private IInvestorFeedRepository Repository { get; }
        private Configuration.TransactionCodes TransactionCodes { get; }
        private IEnumerable<Configuration.Investor> Investors { get; }
        private ILogger Logger { get; }

        public IInvestorFeedEntry Create(TransactionInfo transaction, ILoan loan, IEnumerable<IInstallment> schedule)
        {
            var tapeLogger = Logger.ForTransactionTape(transaction.LoanReference, transaction.Id);
            var installment = string.IsNullOrWhiteSpace(transaction.PaymentId) ? null : schedule.SingleOrDefault(i => i.PaymentId == transaction.PaymentId);
            var investor = Investors.SingleOrDefault(i => i.Id == loan.Investor.Id);
            var borrower = loan.Borrowers.SingleOrDefault(b => b.IsPrimary);

            if (!string.IsNullOrWhiteSpace(transaction.PaymentId) && installment == null)
                tapeLogger.Warn("Installment with paymentId {PaymentId} not found for transaction {Id}", transaction);

            if (investor == null)
                tapeLogger.Warn("Investor with id {InvestorId} not found", new { InvestorId = loan.Investor.Id });

            if (borrower == null)
                tapeLogger.Warn("Primary borrower not found for loan {LoanReferenceNumber}", new { LoanReferenceNumber = loan.ReferenceNumber });

            var paymentEconomics = GetPaymentEconomics(transaction, installment, tapeLogger);

            var entry = new InvestorFeedEntry() as IInvestorFeedEntry;

            entry.LoanReferenceNumber = transaction.LoanReference;
            entry.Investor = loan.Investor.Id;
            entry.TransactionId = transaction.Id;
            entry.TransactionCode = transaction.Code;
            entry.PaymentId = transaction.PaymentId;
            entry.FeedDate = transaction.Timestamp;
            entry.Entity = investor?.Name;
            entry.TransactionNumber = IsReversal(transaction) ? null : transaction.Id;
            entry.ReversalNumber = IsReversal(transaction) ? transaction.Id : null;
            entry.TransactionDate = transaction.Date;
            entry.OriginationDate = loan.Terms.OriginationDate;
            entry.BorrowerFirstName = Truncate(borrower?.FirstName, 1).ToUpper();
            entry.BorrowerLastName = Truncate(borrower?.LastName, 3).ToUpper();
            entry.DueDate = GetScheduleDate(transaction, installment, paymentEconomics);
            entry.LoanAge = GetLoanAge(transaction, loan);
            entry.TransactionAmount = ChangeOperationSign(transaction.Amount, transaction);
            entry.Description = GetTransactionDescription(transaction, installment);
            entry.PaymentMethod = (installment?.PaymentMethod ?? loan.PaymentMethod).ToString();
            entry.Principal = ChangeOperationSign(paymentEconomics?.Principal ?? 0.0, transaction);
            entry.Interest = ChangeOperationSign(paymentEconomics?.Interest ?? 0.0, transaction);
            entry.Fees = 0.0;
            entry.LateCharges = 0.0;
            entry.EffectivePaymentAmount = GetEffectivePaymentAmount(transaction);

            return entry;
        }

        private double ChangeOperationSign(double amount, TransactionInfo transaction)
        {
            if (amount == 0.0) return 0.0;
            return amount * (IsReversal(transaction.Code) ? -1 : 1);
        }

        private PaymentEconomics GetPaymentEconomics(TransactionInfo transaction, IInstallment installment, ILogger tapeLogger)
        {
            if (IsPaymentReversal(transaction))
            {
                var payment = Repository
                    .All(t => t.PaymentId == transaction.PaymentId && t.TransactionId != transaction.Id)
                    .Await()
                    .FirstOrDefault();

                if (payment != null)
                {
                    return new PaymentEconomics
                    {
                        Principal = payment.Principal,
                        Interest = payment.Interest,
                        DueDate = payment.DueDate
                    };
                }
                else
                {
                    tapeLogger.Warn("Payment transaction not found for reversal: {Id}", transaction);
                }
            }

            if (installment != null)
            {
                return new PaymentEconomics
                {
                    Principal = installment.Principal,
                    Interest = installment.Interest,
                };
            }

            return null;
        }
        
        private bool IsPaymentReversal(TransactionInfo transaction)
        {
            return IsReversal(transaction.Code) && !string.IsNullOrWhiteSpace(transaction.PaymentId);
        }

        private static string Truncate(string str, int length)
        {
            if (str == null) return null;
            if (str.Length <= length) return str;
            return str.Substring(0, length);
        }

        private double GetEffectivePaymentAmount(TransactionInfo transaction)
        {
            return transaction.Amount * (IsDebitOperation(transaction) ? -1.0 : 1.0);
        }

        private string GetTransactionDescription(TransactionInfo transaction, IInstallment installment)
        {
            if (installment != null)
            {
                if (installment.Type == InstallmentType.Additional)
                    return "Extra Payment";

                if (installment.Type == InstallmentType.ChargeOff)
                    return "Charge-off";
            }

            return transaction.Description;
        }

        private int GetLoanAge(TransactionInfo transaction, ILoan loan)
        {
            //TODO: refactor this method
            var origination = loan.Terms.OriginationDate;
            var transactionDate = transaction.Date;
            for (var period = 1; ; period++)
            {
                var anniversary = origination.AddMonths(period);
                if (transactionDate.IsDateBefore(anniversary))
                {
                    return period - 1;
                }
            }
            throw new Exception("Should not reach this point");
        }

        private DateTimeOffset? GetScheduleDate(TransactionInfo transaction, IInstallment installment, PaymentEconomics paymentEconomics)
        {
            //TODO: refactor this method
            if (installment == null)
            {
                return paymentEconomics?.DueDate ?? GetTransactionScheduleDate(transaction);
            }

            if (installment.PaymentMethod == PaymentMethod.Check)
            {
                return GetCheckPaymentScheduleDate(installment);
            }

            return installment.DueDate;
        }

        private DateTimeOffset? GetCheckPaymentScheduleDate(IInstallment installment)
        {
            if (installment.Type == InstallmentType.Scheduled)
                return installment.DueDate.Value;

            // Other types of check payment don't have a schedule date
            return null;
        }

        private DateTimeOffset? GetTransactionScheduleDate(TransactionInfo transaction)
        {
            // Hack: if it's P&I or P&I reversal and there's no installment associated,
            // use the transaction date as schedule date.
            if (transaction.Code == "124" || transaction.Code == "125")
                return transaction.Date;

            return null;
        }

        private bool IsReversal(string transactionCode)
        {
            return TransactionCodes.Reversals.Any(reversalCode => transactionCode == reversalCode);
        }

        private bool IsReversal(TransactionInfo transaction)
        {
            return IsReversal(transaction.Code);
        }
        
        private bool IsDebitOperation(TransactionInfo transaction)
        {
            return TransactionCodes.DebitOperations.Any(debitOperationCode => transaction.Code == debitOperationCode);
        }
    }

    internal static class DateTimeOffsetExtensions
    {
        public static bool IsDateBefore(this DateTimeOffset left, DateTimeOffset right)
        {
            var leftYear = left.Year;
            var rightYear = right.Year;

            if (leftYear < rightYear) return true;
            if (leftYear > rightYear) return false;

            var leftMonth = left.Month;
            var rightMonth = right.Month;

            if (leftMonth < rightMonth) return true;
            if (leftMonth > rightMonth) return false;

            return left.Day < right.Day;
        }
    }
}
