﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Tape.Daily.InvestorFeed
{
    public interface IInvestorFeedServiceFactory
    {
        IInvestorFeedService Create(ITokenReader tokenReader);
    }
}
