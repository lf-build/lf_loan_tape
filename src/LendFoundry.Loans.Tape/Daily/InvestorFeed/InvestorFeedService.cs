﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Tape.Configuration;
using LendFoundry.Loans.TransactionLog.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using LoanTransaction = LendFoundry.Loans.TransactionLog.Client.TransactionInfo;

namespace LendFoundry.Loans.Tape.Daily.InvestorFeed
{
    public class InvestorFeedService : IInvestorFeedService
    {
        public InvestorFeedService(
            TapeServiceConfiguration configuration,
            IInvestorFeedEntryFactory entryFactory,
            IInvestorFeedRepository repository,
            ITransactionLogService transactionLogClient,
            ILoanService loanService,
            ILogger logger)
        {
            Configuration = configuration.DailyInvestorFeed;
            EntryFactory = entryFactory;
            Repository = repository;
            TransactionLogClient = transactionLogClient;
            LoanService = loanService;
            Logger = logger.Scope(this);
        }

        private InvestorFeedConfiguration Configuration { get; }
        private IInvestorFeedEntryFactory EntryFactory { get; }
        private IInvestorFeedRepository Repository { get; }
        private ITransactionLogService TransactionLogClient { get; }
        private ILoanService LoanService { get; }
        private ILogger Logger { get; }

        public void Update(string loanReferenceNumber)
        {
            var tapeLogger = Logger.Scope(loanReferenceNumber);
            var loan = GetLoan(loanReferenceNumber);
            var schedule = LoanService.GetPaymentSchedule(loanReferenceNumber);
            var transactions = TransactionLogClient.GetTransactions(loanReferenceNumber);

            foreach (var transaction in transactions)
            {
                Update(transaction, loan, schedule);
            }
        }

        public void Update(string loanReferenceNumber, string transactionId)
        {
            Update(GetTransaction(loanReferenceNumber, transactionId));
        }
        
        public void Update(LoanTransaction transaction)
        {
            var loan = GetLoan(transaction.LoanReference);
            var schedule = LoanService.GetPaymentSchedule(transaction.LoanReference);
            Update(transaction, loan, schedule);
        }

        public void Update(LoanTransaction transaction, ILoan loan, IEnumerable<IInstallment> schedule)
        {
            var entry = EntryFactory.Create(transaction, loan, schedule);
            Repository.AddOrUpdate(entry);
        }

        private LoanTransaction GetTransaction(string loanReferenceNumber, string transactionId)
        {
            var transaction = TransactionLogClient.GetTransactionByLoanAndTransaction(loanReferenceNumber, transactionId);
            if (transaction == null)
            {
                throw new ArgumentException($"Transaction not found: loanReferenceNumber={loanReferenceNumber} id={transactionId}");
            }

            return transaction;
        }

        private ILoan GetLoan(string loanReferenceNumber)
        {
            var info = LoanService.GetLoanInfo(loanReferenceNumber);

            if (info == null)
                throw new ArgumentException($"Loan not found: {loanReferenceNumber}", nameof(loanReferenceNumber));

            return info.Loan;
        }

        public IEnumerable<IInvestorFeedEntry> BuildTape(DateTimeOffset endDate)
        {
            return Sort(Repository.Query(endDate, Configuration.AcceptedInvestors, IgnoredTransactionCodes));
        }

        public IEnumerable<IInvestorFeedEntry> BuildTape(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            return Sort(Repository.Query(startDate, endDate, Configuration.AcceptedInvestors, IgnoredTransactionCodes));
        }

        private IEnumerable<IInvestorFeedEntry> Sort(IEnumerable<IInvestorFeedEntry> transactions)
        {
            return transactions
                .OrderBy(t => t.FeedDate)
                .ThenBy(t => t.Entity)
                .ThenBy(t => t.TransactionDate)
                .ThenBy(t => t.LoanReferenceNumber)
                .ThenBy(t => t.TransactionNumber)
                .ThenBy(t => t.ReversalNumber);
        }

        private string[] IgnoredTransactionCodes => Configuration.IgnoredTransactions.Select(t => t.Code).ToArray();
    }
}
