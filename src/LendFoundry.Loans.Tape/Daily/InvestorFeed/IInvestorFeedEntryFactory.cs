﻿using LendFoundry.Loans.TransactionLog.Client;
using System.Collections.Generic;

namespace LendFoundry.Loans.Tape.Daily.InvestorFeed
{
    public interface IInvestorFeedEntryFactory
    {
        IInvestorFeedEntry Create(TransactionInfo transaction, ILoan loan, IEnumerable<IInstallment> schedule);
    }
}
