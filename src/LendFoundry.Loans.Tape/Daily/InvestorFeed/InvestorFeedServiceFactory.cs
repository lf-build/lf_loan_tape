﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Tape.Configuration;
using LendFoundry.Loans.TransactionLog.Client;
using LendFoundry.Security.Tokens;
using System.Collections.Generic;
using Investor = LendFoundry.Loans.Tape.Configuration.Investor;

namespace LendFoundry.Loans.Tape.Daily.InvestorFeed
{
    public class InvestorFeedServiceFactory : IInvestorFeedServiceFactory
    {
        public InvestorFeedServiceFactory(
            ITransactionLogServiceFactory transactionLogServiceFactory,
            ILoanServiceFactory loanServiceFactory,
            IInvestorFeedRepositoryFactory investorFeedRepositoryFactory,
            IConfigurationServiceFactory configurationFactory,
            IConfigurationServiceFactory<TapeServiceConfiguration> tapeServiceConfigurationFactory,
            ILoggerFactory loggerFactory)
        {
            ConfigurationFactory = configurationFactory;
            TransactionLogServiceFactory = transactionLogServiceFactory;
            LoanServiceFactory = loanServiceFactory;
            InvestorFeedRepositoryFactory = investorFeedRepositoryFactory;
            TapeServiceConfigurationFactory = tapeServiceConfigurationFactory;
            LoggerFactory = loggerFactory;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IConfigurationServiceFactory<TapeServiceConfiguration> TapeServiceConfigurationFactory { get; }
        private ITransactionLogServiceFactory TransactionLogServiceFactory { get; }
        private ILoanServiceFactory LoanServiceFactory { get; }
        private IInvestorFeedRepositoryFactory InvestorFeedRepositoryFactory { get; }
        private ILoggerFactory LoggerFactory { get; }

        public IInvestorFeedService Create(ITokenReader reader)
        {
            var configuration = GetConfiguration(reader);
            var investors = GetInvestors(reader);
            var investorFeedRepository = InvestorFeedRepositoryFactory.Create(reader);

            return new InvestorFeedService(
                configuration,
                new InvestorFeedEntryFactory(configuration, investorFeedRepository, investors, LoggerFactory.CreateLogger()),
                investorFeedRepository,
                TransactionLogServiceFactory.Create(reader),
                LoanServiceFactory.Create(reader),
                LoggerFactory.CreateLogger()
            );
        }

        private TapeServiceConfiguration GetConfiguration(ITokenReader reader)
        {
            var configurationService = TapeServiceConfigurationFactory.Create(reader);
            var configuration = configurationService.Get();
            return configuration;
        }

        private IEnumerable<Configuration.Investor> GetInvestors(ITokenReader reader)
        {
            var configurationService = ConfigurationFactory.Create<InvestorServiceConfiguration>("investors", reader);
            var configuration = configurationService.Get();
            return configuration.Investors;
        }
    }
}
