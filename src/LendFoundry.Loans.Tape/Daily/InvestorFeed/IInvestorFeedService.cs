﻿using System;
using System.Collections.Generic;
using LendFoundry.Loans.TransactionLog.Client;

namespace LendFoundry.Loans.Tape.Daily.InvestorFeed
{
    public interface IInvestorFeedService
    {
        void Update(string loanReferenceNumber);
        void Update(string loanReferenceNumber, string transactionId);
        void Update(TransactionInfo transaction);
        IEnumerable<IInvestorFeedEntry> BuildTape(DateTimeOffset endDate);
        IEnumerable<IInvestorFeedEntry> BuildTape(DateTimeOffset startDate, DateTimeOffset endDate);
    }
}
