﻿using System;

namespace LendFoundry.Loans.Tape.Daily
{
    public class PaymentEconomics
    {
        public double? OpeningBalance { get; set; }
        public double? Principal { get; set; }
        public double? Interest { get; set; }
        public double? EndingBalance { get; set; }
        public DateTimeOffset? DueDate { get; set; }
    }
}
