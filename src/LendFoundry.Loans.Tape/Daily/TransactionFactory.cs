﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Loans.Tape.Daily.Transactions;
using LendFoundry.Loans.TransactionLog.Client;
using System;

namespace LendFoundry.Loans.Tape.Daily
{
    public class TransactionFactory
    {
        public TransactionFactory(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        private ITenantTime TenantTime { get; }

        public TransactionInfo Create(EventInfo eventInfo)
        {
            if (eventInfo == null)
                throw new ArgumentNullException(nameof(eventInfo));

            var transaction = eventInfo.Cast<TransactionLog.Client.TransactionInfo>();

            /* convert dates to tenant timezone because the deserialization mechanism
             * creates dates using the local machine's timezone  */
            transaction.Date = ToTenantTime(transaction.Date);
            transaction.Timestamp = ToTenantTime(transaction.Timestamp);

            return transaction;
        }

        private DateTimeOffset ToTenantTime(DateTimeOffset date)
        {
            return TenantTime.Create(date.Year, date.Month, date.Day);
        }
    }
}
