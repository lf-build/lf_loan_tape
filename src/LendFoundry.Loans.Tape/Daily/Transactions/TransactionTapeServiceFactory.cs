﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Tape.Configuration;
using LendFoundry.Loans.TransactionLog.Client;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Tape.Daily.Transactions
{
    public class TransactionTapeServiceFactory : ITransactionTapeServiceFactory
    {
        public TransactionTapeServiceFactory(
            ITransactionLogServiceFactory transactionLogServiceFactory,
            ILoanServiceFactory loanServiceFactory,
            ITransactionTapeRepositoryFactory transactionTapeRepositoryFactory,
            IConfigurationServiceFactory<TapeServiceConfiguration> configurationServiceFactory,
            ILoggerFactory loggerFactory)
        {
            TransactionLogServiceFactory = transactionLogServiceFactory;
            LoanServiceFactory = loanServiceFactory;
            TransactionTapeRepositoryFactory = transactionTapeRepositoryFactory;
            ConfigurationServiceFactory = configurationServiceFactory;
            LoggerFactory = loggerFactory;
        }

        private IConfigurationServiceFactory<TapeServiceConfiguration> ConfigurationServiceFactory { get; }
        private ITransactionLogServiceFactory TransactionLogServiceFactory { get; }
        private ILoanServiceFactory LoanServiceFactory { get; }
        private ITransactionTapeRepositoryFactory TransactionTapeRepositoryFactory { get; }
        private ILoggerFactory LoggerFactory { get; }

        public ITransactionTapeService Create(ITokenReader reader)
        {
            var configurationService = ConfigurationServiceFactory.Create(reader);
            var configuration = configurationService.Get();
            var transactionTapeRepository = TransactionTapeRepositoryFactory.Create(reader);

            return new TransactionTapeService
            (
                TransactionLogServiceFactory.Create(reader),
                new TransactionEntryFactory(configuration, transactionTapeRepository, LoggerFactory.CreateLogger()),
                transactionTapeRepository,
                configuration.DailyTransactionTape,
                LoanServiceFactory.Create(reader),
                LoggerFactory.CreateLogger()
            );
        }
    }
}
