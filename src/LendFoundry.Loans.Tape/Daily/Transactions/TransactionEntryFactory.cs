﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Tape.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using TransactionInfo = LendFoundry.Loans.TransactionLog.Client.TransactionInfo;
using LendFoundry.Loans.TransactionLog.Client;

namespace LendFoundry.Loans.Tape.Daily.Transactions
{
    public class TransactionEntryFactory : ITransactionEntryFactory
    {
        public TransactionEntryFactory(TapeServiceConfiguration configuration, ITransactionTapeRepository repository, ILogger logger)
        {
            Configuration = configuration;
            Repository = repository;
            Logger = logger.Scope(this);
        }

        private TapeServiceConfiguration Configuration { get; }
        private ITransactionTapeRepository Repository { get; }
        private ILogger Logger { get; }

        public ITransactionEntry Create(TransactionInfo transaction, ILoan loan, IEnumerable<IInstallment> schedule)
        {
            if (loan == null)
                throw new ArgumentException($"Loan not found: {transaction.LoanReference}", nameof(transaction.LoanReference));

            var tapeLogger = Logger.ForTransactionTape(transaction.LoanReference, transaction.Id);
            var installment = GetInstallment(transaction.PaymentId, schedule);

            if (installment == null)
                tapeLogger.Warn("Installment not found for transaction {Id}", transaction);

            var paymentEconomics = GetPaymentEconomics(transaction, installment, tapeLogger);

            var entry = new TransactionEntry() as ITransactionEntry;

            entry.LoanReferenceNumber = transaction.LoanReference;
            entry.TransactionId = transaction.Id;
            entry.TransactionCode = transaction.Code;
            entry.PaymentId = transaction.PaymentId;
            entry.FeedDate = transaction.Timestamp;
            entry.TransactionDate = transaction.Date;
            entry.TransactionAmount = ChangeOperationSign(transaction.Amount, transaction).Value;
            entry.PaymentType = GetPaymentType(transaction, installment);
            entry.OpeningBalance = IsReversal(transaction) ? paymentEconomics?.EndingBalance : paymentEconomics?.OpeningBalance;
            entry.Principal = ChangeOperationSign(paymentEconomics?.Principal, transaction);
            entry.Interest = ChangeOperationSign(paymentEconomics?.Interest, transaction);
            entry.EndingBalance = IsReversal(transaction) ? paymentEconomics?.OpeningBalance : paymentEconomics?.EndingBalance;
            entry.Investor = loan.Investor?.Id;
            entry.OriginationDate = loan.Terms.OriginationDate;
            entry.FundedDate = loan.Terms.FundedDate;
            entry.OriginationFee = GetOriginationFee(loan);
            entry.LoanAmount = loan.Terms.LoanAmount;
            entry.FundedAmount = GetFundedAmount(loan);
            entry.InterestRate = loan.Terms.Rate;
            entry.LoanTerm = loan.Terms.Term;
            entry.LateFeesPaid = null;
            entry.OtherFeesPaid = null;
            entry.TotalFeesPaid = null;
            entry.ClosedDate = null;

            return entry;
        }

        private double? ChangeOperationSign(double? amount, TransactionInfo transaction)
        {
            if (amount == null) return null;
            if (amount == 0.0) return 0.0;
            return amount * (IsReversal(transaction.Code) ? -1 : 1);
        }

        private PaymentEconomics GetPaymentEconomics(TransactionInfo transaction, IInstallment installment, ILogger tapeLogger)
        {
            if (IsPaymentReversal(transaction))
            {
                var payment = Repository
                    .All(t => t.PaymentId == transaction.PaymentId && t.TransactionId != transaction.Id)
                    .Await()
                    .FirstOrDefault();

                if (payment != null)
                {
                    return new PaymentEconomics
                    {
                        OpeningBalance = payment.OpeningBalance,
                        Principal = payment.Principal,
                        Interest = payment.Interest,
                        EndingBalance = payment.EndingBalance
                    };
                }
                else
                {
                    tapeLogger.Warn("Payment transaction not found for reversal: {Id}", transaction);
                }
            }

            if (installment != null)
            {
                return new PaymentEconomics
                {
                    OpeningBalance = installment.OpeningBalance,
                    Principal = installment.Principal,
                    Interest = installment.Interest,
                    EndingBalance = installment.EndingBalance
                };
            }

            return null;
        }

        private bool IsPaymentReversal(TransactionInfo transaction)
        {
            return IsReversal(transaction) && !string.IsNullOrWhiteSpace(transaction.PaymentId);
        }

        private bool IsReversal(TransactionInfo transaction)
        {
            return IsReversal(transaction.Code);
        }

        private bool IsReversal(string transactionCode)
        {
            return Configuration.TransactionCodes.Reversals.Any(reversalCode => transactionCode == reversalCode);
        }

        private double GetFundedAmount(ILoan loan)
        {
            var originationFee = GetOriginationFee(loan);
            var loanAmount = loan.Terms.LoanAmount;
            return Math.Round(loanAmount - originationFee, 2, MidpointRounding.AwayFromZero);
        }

        private double GetOriginationFee(ILoan loan)
        {
            var feeAmount = loan.Terms.Fees
                .Where(f => f.Code == Configuration.TransactionCodes.OriginationFee
                    /* There are some old loans in production with null fee code.
                     * Remove this condition as soon as we fix them: */
                    || f.Code == null) 
                .Sum(f => f.Amount);

            return Math.Round(feeAmount, 2, MidpointRounding.AwayFromZero);
        }

        private IInstallment GetInstallment(string paymentId, IEnumerable<IInstallment> schedule)
        {
            if (string.IsNullOrWhiteSpace(paymentId))
                return null;

            return schedule.SingleOrDefault(i => i.PaymentId == paymentId);
        }

        private string GetPaymentType(TransactionInfo transaction, IInstallment installment)
        {
            var paymentTypeOverride = Configuration.DailyTransactionTape.PaymentTypeOverrides?.SingleOrDefault(t => t.Code == transaction.Code);

            if (paymentTypeOverride != null)
                return paymentTypeOverride.Name;

            if (!IsReversal(transaction.Code) && installment != null)
                return GetPaymentType(installment);

            return transaction.Description;
        }

        private static string GetPaymentType(IInstallment installment)
        {
            return installment.Type == InstallmentType.Scheduled ? "Scheduled" : "Extra";
        }
    }
}
