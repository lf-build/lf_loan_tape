﻿using LendFoundry.Loans.TransactionLog.Client;
using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Tape.Daily.Transactions
{
    public interface ITransactionTapeService
    {
        void Update(string loanReferenceNumber);
        void Update(string loanReferenceNumber, string transactionId);
        void Update(TransactionInfo transaction);
        IEnumerable<ITransactionEntry> BuildTape(DateTimeOffset endDate);
        IEnumerable<ITransactionEntry> BuildTape(DateTimeOffset startDate, DateTimeOffset endDate);
    }
}
