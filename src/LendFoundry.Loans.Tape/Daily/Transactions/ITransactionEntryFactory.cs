﻿using LendFoundry.Loans.TransactionLog.Client;
using System.Collections.Generic;

namespace LendFoundry.Loans.Tape.Daily.Transactions
{
    public interface ITransactionEntryFactory
    {
        ITransactionEntry Create(TransactionInfo transaction, ILoan loan, IEnumerable<IInstallment> schedule);
    }
}
