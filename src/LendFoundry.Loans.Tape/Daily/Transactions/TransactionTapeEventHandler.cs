﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Tape.Configuration;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Tape.Daily.Transactions
{
    public class TransactionTapeEventHandler : EventHandler
    {
        public TransactionTapeEventHandler
        (
            TenantInfo tenant,
            TapeServiceConfiguration serviceConfiguration,
            ITokenHandler tokenHandler,
            ITransactionTapeServiceFactory transactionTapeServiceFactory,
            TransactionFactory transactionFactory,
            ILoggerFactory loggerFactory
        ) : base(tenant.Id, loggerFactory.CreateLogger())
        {
            Events = serviceConfiguration.DailyTransactionTape.Events ?? new TransactionEvent[] { };
            TransactionTapeService = transactionTapeServiceFactory.Create(CreateTokenReader(tokenHandler, tenant.Id));
            TransactionFactory = transactionFactory;
            Logger = loggerFactory.CreateLogger().Scope(this);

            if(!Events.Any())
            {
                Logger.Warn("No events will be handled: configuration does not specify a list of events to listen for");
            }
        }

        private TransactionEvent[] Events { get; }
        private ITransactionTapeService TransactionTapeService { get; }
        private TransactionFactory TransactionFactory { get; }
        private ILogger Logger { get; }

        public override IEnumerable<string> AcceptedEvents => Events.Select(e => e.Name);

        protected override void Handle(EventInfo eventInfo)
        {
            var @event = Events.Single(e => e.Name == eventInfo.Name);
            Handle(@event, eventInfo, GetTransactionFrom(eventInfo, @event));
        }

        private TransactionLog.Client.TransactionInfo GetTransactionFrom(EventInfo eventInfo, TransactionEvent @event)
        {
            return @event.ContainsFullTransactionInfo ? TransactionFactory.Create(eventInfo) : null;
        }

        private void Handle(TransactionEvent @event, EventInfo eventInfo, TransactionLog.Client.TransactionInfo transaction)
        {
            var loanReferenceNumber = StringInterpolation.Interpolate(@event.LoanReferenceNumber, eventInfo);
            var transactionId = StringInterpolation.Interpolate(@event.TransactionId, eventInfo);
            var tapeLogger = Logger.ForTransactionTape(loanReferenceNumber, transactionId);

            if (string.IsNullOrWhiteSpace(loanReferenceNumber))
            {
                tapeLogger.Info("Ignoring event {eventName}. Reason: loan reference number is null or empty", new { eventName = @event.Name, LoanReferenceNumber = loanReferenceNumber });
            }
            else
            {
                tapeLogger.Info("Handling event {eventName}", new { eventName = @event.Name, LoanReferenceNumber = loanReferenceNumber });

                try
                {
                    if (string.IsNullOrWhiteSpace(transactionId))
                    {
                        TransactionTapeService.Update(loanReferenceNumber);
                    }
                    else if (transaction != null)
                    {
                        TransactionTapeService.Update(transaction);
                    }
                    else
                    {
                        TransactionTapeService.Update(loanReferenceNumber, transactionId);
                    }
                }
                catch (Exception exception)
                {
                    tapeLogger.Error("Error handling transaction event {eventName}", exception, new { eventName = @event.Name, LoanReferenceNumber = loanReferenceNumber });
                }
            }
        }

        private static ITokenReader CreateTokenReader(ITokenHandler tokenHandler, string tenantId)
        {
            var token = tokenHandler.Issue(tenantId, Settings.ServiceName);
            return new StaticTokenReader(token.Value);
        }
    }
}
