﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Tape.Configuration;
using LendFoundry.Loans.TransactionLog.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Tape.Daily.Transactions
{
    public class TransactionTapeService : ITransactionTapeService
    {
        public TransactionTapeService(ITransactionLogService transactionLogClient, ITransactionEntryFactory entryFactory, ITransactionTapeRepository repository, TransactionTapeConfiguration configuration, ILoanService loanService, ILogger logger)
        {
            Configuration = configuration;
            TransactionLogClient = transactionLogClient;
            EntryFactory = entryFactory;
            Repository = repository;
            LoanService = loanService;
            Logger = logger.Scope(this);
        }

        private TransactionTapeConfiguration Configuration { get; }
        private ITransactionLogService TransactionLogClient { get; }
        private ITransactionEntryFactory EntryFactory { get; }
        private ITransactionTapeRepository Repository { get; }
        private ILoanService LoanService { get; }
        private ILogger Logger { get; }

        public void Update(string loanReferenceNumber)
        {
            var loan = GetLoan(loanReferenceNumber);
            var schedule = LoanService.GetPaymentSchedule(loanReferenceNumber);
            var transactions = TransactionLogClient.GetTransactions(loanReferenceNumber);

            foreach(var transaction in transactions)
            {
                var entry = EntryFactory.Create(transaction, loan, schedule);
                Repository.AddOrUpdate(entry);
            }
        }

        public void Update(string loanReferenceNumber, string transactionId)
        {
            var transaction = TransactionLogClient.GetTransactionByLoanAndTransaction(loanReferenceNumber, transactionId);
            if (transaction == null)
                throw new ArgumentException($"Transaction not found: id={transactionId} loanReferenceNumber={loanReferenceNumber}");

            Update(transaction);
        }
        
        public void Update(TransactionLog.Client.TransactionInfo transaction)
        {
            var loan = GetLoan(transaction.LoanReference);
            var schedule = LoanService.GetPaymentSchedule(transaction.LoanReference);
            var entry = EntryFactory.Create(transaction, loan, schedule);
            Repository.AddOrUpdate(entry);
        }

        private ILoan GetLoan(string loanReferenceNumber)
        {
            var info = LoanService.GetLoanInfo(loanReferenceNumber);
            return info.Loan;
        }

        public IEnumerable<ITransactionEntry> BuildTape(DateTimeOffset endDate)
        {
            return Sort(Repository.Query(endDate, IgnoredTransactionCodes));
        }

        public IEnumerable<ITransactionEntry> BuildTape(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            return Sort(Repository.Query(startDate, endDate, IgnoredTransactionCodes));
        }

        private IEnumerable<ITransactionEntry> Sort(IEnumerable<ITransactionEntry> transactions)
        {
            return transactions
                .OrderBy(t => t.FeedDate)
                .ThenBy(t => t.TransactionDate)
                .ThenBy(t => t.LoanReferenceNumber)
                .ThenBy(t => t.EndingBalance);
        }
        private string[] IgnoredTransactionCodes => Configuration.IgnoredTransactions.Select(t => t.Code).ToArray();
    }
}
