﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Tape.Daily.Transactions
{
    public interface ITransactionTapeServiceFactory
    {
        ITransactionTapeService Create(ITokenReader tokenReader);
    }
}
