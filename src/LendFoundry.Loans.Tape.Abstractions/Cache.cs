﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Tape
{
    public class Cache
    {
        private readonly IDictionary<string, Entry> Entries = new Dictionary<string, Entry>();

        public T Get<T>(string key, long lifetimeInMillis, Func<T> source)
        {
            return Get(key, DateTime.Now.AddMilliseconds(lifetimeInMillis), source);
        }

        public T Get<T>(string key, DateTime expiration, Func<T> source)
        {
            lock (Entries)
            {
                var entry = FindEntry(key);

                if (entry == null)
                {
                    Entries[key] = entry = new Entry(source(), expiration);
                }

                return (T) entry.Value;
            }
        }

        private Entry FindEntry(string key)
        {
            if (Entries.ContainsKey(key))
            {
                var entry = Entries[key];

                if (!entry.HasExpired)
                {
                    return entry;
                }
            }

            return null;
        }

        private class Entry
        {
            public Entry(object value, DateTime expiration)
            {
                Value = value;
                Expiration = expiration;
            }

            public object Value { get; }
            public DateTime Expiration { get; }
            public bool HasExpired => DateTime.Now >= Expiration;
        }
    }
}
