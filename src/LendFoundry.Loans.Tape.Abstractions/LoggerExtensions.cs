﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Tape.Daily.InvestorFeed;
using LendFoundry.Loans.Tape.Daily.Transactions;
using LendFoundry.Loans.Tape.Monthly;
using System;

namespace LendFoundry.Loans.Tape
{
    public static class LoggerExtensions
    {
        public static ILogger Scope(this ILogger logger, string scope)
        {
            if (logger is ScopedLogger)
            {
                return ((ScopedLogger)logger).AddScope(scope);
            }

            return new ScopedLogger(logger, scope);
        }

        public static ILogger Scope(this ILogger logger, object scope)
        {
            return Scope(logger, scope.GetType().Name);
        }

        public static ILogger ForLoanTape(this ILogger logger, ILoanTape tape)
        {
            return logger.Scope(tape.LoanReferenceNumber).Scope(tape.CreationDate.Time.ToString("yyyy-MM-dd"));
        }

        public static ILogger ForLoanTape(this ILogger logger, string loanReferenceNumber, DateTimeOffset referenceDate)
        {
            return logger.Scope(loanReferenceNumber).Scope(referenceDate.ToString("yyyy-MM-dd"));
        }

        public static ILogger ForTransactionTape(this ILogger logger, ITransactionEntry transaction)
        {
            return logger.Scope(transaction.LoanReferenceNumber).Scope(transaction.TransactionId);
        }

        public static ILogger ForTransactionTape(this ILogger logger, string loanReferenceNumber, string transactionId)
        {
            return logger.Scope(loanReferenceNumber).Scope(transactionId);
        }

        public static ILogger ForInvestorFeed(this ILogger logger, IInvestorFeedEntry entry)
        {
            return logger.Scope(entry.LoanReferenceNumber).Scope(entry.TransactionId);
        }

        public static ILogger ForInvestorFeed(this ILogger logger, string loanReferenceNumber, string transactionId)
        {
            return logger.Scope(loanReferenceNumber).Scope(transactionId);
        }
    }
}
