﻿namespace LendFoundry.Loans.Tape.Events
{
    public class TapeUploadFailed
    {
        public TapeUploadFailed(string tapeName)
        {
            TapeName = tapeName;
        }

        public string TapeName { get; set; }
    }
}
