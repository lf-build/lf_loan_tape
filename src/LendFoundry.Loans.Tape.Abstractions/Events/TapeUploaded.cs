﻿namespace LendFoundry.Loans.Tape.Events
{
    public class TapeUploaded
    {
        public TapeUploaded(string tapeName)
        {
            TapeName = tapeName;
        }

        public string TapeName { get; set; }
    }
}
