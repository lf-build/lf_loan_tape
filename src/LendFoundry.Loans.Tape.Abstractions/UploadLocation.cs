﻿namespace LendFoundry.Loans.Tape
{
    public class UploadLocation
    {
        public string FileName { get; set; }
        public string FileId { get; set; }
    }
}
