﻿using LendFoundry.Loans.Tape.Configuration;

namespace LendFoundry.Loans.Tape.Monthly
{
    public class LoanTapeConfiguration
    {
        public EventMapping[] Events { get; set; }
        public string FicoScoreName { get; set; }
        public string Projection { get; set; }
        public TransactionInfo[] FeeTransactions { get; set; }
        public TransactionInfo[] LateFeeTransactions { get; set; }
        public TransactionInfo[] NsfFeeTransactions { get; set; }
        public TransactionInfo[] CheckProcessingFeeTransactions { get; set; }
    }
}
