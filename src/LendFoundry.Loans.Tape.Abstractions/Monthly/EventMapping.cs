﻿namespace LendFoundry.Loans.Tape.Monthly
{
    public class EventMapping
    {
        public string Name { get; set; }
        public string ReferenceDate { get; set; }
        public string ReferenceNumber { get; set; }
    }
}
