﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Tape.Monthly
{
    public interface ILoanTapeServiceFactory
    {
        ILoanTapeService Create(ITokenReader reader);
    }
}
