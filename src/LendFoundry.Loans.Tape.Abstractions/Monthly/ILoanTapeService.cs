﻿using System;

namespace LendFoundry.Loans.Tape.Monthly
{
    public interface ILoanTapeService
    {
        void GenerateTape(string loanReferenceNumber, DateTimeOffset referenceDate);
        ILoanTape[] GetByLoan(string referenceNumber);
        ILoanTape GetByLoan(string referenceNumber, string yearAndMonth);
        ILoanTape[] GetByYearMonth(string yearAndMonth);
        string GenerateFileName(string yearAndMonth, string sufixName);
    }
}