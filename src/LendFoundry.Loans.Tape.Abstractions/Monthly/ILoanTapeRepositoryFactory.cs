﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Tape.Monthly
{
    public interface ILoanTapeRepositoryFactory
    {
        ILoanTapeRepository Create(ITokenReader reader);
    }
}