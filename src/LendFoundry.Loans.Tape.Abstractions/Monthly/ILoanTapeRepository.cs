﻿namespace LendFoundry.Loans.Tape.Monthly
{
    public interface ILoanTapeRepository
    {
        void AddOrUpdate(ILoanTape tape);
        ILoanTape[] GetByLoan(string referenceNumber);
        ILoanTape GetByLoan(string referenceNumber, string yearAndMonth);
        ILoanTape[] GetByYearMonth(string yearAndMonth);
    }
}