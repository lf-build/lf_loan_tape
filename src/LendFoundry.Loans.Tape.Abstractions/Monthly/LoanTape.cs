﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Globalization;

namespace LendFoundry.Loans.Tape.Monthly
{
    public class LoanTape : Aggregate, ILoanTape
    {
        public string EntityId { get; set; }
        public string EntityName { get; set; }
        public string LoanReferenceNumber { get; set; }
        public TimeBucket CreationDate { get; set; }
        public DateTimeOffset OriginationDate { get; set; }
        public DateTimeOffset FundingDate { get; set; }
        public DateTimeOffset PurchaseDate { get; set; }
        public DateTimeOffset? MaturityDate { get; set; }
        public string BorrowerLastName { get; set; }
        public string BorrowerFirstName { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string OriginationFicoScore { get; set; }
        public string UpdatedFicoScore { get; set; }
        public double OriginationFeePercent { get; set; }
        public double OriginationFeeAmount { get; set; }
        public double LoanAmount { get; set; }
        public double FundedAmount { get; set; }
        public double InterestRate { get; set; }
        public int Term { get; set; }
        public double MonthlyPayment { get; set; }
        public string RiskGrade { get; set; }
        public double? DTI { get; set; }
        public double MonthlyIncome { get; set; }
        public string LoanPurpose { get; set; }
        public string HomeOwnership { get; set; }
        public string CampaignCode { get; set; }
        public double Principal { get; set; }
        public double Interest { get; set; }
        public double FeesThisMonth { get; set; }
        public DateTimeOffset? SchedulePaymentDue { get; set; }
        public DateTimeOffset? LastPaymentReceivedDate { get; set; }
        public string LoanStatus { get; set; }
        public int DaysPastDue { get; set; }
        public int LoanAge { get; set; }
        public double? PrincipalRecovery { get; set; }
        public DateTimeOffset? LastPaymentDueDate { get; set; }
        public DateTimeOffset? NextPaymentDueDate { get; set; }
        public bool? IsLoanModified { get; set; }
        public DateTimeOffset? ChargeOffDate { get; set; }
        public double ChargeOffAmount { get; set; }
        public double? ServicingFeeAccrued { get; set; }
        public double? FraudAmount { get; set; }
        public double? PrincipalAdjustment { get; set; }
        public double? InterestAdjustment { get; set; }
        public double LateFeeAssessed { get; set; }
        public double LateFeePaid { get; set; }
        public double NSFFeeAssessed { get; set; }
        public double NSFFeePaid { get; set; }
        public double PastDueAmount { get; set; }
        public double AverageDailyBalance { get; set; }
        public double SimpleInterestAccrued { get; set; }
        public double SimpleInterestAccruedLastMonth { get; set; }
        public double CumulativeInterestPaid { get; set; }
        /* fields to use inside InvestorTape */
        public string MonthOfServicingReport { get; set; }
        public double BeginningPrincipalBalance { get; set; }
        public double PrincipalPaidDuringThePeriod { get; set; }
        public double InterestPaidDuringThePeriod { get; set; }
        public double FeesPaidDuringThePeriod { get; set; }
        public double SchedulePaymentDueInPeriod { get; set; }
        public DateTimeOffset? DateOfLastPaymentReceived { get; set; }
        public int DaysDelinquent { get; set; }
        public double EndingPrincipalBalance { get; set; }
        public double? TrustInterestPurchased { get; set; }
        public string ReportDate { get; set; }
        public string SubPoolId { get; set; }
        public string BeginningSubpoolId { get; set; }
        public DateTimeOffset? ConfirmOfFraudDate { get; set; }
        public double? BoardingAmount { get; set; }
        public string OwnershipTransfers { get; set; }
        public string ErrorCheck { get; set; }
        public string Vintage { get { return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(OriginationDate.ToString("yy-MMM")); } }
        public double PaymentRecovery { get; set; }
        public double TotalBorrowerPayment { get; set; }
        public double NetInterestAccrued { get; set; }
        public double CheckProcessingFee { get; set; }
    }
}