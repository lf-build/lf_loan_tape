﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Loans.Tape.Monthly
{
    public interface ILoanTape : IAggregate
    {
        string EntityId { get; set; }
        string EntityName { get; set; }
        string LoanReferenceNumber { get; set; }
        TimeBucket CreationDate { get; set; }
        DateTimeOffset OriginationDate { get; set; }
        DateTimeOffset FundingDate { get; set; }
        DateTimeOffset PurchaseDate { get; set; }
        DateTimeOffset? MaturityDate { get; set; }
        string BorrowerLastName { get; set; }
        string BorrowerFirstName { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
        string OriginationFicoScore { get; set; }
        string UpdatedFicoScore { get; set; }
        double OriginationFeePercent { get; set; }
        double OriginationFeeAmount { get; set; }
        double LoanAmount { get; set; }
        double FundedAmount { get; set; }
        double InterestRate { get; set; }
        int Term { get; set; }
        double MonthlyPayment { get; set; }
        string RiskGrade { get; set; }
        double? DTI { get; set; }
        double MonthlyIncome { get; set; }
        string LoanPurpose { get; set; }
        string HomeOwnership { get; set; }
        string CampaignCode { get; set; }
        double Principal { get; set; }
        double Interest { get; set; }
        double FeesThisMonth { get; set; }
        DateTimeOffset? SchedulePaymentDue { get; set; }
        DateTimeOffset? LastPaymentReceivedDate { get; set; }
        string LoanStatus { get; set; }
        int DaysPastDue { get; set; }        
        int LoanAge { get; set; }
        double? PrincipalRecovery { get; set; }
        DateTimeOffset? LastPaymentDueDate { get; set; }
        DateTimeOffset? NextPaymentDueDate { get; set; }
        bool? IsLoanModified { get; set; }
        DateTimeOffset? ChargeOffDate { get; set; }
        double ChargeOffAmount { get; set; }
        double? ServicingFeeAccrued { get; set; }
        double? FraudAmount { get; set; }
        double? PrincipalAdjustment { get; set; }
        double? InterestAdjustment { get; set; }
        double LateFeeAssessed { get; set; }
        double LateFeePaid { get; set; }
        double NSFFeeAssessed { get; set; }
        double NSFFeePaid { get; set; }
        double PastDueAmount { get; set; }
        double AverageDailyBalance { get; set; }
        double SimpleInterestAccrued { get; set; }
        double SimpleInterestAccruedLastMonth { get; set; }
        double CumulativeInterestPaid { get; set; }
        string MonthOfServicingReport { get; set; }
        double BeginningPrincipalBalance { get; set; }
        double PrincipalPaidDuringThePeriod { get; set; }
        double InterestPaidDuringThePeriod { get; set; }
        double FeesPaidDuringThePeriod { get; set; }
        double SchedulePaymentDueInPeriod { get; set; }
        DateTimeOffset? DateOfLastPaymentReceived { get; set; }
        int DaysDelinquent { get; set; }
        double EndingPrincipalBalance { get; set; }        
        double? TrustInterestPurchased { get; set; }
        string ReportDate { get; set; }
        string SubPoolId { get; set; }
        string BeginningSubpoolId { get; set; }
        DateTimeOffset? ConfirmOfFraudDate { get; set; }
        double? BoardingAmount { get; set; }
        string OwnershipTransfers { get; set; }
        string ErrorCheck { get; set; }
        string Vintage { get; }
        double PaymentRecovery { get; set; }
        double TotalBorrowerPayment { get; set; }
        double NetInterestAccrued { get; set; }
        double CheckProcessingFee { get; set; }
    }
}