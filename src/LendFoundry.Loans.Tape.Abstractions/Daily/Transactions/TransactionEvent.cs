﻿namespace LendFoundry.Loans.Tape.Daily.Transactions
{
    public class TransactionEvent
    {
        public string Name { get; set; }
        public string LoanReferenceNumber { get; set; }
        public string TransactionId { get; set; }
        public bool ContainsFullTransactionInfo { get; set; }
    }
}
