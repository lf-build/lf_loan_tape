﻿using System;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Loans.Tape.Daily.Transactions
{
    public interface ITransactionEntry : IAggregate
    {
        string LoanReferenceNumber { get; set; }
        string TransactionId { get; set; }
        string TransactionCode { get; set; }
        string PaymentId { get; set; }
        DateTimeOffset FeedDate { get; set; }
        DateTimeOffset TransactionDate { get; set; }
        int TransactionNumber { get; set; }
        double OriginationFee { get; set; }
        double FundedAmount { get; set; }
        double TransactionAmount { get; set; }
        string PaymentType { get; set; }
        double? OpeningBalance { get; set; }
        double? Principal { get; set; }
        double? Interest { get; set; }
        double? EndingBalance { get; set; }
        string Investor { get; set; }
        DateTimeOffset OriginationDate { get; set; }
        DateTimeOffset FundedDate { get; set; }
        double LoanAmount { get; set; }
        double InterestRate { get; set; }
        int LoanTerm { get; set; }
        double? LateFeesPaid { get; set; }
        double? OtherFeesPaid { get; set; }
        double? TotalFeesPaid { get; set; }
        DateTimeOffset? ClosedDate { get; set; }
    }
}
