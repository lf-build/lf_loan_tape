﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Loans.Tape.Daily.Transactions
{
    public interface ITransactionTapeRepository : IRepository<ITransactionEntry>
    {
        void AddOrUpdate(ITransactionEntry entry);
        IEnumerable<ITransactionEntry> Query(DateTimeOffset endDate, string[] ignoredTransactionCodes);
        IEnumerable<ITransactionEntry> Query(DateTimeOffset startDate, DateTimeOffset endDate, string[] ignoredTransactionCodes);
    }
}
