﻿using System;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Loans.Tape.Daily.Transactions
{
    public class TransactionEntry : Aggregate, ITransactionEntry
    {
        public string LoanReferenceNumber { get; set; }
        public string TransactionId { get; set; }
        public string PaymentId { get; set; }
        public DateTimeOffset FeedDate { get; set; }
        public DateTimeOffset TransactionDate { get; set; }
        public int TransactionNumber { get; set; }
        public double OriginationFee { get; set; }
        public double FundedAmount { get; set; }
        public double TransactionAmount { get; set; }
        public string PaymentType { get; set; }
        public double? OpeningBalance { get; set; }
        public double? Principal { get; set; }
        public double? Interest { get; set; }
        public double? EndingBalance { get; set; }
        public string Investor { get; set; }
        public DateTimeOffset OriginationDate { get; set; }
        public DateTimeOffset FundedDate { get; set; }
        public double LoanAmount { get; set; }
        public double InterestRate { get; set; }
        public int LoanTerm { get; set; }
        public double? LateFeesPaid { get; set; }
        public double? OtherFeesPaid { get; set; }
        public double? TotalFeesPaid { get; set; }
        public DateTimeOffset? ClosedDate { get; set; }
        public string TransactionCode { get; set; }
    }
}
