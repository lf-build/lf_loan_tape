﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Tape.Daily.Transactions
{
    public interface ITransactionTapeRepositoryFactory
    {
        ITransactionTapeRepository Create(ITokenReader reader);
    }
}
