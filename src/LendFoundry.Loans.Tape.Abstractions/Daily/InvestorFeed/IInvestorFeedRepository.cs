﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Loans.Tape.Configuration;

namespace LendFoundry.Loans.Tape.Daily.InvestorFeed
{
    public interface IInvestorFeedRepository : IRepository<IInvestorFeedEntry>
    {
        void AddOrUpdate(IInvestorFeedEntry entry);
        IEnumerable<IInvestorFeedEntry> Query(DateTimeOffset endDate, string[] acceptedInvestors, string[] ignoredTransactionCodes);
        IEnumerable<IInvestorFeedEntry> Query(DateTimeOffset startDate, DateTimeOffset endDate, string[] acceptedInvestors, string[] ignoredTransactionCodes);
    }
}
