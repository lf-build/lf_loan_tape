﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Loans.Tape.Daily.InvestorFeed
{
    public class InvestorFeedEntry : Aggregate, IInvestorFeedEntry
    {
        public string LoanReferenceNumber { get; set; }
        public string TransactionId { get; set; }
        public string PaymentId { get; set; }
        public DateTimeOffset FeedDate { get; set; }
        public string Entity { get; set; }
        public string TransactionNumber { get; set; }
        public string ReversalNumber { get; set; }
        public DateTimeOffset TransactionDate { get; set; }
        public DateTimeOffset OriginationDate { get; set; }
        public string BorrowerFirstName { get; set; }
        public string BorrowerLastName { get; set; }
        public DateTimeOffset? DueDate { get; set; }
        public int LoanAge { get; set; }
        public double TransactionAmount { get; set; }
        public string Description { get; set; }
        public string PaymentMethod { get; set; }
        public double Principal { get; set; }
        public double Interest { get; set; }
        public double Fees { get; set; }
        public double LateCharges { get; set; }
        public double EffectivePaymentAmount { get; set; }
        public string Investor { get; set; }
        public string TransactionCode { get; set; }
    }
}
