﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Tape.Daily.InvestorFeed
{
    public interface IInvestorFeedRepositoryFactory
    {
        IInvestorFeedRepository Create(ITokenReader reader);
    }
}
