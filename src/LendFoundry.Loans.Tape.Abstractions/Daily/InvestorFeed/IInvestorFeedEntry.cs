﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Loans.Tape.Daily.InvestorFeed
{
    public interface IInvestorFeedEntry : IAggregate
    {
        string LoanReferenceNumber { get; set; }
        string Investor { get; set; }
        string TransactionId { get; set; }
        string TransactionCode { get; set; }
        string PaymentId { get; set; }
        DateTimeOffset FeedDate { get; set; }
        string Entity { get; set; }
        string TransactionNumber { get; set; }
        string ReversalNumber { get; set; }
        DateTimeOffset TransactionDate { get; set; }
        DateTimeOffset OriginationDate { get; set; }
        string BorrowerFirstName { get; set; }
        string BorrowerLastName { get; set; }
        DateTimeOffset? DueDate { get; set; }
        int LoanAge { get; set; }
        double TransactionAmount { get; set; }
        string Description { get; set; }
        string PaymentMethod { get; set; }
        double Principal { get; set; }
        double Interest { get; set; }
        double Fees { get; set; }
        double LateCharges { get; set; }
        double EffectivePaymentAmount { get; set; }
    }
}
