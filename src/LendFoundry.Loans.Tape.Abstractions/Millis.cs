﻿namespace LendFoundry.Loans.Tape
{
    public static class Millis
    {
        public static long Minutes(int minutes) => minutes * 60L * 1000L;
    }
}
