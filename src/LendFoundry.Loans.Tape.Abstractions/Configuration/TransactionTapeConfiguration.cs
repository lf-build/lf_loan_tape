﻿using LendFoundry.Loans.Tape.Daily.Transactions;

namespace LendFoundry.Loans.Tape.Configuration
{
    public class TransactionTapeConfiguration
    {
        public TransactionEvent[] Events { get; set; }
        public TransactionInfo[] IgnoredTransactions { get; set; }
        public TransactionInfo[] PaymentTypeOverrides { get; set; }
        public string Projection { get; set; }
        public UploadLocation UploadLocation { get; set; }
    }
}
