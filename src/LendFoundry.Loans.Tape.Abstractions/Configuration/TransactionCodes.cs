﻿namespace LendFoundry.Loans.Tape.Configuration
{
    public class TransactionCodes
    {
        public string OriginationFee { get; set; }
        public string ChargeOff { get; set; }
        public string[] DebitOperations { get; set; }
        public string[] Reversals { get; set; }
    }
}
