﻿using LendFoundry.Loans.Tape.Daily.Transactions;

namespace LendFoundry.Loans.Tape.Configuration
{
    public class InvestorFeedConfiguration
    {
        public TransactionEvent[] Events { get; set; }
        public TransactionInfo[] IgnoredTransactions { get; set; }
        public string[] AcceptedInvestors { get; set; }
        public string Projection { get; set; }
        public UploadLocation UploadLocation { get; set; }
    }
}
