﻿namespace LendFoundry.Loans.Tape.Configuration
{
    public class BoxConfiguration
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string EnterpriseId { get; set; }
        public string PrivateKey { get; set; }
        public string PrivateKeyPassword { get; set; }
        public string PublicKeyId { get; set; }
        public string UserId { get; set; }
    }
}
