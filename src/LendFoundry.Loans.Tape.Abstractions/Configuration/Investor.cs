﻿namespace LendFoundry.Loans.Tape.Configuration
{
    public class Investor
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
