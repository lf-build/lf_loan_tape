﻿using LendFoundry.Loans.Tape.Monthly;

namespace LendFoundry.Loans.Tape.Configuration
{
    public class TapeServiceConfiguration
    {
        public LoanTapeConfiguration MonthlyLoanTape { get; set; }
        public TransactionTapeConfiguration DailyTransactionTape { get; set; }
        public InvestorFeedConfiguration DailyInvestorFeed { get; set; }
        public TransactionCodes TransactionCodes { get; set; }
        public BoxConfiguration Box { get; set; }
    }
}
