﻿namespace LendFoundry.Loans.Tape.Configuration
{
    public class TransactionInfo
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
