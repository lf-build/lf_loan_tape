﻿namespace LendFoundry.Loans.Tape.Configuration
{
    public class InvestorServiceConfiguration
    {
        public Investor[] Investors { get; set; }
    }
}
