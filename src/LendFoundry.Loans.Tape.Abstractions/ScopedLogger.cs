﻿using System;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Loans.Tape
{
    public class ScopedLogger : ILogger
    {
        public ScopedLogger(ILogger logger, string scope)
        {
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            if (scope == null)
                throw new ArgumentNullException(nameof(scope));

            if (string.IsNullOrWhiteSpace(scope))
                throw new ArgumentException("Argument cannot be empty", nameof(scope));

            Logger = logger;
            Scope = scope;
        }

        public ILogger AddScope(string newScope) => new ScopedLogger(Logger, Scope + "|" + newScope);

        private ILogger Logger { get; }

        private string Scope { get; }

        private string BuildMessage(string message) => $"[{Scope}] {message}";

        public void Debug(string message) => Logger.Debug(BuildMessage(message));
        public void Debug(string message, object properties) => Logger.Debug(BuildMessage(message), properties);
        public void Error(string message) => Logger.Error(BuildMessage(message));
        public void Error(string message, Exception exception) => Logger.Error(BuildMessage(message), exception);
        public void Error(string message, object properties) => Logger.Error(BuildMessage(message), properties);
        public void Error(string message, Exception exception, object properties) => Logger.Error(BuildMessage(message), exception, properties);
        public void Info(string message) => Logger.Info(BuildMessage(message));
        public void Info(string message, object properties) => Logger.Info(BuildMessage(message), properties);
        public void Warn(string message) => Logger.Warn(BuildMessage(message));
        public void Warn(string message, object properties) => Logger.Warn(BuildMessage(message), properties);
    }
}
