﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace LendFoundry.Loans.Tape.Persistence
{
    internal class CounterRepository : MongoRepository<ICounter, Counter>
    {
        public CounterRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "counters")
        {
        }

        public int Increment(string name)
        {
            var options = new FindOneAndUpdateOptions<ICounter> { IsUpsert = true, ReturnDocument = ReturnDocument.After };
            var key = new BsonElement("$set", new BsonDocument { { "Name", name }, { "TenantId", TenantService.Current.Id } });
            var countIncrement = new BsonElement("$inc", new BsonDocument { { "Count", 1 } });
            var update = new BsonDocument(new List<BsonElement> { key, countIncrement });
            var filterByKey = Builders<ICounter>.Filter.And(
                Builders<ICounter>.Filter.Eq(c => c.Name, name),
                Builders<ICounter>.Filter.Eq(c => c.TenantId, TenantService.Current.Id)
            );
            var counter = Collection.FindOneAndUpdate(filterByKey, update, options);
            return counter.Count;
        }
    }

    internal interface ICounter : IAggregate
    {
        string Name { get; set; }
        int Count { get; set; }
    }

    internal class Counter : Aggregate, ICounter
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
