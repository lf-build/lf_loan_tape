﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Loans.Tape.Daily.Transactions;
using LendFoundry.Loans.Tape.Persistence;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace LendFoundry.Loans.Tape.Daily.Persistence
{
    public class TransactionTapeRepository : MongoRepository<ITransactionEntry, TransactionEntry>, ITransactionTapeRepository
    {
        private const string CollectionName = "daily-transaction-tape";

        public TransactionTapeRepository(ITenantService tenantService, IMongoConfiguration configuration, ILogger logger)
            : base(tenantService, configuration, CollectionName)
        {
            Logger = logger.Scope(this);
            CounterRepository = new CounterRepository(tenantService, configuration);
        }

        static TransactionTapeRepository()
        {
            BsonClassMap.RegisterClassMap<TransactionEntry>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.FeedDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(p => p.FundedDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(p => p.OriginationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(p => p.TransactionDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TransactionEntry);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        private ILogger Logger { get; }
        private CounterRepository CounterRepository { get; }

        public void AddOrUpdate(ITransactionEntry entry)
        {
            if (entry == null)
                throw new ArgumentNullException(nameof(entry));

            if (string.IsNullOrWhiteSpace(entry.TransactionId))
                throw new ArgumentNullException(nameof(entry.TransactionId));

            Expression<Func<ITransactionEntry, bool>> query = l =>
                l.TenantId == TenantService.Current.Id &&
                l.TransactionId == entry.TransactionId;

            var existingEntry = Query.Where(query).Select(v => new ExistingEntry(v.Id, v.TransactionNumber)).FirstOrDefault();
            var exists = existingEntry != null;

            entry.TenantId = TenantService.Current.Id;
            entry.Id = existingEntry?.Id ?? null;
            entry.TransactionNumber = existingEntry?.TransactionNumber ?? CounterRepository.Increment(CollectionName);

            if (exists)
                Collection.ReplaceOne(query, entry);
            else
                Collection.InsertOne(entry);

            Logger.ForTransactionTape(entry).Info("Transaction " + (exists ? "updated" : "inserted"),
                new { entry.LoanReferenceNumber, TransactionId = entry.TransactionId });
        }

        IEnumerable<ITransactionEntry> ITransactionTapeRepository.Query(DateTimeOffset endDate, string[] ignoredTransactionCodes)
        {
            var builder = new FilterDefinitionBuilder<ITransactionEntry>();
            var filter = builder.And(
                builder.Eq(e => e.TenantId, TenantService.Current.Id),
                builder.Lte(e => e.FeedDate, endDate),
                builder.Nin(e => e.TransactionCode, ignoredTransactionCodes)
            );
            return Collection.Find(filter).ToList();
        }

        IEnumerable<ITransactionEntry> ITransactionTapeRepository.Query(DateTimeOffset startDate, DateTimeOffset endDate, string[] ignoredTransactionCodes)
        {
            var builder = new FilterDefinitionBuilder<ITransactionEntry>();
            var filter = builder.And(
                builder.Eq(e => e.TenantId, TenantService.Current.Id),
                builder.Gte(e => e.FeedDate, startDate),
                builder.Lte(e => e.FeedDate, endDate),
                builder.Nin(e => e.TransactionCode, ignoredTransactionCodes)
            );
            return Collection.Find(filter).ToList();
        }

        private class ExistingEntry
        {
            public ExistingEntry(string id, int transactionNumber)
            {
                Id = id;
                TransactionNumber = transactionNumber;
            }

            public string Id { get; set; }
            public int TransactionNumber { get; set; }
        }
    }
}
