﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Loans.Tape.Daily.InvestorFeed;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Framework.DependencyInjection;
using System;

namespace LendFoundry.Loans.Tape.Daily.Persistence
{
    public class InvestorFeedRepositoryFactory : IInvestorFeedRepositoryFactory
    {
        public InvestorFeedRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IInvestorFeedRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            var logger = Provider.GetService<ILoggerFactory>().CreateLogger();
            return new InvestorFeedRepository(tenantService, mongoConfiguration, logger);
        }
    }
}