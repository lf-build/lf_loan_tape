﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Loans.Tape.Daily.InvestorFeed;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Tape.Daily.Persistence
{
    public class InvestorFeedRepository : MongoRepository<IInvestorFeedEntry, InvestorFeedEntry>, IInvestorFeedRepository
    {
        public InvestorFeedRepository(ITenantService tenantService, IMongoConfiguration configuration, ILogger logger)
            : base(tenantService, configuration, "daily-investor-feed")
        {
            Logger = logger.Scope(this);
        }

        static InvestorFeedRepository()
        {
            BsonClassMap.RegisterClassMap<InvestorFeedEntry>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.FeedDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(p => p.OriginationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(p => p.TransactionDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(p => p.DueDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));

                var type = typeof(InvestorFeedEntry);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        private ILogger Logger { get; }

        public void AddOrUpdate(IInvestorFeedEntry entry)
        {
            if (entry == null)
                throw new ArgumentNullException(nameof(entry));

            if (string.IsNullOrWhiteSpace(entry.TransactionId))
                throw new ArgumentNullException(nameof(entry.TransactionId));

            Expression<Func<IInvestorFeedEntry, bool>> tapeAlreadyCreated = l =>
                l.TenantId == TenantService.Current.Id &&
                l.TransactionId == entry.TransactionId;

            var existingId = Query.Where(tapeAlreadyCreated).Select(v => v.Id).FirstOrDefault();

            entry.TenantId = TenantService.Current.Id;
            entry.Id = existingId ?? ObjectId.GenerateNewId().ToString();

            var exists = !string.IsNullOrWhiteSpace(existingId);

            if (exists)
                Collection.ReplaceOne(tapeAlreadyCreated, entry);
            else
                Collection.InsertOne(entry);

            Logger.ForInvestorFeed(entry).Info("Transaction " + (exists ? "updated" : "inserted"),
                new { entry.LoanReferenceNumber, TransactionId = entry.TransactionId });
        }

        IEnumerable<IInvestorFeedEntry> IInvestorFeedRepository.Query(DateTimeOffset endDate, string[] acceptedInvestors, string[] ignoredTransactionCodes)
        {
            var builder = new FilterDefinitionBuilder<IInvestorFeedEntry>();
            var filter = builder.And(
                builder.Eq(e => e.TenantId, TenantService.Current.Id),
                builder.Lte(e => e.FeedDate, endDate),
                builder.In(e => e.Investor, acceptedInvestors),
                builder.Nin(e => e.TransactionCode, ignoredTransactionCodes)
            );
            return Collection.Find(filter).ToList();
        }

        IEnumerable<IInvestorFeedEntry> IInvestorFeedRepository.Query(DateTimeOffset startDate, DateTimeOffset endDate, string[] acceptedInvestors, string[] ignoredTransactionCodes)
        {
            var builder = new FilterDefinitionBuilder<IInvestorFeedEntry>();
            var filter = builder.And(
                builder.Eq(e => e.TenantId, TenantService.Current.Id),
                builder.Gte(e => e.FeedDate, startDate),
                builder.Lte(e => e.FeedDate, endDate),
                builder.In(e => e.Investor, acceptedInvestors),
                builder.Nin(e => e.TransactionCode, ignoredTransactionCodes)
            );
            return Collection.Find(filter).ToList();
        }
    }
}
