﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Loans.Tape.Daily.Transactions;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Framework.DependencyInjection;
using System;

namespace LendFoundry.Loans.Tape.Daily.Persistence
{
    public class TransactionTapeRepositoryFactory : ITransactionTapeRepositoryFactory
    {
        public TransactionTapeRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public ITransactionTapeRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            var logger = Provider.GetService<ILoggerFactory>().CreateLogger();
            return new TransactionTapeRepository(tenantService, mongoConfiguration, logger);
        }
    }
}