﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Framework.DependencyInjection;
using System;

namespace LendFoundry.Loans.Tape.Monthly.Persistence
{
    public class LoanTapeRepositoryFactory : ILoanTapeRepositoryFactory
    {
        public LoanTapeRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public ILoanTapeRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            var logger = Provider.GetService<ILoggerFactory>().CreateLogger();
            return new LoanTapeRepository(tenantService, mongoConfiguration, logger);
        }
    }
}