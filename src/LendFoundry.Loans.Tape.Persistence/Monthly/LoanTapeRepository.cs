﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace LendFoundry.Loans.Tape.Monthly.Persistence
{
    public class LoanTapeRepository : MongoRepository<ILoanTape, LoanTape>, ILoanTapeRepository
    {
        static LoanTapeRepository()
        {
            BsonClassMap.RegisterClassMap<LoanTape>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.OriginationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(p => p.FundingDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(p => p.PurchaseDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(p => p.MaturityDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(p => p.SchedulePaymentDue).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(p => p.LastPaymentReceivedDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(p => p.LastPaymentDueDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(p => p.NextPaymentDueDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(p => p.ChargeOffDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));

                var type = typeof(LoanTape);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public LoanTapeRepository(ITenantService tenantService, IMongoConfiguration configuration, ILogger logger)
            : base(tenantService, configuration, "monthly-loan-tape")
        {
            CreateIndexIfNotExists
            (
               indexName: "loan-reference-number",
               index: Builders<ILoanTape>.IndexKeys.Ascending(i => i.LoanReferenceNumber)
            );

            Logger = logger.Scope(this);
        }

        private ILogger Logger { get; }

        public void AddOrUpdate(ILoanTape tape)
        {
            if (tape == null)
                throw new ArgumentNullException(nameof(tape));

            if (string.IsNullOrWhiteSpace(tape.LoanReferenceNumber))
                throw new ArgumentException("Loan Reference Number must to be informed.");

            var currentMonth = tape.CreationDate.Month;

            Expression<Func<ILoanTape, bool>> tapeAlreadyCreated = l =>
                l.TenantId == TenantService.Current.Id &&
                l.LoanReferenceNumber == tape.LoanReferenceNumber
                && l.CreationDate.Month == currentMonth;

            var existingId = Query
                .Where(tapeAlreadyCreated)
                .Select(v => v.Id)
                .FirstOrDefault();

            tape.TenantId = TenantService.Current.Id;
            tape.Id = existingId ?? ObjectId.GenerateNewId().ToString();

            var exists = !string.IsNullOrWhiteSpace(existingId);

            if (exists)
                Collection.ReplaceOne(tapeAlreadyCreated, tape);
            else
                Collection.InsertOne(tape);

            Logger.ForLoanTape(tape).Info("Tape " + (exists ? "updated" : "inserted"),
                new { tape.LoanReferenceNumber, ReferenceDate = tape.CreationDate.Time.ToString("yyyy-MM-dd") });
        }

        public ILoanTape[] GetByLoan(string referenceNumber)
        {
            if (string.IsNullOrWhiteSpace(referenceNumber))
                throw new ArgumentNullException(nameof(referenceNumber));

            return Query.Where(q => q.LoanReferenceNumber == referenceNumber).ToArray();
        }

        public ILoanTape GetByLoan(string referenceNumber, string yearAndMonth)
        {
            if (string.IsNullOrWhiteSpace(referenceNumber))
                throw new ArgumentNullException(nameof(referenceNumber));

            if (string.IsNullOrWhiteSpace(yearAndMonth))
                throw new ArgumentNullException(nameof(yearAndMonth));

            var searchTerm = $"{yearAndMonth}-m";

            return Query.Where(
                q =>
                q.LoanReferenceNumber == referenceNumber &&
                q.CreationDate.Month == searchTerm
            ).SingleOrDefault();
        }

        public ILoanTape[] GetByYearMonth(string yearAndMonth)
        {
            if (string.IsNullOrWhiteSpace(yearAndMonth))
                throw new ArgumentNullException(nameof(yearAndMonth));

            var searchTerm = $"{yearAndMonth}-m";

            return Query.Where(
                q => q.CreationDate.Month == searchTerm
            ).ToArray();
        }
    }
}