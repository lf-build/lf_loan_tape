﻿using System;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Tape.Client
{
    public class TapeServiceFactory : ITapeServiceFactory
    {
        public TapeServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public ITapeService Create(ITokenReader reader)
        {
            return new TapeService(Provider.GetServiceClient(reader, Endpoint, Port));
        }
    }
}