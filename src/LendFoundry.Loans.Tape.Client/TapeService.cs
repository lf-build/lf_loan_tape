﻿using System;
using LendFoundry.Foundation.Services;
using RestSharp;

namespace LendFoundry.Loans.Tape.Client
{
    public class TapeService : ITapeService
    {
        public TapeService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public void UploadDailyTransactionTape()
        {
            HandleResponse(() => Client.Execute(new RestRequest("/transactions/upload", Method.POST)));
        }

        public void UploadDailyInvestorTape()
        {
            HandleResponse(() => Client.Execute(new RestRequest("/investor/upload", Method.POST)));
        }

        private static void HandleResponse(Action expression)
        {
            try
            {
                expression();
            }
            catch (ClientException exception)
            {
                throw HandleError(exception);
            }
        }

        private static Exception HandleError(ClientException exception)
        {
            var error = exception.Error;

            if (error == null)
                return exception;

            switch (error.Code)
            {
                case 404: return new NotFoundException(error.Message);
                case 400: return new ArgumentException(error.Message);
                default: return exception;
            }
        }
    }
}
