﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.Tape.Client
{
    public static class TapeServiceExtensions
    {
        public static IServiceCollection AddTapeService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ITapeServiceFactory>(p=> new TapeServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ITapeServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}