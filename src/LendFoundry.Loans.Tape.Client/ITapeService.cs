﻿namespace LendFoundry.Loans.Tape.Client
{
    public interface ITapeService
    {
        void UploadDailyTransactionTape();
        void UploadDailyInvestorTape();
    }
}
