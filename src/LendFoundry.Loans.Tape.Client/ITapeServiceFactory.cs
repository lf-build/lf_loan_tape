using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Tape.Client
{
    public interface ITapeServiceFactory
    {
        ITapeService Create(ITokenReader reader);
    }
}