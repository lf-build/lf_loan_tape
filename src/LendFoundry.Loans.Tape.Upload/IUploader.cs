﻿using System.IO;

namespace LendFoundry.Loans.Tape.Upload
{
    public interface IUploader
    {
        void Upload(Stream content, UploadLocation location);
    }
}
