﻿using Box.V2;
using Box.V2.Auth;
using Box.V2.Config;
using Box.V2.JWTAuth;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Tape.Configuration;
using System;
using System.IO;
using System.Text;

namespace LendFoundry.Loans.Tape.Upload
{
    public class BoxUploader : IUploader
    {
        public BoxUploader(TapeServiceConfiguration tapeServiceConfiguration, Cache cache, ILogger logger)
        {
            if (tapeServiceConfiguration == null) throw new ArgumentNullException(nameof(tapeServiceConfiguration));
            if (tapeServiceConfiguration.Box == null) throw new ArgumentNullException(nameof(tapeServiceConfiguration.Box));

            var configuration = tapeServiceConfiguration.Box;

            if (string.IsNullOrWhiteSpace(configuration.ClientId)) throw new ArgumentException("Argument is null or empty", nameof(tapeServiceConfiguration.Box.ClientId));
            if (string.IsNullOrWhiteSpace(configuration.ClientSecret)) throw new ArgumentException("Argument is null or empty", nameof(tapeServiceConfiguration.Box.ClientSecret));
            if (string.IsNullOrWhiteSpace(configuration.EnterpriseId)) throw new ArgumentException("Argument is null or empty", nameof(tapeServiceConfiguration.Box.EnterpriseId));
            if (string.IsNullOrWhiteSpace(configuration.PrivateKey)) throw new ArgumentException("Argument is null or empty", nameof(tapeServiceConfiguration.Box.PrivateKey));
            if (string.IsNullOrWhiteSpace(configuration.PublicKeyId)) throw new ArgumentException("Argument is null or empty", nameof(tapeServiceConfiguration.Box.PublicKeyId));
            if (string.IsNullOrWhiteSpace(configuration.UserId)) throw new ArgumentException("Argument is null or empty", nameof(tapeServiceConfiguration.Box.UserId));

            Configuration = configuration;
            Cache = cache;
            Logger = logger.Scope(this);
        }

        private BoxConfiguration Configuration { get; }
        private Cache Cache { get; }
        private ILogger Logger { get; }

        public void Upload(Stream content, UploadLocation location)
        {
            if (content == null) throw new ArgumentNullException(nameof(content));
            if (location == null) throw new ArgumentNullException(nameof(location));
            if (string.IsNullOrWhiteSpace(location.FileId)) throw new ArgumentException("Argument is null or empty", nameof(location.FileId));
            if (string.IsNullOrWhiteSpace(location.FileName)) throw new ArgumentException("Argument is null or empty", nameof(location.FileName));

            Logger.Info("Uploading content to Box file {FileId} as '{FileName}'", location);

            var config = new BoxConfig(
                Configuration.ClientId,
                Configuration.ClientSecret,
                Configuration.EnterpriseId,
                FromBase64(Configuration.PrivateKey),
                Configuration.PrivateKeyPassword,
                Configuration.PublicKeyId
            );

            var userId = Configuration.UserId;
            var boxJwt = new BoxJWTAuth(config);
            var userToken = Cache.Get($"box_user_token_{userId}", Millis.Minutes(50), () => boxJwt.UserToken(userId));
            var session = new OAuthSession(userToken, "NOT_NEEDED", 3600, "bearer");
            var client = new BoxClient(config, session);

            var task = client.FilesManager.UploadNewVersionAsync(location.FileName, location.FileId, content);

            task.Wait();

            if (task.IsFaulted)
            {
                throw new Exception("Failed to upload file to Box", task.Exception);
            }
            
            Logger.Info("Content successfully uploaded to Box file {FileId} as {FileName}", location);
        }

        private static string FromBase64(string str)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(str));
        }
    }
}
