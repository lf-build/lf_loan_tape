﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Tape.Api.Helper;
using LendFoundry.Loans.Tape.Configuration;
using LendFoundry.Loans.Tape.Daily.InvestorFeed;
using LendFoundry.Loans.Tape.Daily.Transactions;
using LendFoundry.Loans.Tape.Monthly;
using Microsoft.AspNet.Mvc;
using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using LendFoundry.Loans.Tape.Upload;
using System.IO;
using System.Text;
using LendFoundry.EventHub.Client;
using LendFoundry.Loans.Tape.Events;

namespace LendFoundry.Loans.Tape.Api.Controllers
{
    public class MainController : Controller
    {
        public MainController(
            ILoanTapeService loanTapeService,
            ITransactionTapeService transactionTapeService,
            IInvestorFeedService investorFeedService,
            IUploader uploader,
            TapeServiceConfiguration configuration,
            ITenantTime tenantTime,
            IEventHubClient eventHub,
            ILogger logger)
        {
            LoanTapeService = loanTapeService;
            TransactionTapeService = transactionTapeService;
            InvestorFeedService = investorFeedService;
            Uploader = uploader;
            Configuration = configuration;
            TenantTime = tenantTime;
            EventHub = eventHub;
            Logger = logger.Scope(this);
        }

        private ILoanTapeService LoanTapeService { get; }
        private ITransactionTapeService TransactionTapeService { get; }
        private IInvestorFeedService InvestorFeedService { get; }
        private TapeServiceConfiguration Configuration { get; }
        private ITenantTime TenantTime { get; }
        private ILogger Logger { get; }
        private IEventHubClient EventHub { get; }
        private IUploader Uploader { get; }



        [HttpGet("/loan/{loanReferenceNumber}/{yearAndMonth}")]
        public IActionResult GetMonthlyLoanTape(string loanReferenceNumber, string yearAndMonth)
        {
            return Execute(() =>
            {
                if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                    throw new ArgumentException($"{nameof(loanReferenceNumber)} is null or empty");

                ValidateYearMonth(yearAndMonth);

                var tape = LoanTapeService.GetByLoan(loanReferenceNumber, yearAndMonth);

                if (tape == null)
                    throw new NotFoundException($"Loan tape not found for loan {loanReferenceNumber} on {yearAndMonth}");

                return new HttpOkObjectResult(tape);
            });
        }

        [HttpPut("/loans/{referenceDate}")]
        public void UpdateMonthlyLoanTape(DateTime referenceDate, [FromBody] string[] loanReferenceNumbers)
        {
            Execute(() =>
            {
                if (referenceDate == default(DateTime))
                    throw new ArgumentException($"Invalid or missing argument for parameter {nameof(referenceDate)}");

                if (loanReferenceNumbers == null || loanReferenceNumbers.Length == 0)
                    throw new ArgumentException($"An array of loan reference numbers must be specified in the request body");

                foreach (var loanReferenceNumber in loanReferenceNumbers)
                {
                    LoanTapeService.GenerateTape(loanReferenceNumber, TenantTime.FromDate(referenceDate));
                }
            });
        }

        [HttpGet("/loans/{yearAndMonth}")]
        public IActionResult GetMonthlyLoanTape(string yearAndMonth)
        {
            return Execute(() =>
            {
                ValidateYearMonth(yearAndMonth);

                var tapes = LoanTapeService.GetByYearMonth(yearAndMonth);

                if (tapes == null || tapes.Length == 0)
                    throw new NotFoundException($"No entries found on #{yearAndMonth}");

                return new HttpOkObjectResult(tapes);
            });
        }

        [HttpGet("/loans/{yearAndMonth}/export/csv")]
        public IActionResult GetMonthlyLoanTapeCSV(string yearAndMonth)
        {
            return Execute(() =>
            {
                ValidateYearMonth(yearAndMonth);

                var tapes = LoanTapeService.GetByYearMonth(yearAndMonth);

                if (tapes == null || tapes.Length == 0)
                    throw new NotFoundException($"No entries found on #{yearAndMonth}");

                return new CsvFileResult<ILoanTape>(tapes, 
                    LoanTapeService.GenerateFileName(yearAndMonth, "loan-tape"),
                    Configuration.MonthlyLoanTape.Projection);
            });
        }



        [HttpPut("/transactions/{loanReferenceNumber}")]
        public void UpdateDailyTransactionTape(string loanReferenceNumber)
        {
            Execute(() => TransactionTapeService.Update(loanReferenceNumber));
        }

        [HttpPut("/transactions/{loanReferenceNumber}/{transactionId}")]
        public void UpdateDailyTransactionTape(string loanReferenceNumber, string transactionId)
        {
            Execute(() => TransactionTapeService.Update(loanReferenceNumber, transactionId));
        }

        [HttpGet("/transactions")]
        public IActionResult GetDailyTransactionTape()
        {
            return Execute(() => GetDailyTransactionTapeUpTo(EndOfDay(TenantTime.Today)));
        }
        
        [HttpGet("/transactions/export/csv")]
        public IActionResult GetDailyTransactionTapeInCsvFormat()
        {
            return Execute(() => ToCsvResult(TransactionTapeService.BuildTape(EndOfDay(TenantTime.Today)), EndOfDay(TenantTime.Today)));
        }
                                
        [HttpPost("/transactions/upload")]
        public IActionResult UploadDailyTransactionTapeInCsvFormat()
        {
            return Execute(() =>
            {
                Logger.Info("Uploading daily transaction tape where feed date <= {endDate}", new { endDate = TenantTime.Today.ToString("yyyy-MM-dd") });
                return UploadDailyTransactionTapeInCsvFormat(TransactionTapeService.BuildTape(EndOfDay(TenantTime.Today)));
            });
        }

        [HttpGet("/transactions/from/beginning/to/{endDate}")]
        public IActionResult GetDailyTransactionTapeUpTo(string endDate)
        {
            return Execute(() => GetDailyTransactionTapeUpTo(EndOfDay(ParseDate(endDate))));
        }

        [HttpGet("/transactions/from/beginning/to/{endDate}/export/csv")]
        public IActionResult GetDailyTransactionTapeInCsvFormatUpTo(string endDate)
        {
            return Execute(() => GetDailyTransactionTapeInCsvFormatUpTo(EndOfDay(ParseDate(endDate))));
        }
                
        [HttpPost("/transactions/from/beginning/to/{endDate}/upload")]
        public IActionResult UploadDailyTransactionTapeInCsvFormat(string endDate)
        {
            return Execute(() =>
            {
                Logger.Info("Uploading daily transaction tape where feed date <= {endDate}", new { endDate });
                return UploadDailyTransactionTapeInCsvFormat(TransactionTapeService.BuildTape(EndOfDay(ParseDate(endDate))));
            });
        }

        private IActionResult GetDailyTransactionTapeInCsvFormatUpTo(DateTimeOffset endDate)
        {
            return Execute(() => ToCsvResult(TransactionTapeService.BuildTape(endDate), endDate));
        }

        private IActionResult GetDailyTransactionTapeUpTo(DateTimeOffset endDate)
        {
            return Execute(() =>
            {
                var tapes = TransactionTapeService.BuildTape(endDate);

                if (tapes == null || !tapes.Any())
                    throw new NotFoundException($"No entries found up to #{endDate}");

                return new HttpOkObjectResult(tapes);
            });
        }
        
        [HttpGet("/transactions/from/{startDate}/to/{endDate}")]
        public IActionResult GetDailyTransactionTape(string startDate, string endDate)
        {
            return Execute(() => GetDailyTransactionTape(ParseDate(startDate), EndOfDay(ParseDate(endDate))));
        }
                
        [HttpGet("/transactions/from/{startDate}/to/{endDate}/export/csv")]
        public IActionResult GetDailyTransactionTapeInCsvFormat(string startDate, string endDate)
        {
            return Execute(() => GetDailyTransactionTapeInCsvFormat(ParseDate(startDate), EndOfDay(ParseDate(endDate))));
        }
        
        [HttpPost("/transactions/from/{startDate}/to/{endDate}/upload")]
        public IActionResult UploadDailyTransactionTapeInCsvFormat(string startDate, string endDate)
        {
            return Execute(() =>
            {
                Logger.Info("Uploading daily transaction tape where {startDate} >= feed date <= {endDate}", new { startDate, endDate });
                return UploadDailyTransactionTapeInCsvFormat(TransactionTapeService.BuildTape(ParseDate(startDate), EndOfDay(ParseDate(endDate))));
            });
        }

        public IActionResult UploadDailyTransactionTapeInCsvFormat(IEnumerable<ITransactionEntry> tape)
        {
            return Execute(() =>
            {
                var configuration = Configuration.DailyTransactionTape;
                return UploadFileInCsvFormat("daily transaction tape", tape, configuration.Projection, configuration.UploadLocation);
            });
        }

        private IActionResult GetDailyTransactionTapeInCsvFormat(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            return Execute(() => ToCsvResult(TransactionTapeService.BuildTape(startDate, endDate), endDate));
        }

        private IActionResult GetDailyTransactionTape(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            return Execute(() =>
            {
                var tapes = TransactionTapeService.BuildTape(startDate, endDate);

                if (tapes == null || !tapes.Any())
                    throw new NotFoundException($"No entries found between #{startDate} #{endDate}");

                return new HttpOkObjectResult(tapes);
            });
        }
        
        private IActionResult ToCsvResult(IEnumerable<ITransactionEntry> enumerable, DateTimeOffset date)
        {
            return ToCsvResult(enumerable, $"daily-transaction-tape-{date.ToString("yyyy-MM-dd")}", Configuration.DailyTransactionTape.Projection);
        }



        [HttpPut("/investor/{loanReferenceNumber}")]
        public void UpdateDailyInvestorFeed(string loanReferenceNumber)
        {
            Execute(() => InvestorFeedService.Update(loanReferenceNumber));
        }

        [HttpPut("/investor/{loanReferenceNumber}/{transactionId}")]
        public void UpdateDailyInvestorFeed(string loanReferenceNumber, string transactionId)
        {
            Execute(() => InvestorFeedService.Update(loanReferenceNumber, transactionId));
        }

        [HttpGet("/investor")]
        public IActionResult GetDailyInvestorFeed()
        {
            return Execute(() => GetDailyInvestorFeedUpTo(EndOfDay(TenantTime.Today)));
        }
        
        [HttpGet("/investor/export/csv")]
        public IActionResult GetDailyInvestorFeedInCsvFormat()
        {
            return Execute(() => ToCsv(InvestorFeedService.BuildTape(EndOfDay(TenantTime.Today)), EndOfDay(TenantTime.Today)));
        }
        
        [HttpPost("/investor/upload")]
        public IActionResult UploadDailyInvestorFeedInCsvFormat()
        {
            Logger.Info("Uploading daily investor feed where feed date <= {endDate}", new { endDate = TenantTime.Today.ToString("yyyy-MM-dd") });
            return Execute(() => UploadDailyInvestorFeedInCsvFormat(InvestorFeedService.BuildTape(EndOfDay(TenantTime.Today))));
        }

        [HttpGet("/investor/from/beginning/to/{endDate}")]
        public IActionResult GetDailyInvestorFeedUpTo(string endDate)
        {
            return Execute(() => GetDailyInvestorFeedUpTo(EndOfDay(ParseDate(endDate))));
        }

        [HttpGet("/investor/from/beginning/to/{endDate}/export/csv")]
        public IActionResult GetDailyInvestorFeedInCsvFormatUpTo(string endDate)
        {
            return Execute(() => GetDailyInvestorFeedInCsvFormatUpTo(EndOfDay(ParseDate(endDate))));
        }
        
        [HttpPost("/investor/from/beginning/to/{endDate}/upload")]
        public IActionResult UploadDailyInvestorFeedInCsvFormat(string endDate)
        {
            Logger.Info("Uploading daily investor feed where feed date <= {endDate}", new { endDate });
            return Execute(() => UploadDailyInvestorFeedInCsvFormat(InvestorFeedService.BuildTape(EndOfDay(ParseDate(endDate)))));
        }
        
        private IActionResult GetDailyInvestorFeedInCsvFormatUpTo(DateTimeOffset endDate)
        {
            return Execute(() => ToCsv(InvestorFeedService.BuildTape(endDate), endDate));
        }

        private IActionResult GetDailyInvestorFeedUpTo(DateTimeOffset endDate)
        {
            return Execute(() =>
            {
                var tapes = InvestorFeedService.BuildTape(endDate);

                if (tapes == null || !tapes.Any())
                    throw new NotFoundException($"No entries found up to #{endDate}");

                return new HttpOkObjectResult(tapes);
            });
        }
                
        [HttpGet("/investor/from/{startDate}/to/{endDate}")]
        public IActionResult GetDailyInvestorFeed(string startDate, string endDate)
        {
            return Execute(() => GetDailyInvestorFeed(ParseDate(startDate), EndOfDay(ParseDate(endDate))));
        }
        
        [HttpGet("/investor/from/{startDate}/to/{endDate}/export/csv")]
        public IActionResult GetDailyInvestorFeedInCsvFormat(string startDate, string endDate)
        {
            return Execute(() => GetDailyInvestorFeedInCsvFormat(ParseDate(startDate), EndOfDay(ParseDate(endDate))));
        }
        
        [HttpPost("/investor/from/{startDate}/to/{endDate}/upload")]
        public IActionResult UploadDailyInvestorFeedInCsvFormat(string startDate, string endDate)
        {
            Logger.Info("Uploading daily investor feed where {startDate} >= feed date <= {endDate}", new { startDate, endDate });
            return Execute(() => UploadDailyInvestorFeedInCsvFormat(InvestorFeedService.BuildTape(ParseDate(startDate), EndOfDay(ParseDate(endDate)))));
        }

        private IActionResult GetDailyInvestorFeedInCsvFormat(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            return Execute(() => ToCsv(InvestorFeedService.BuildTape(startDate, endDate), endDate));
        }

        private IActionResult GetDailyInvestorFeed(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            return Execute(() =>
            {
                var tapes = InvestorFeedService.BuildTape(startDate, endDate);

                if (tapes == null || !tapes.Any())
                    throw new NotFoundException($"No entries found between #{startDate} #{endDate}");

                return new HttpOkObjectResult(tapes);
            });
        }

        private IActionResult ToCsv(IEnumerable<IInvestorFeedEntry> enumerable, DateTimeOffset date)
        {
            return ToCsvResult(enumerable, $"daily-investor-feed-{date.ToString("yyyy-MM-dd")}", Configuration.DailyInvestorFeed.Projection);
        }

        public IActionResult UploadDailyInvestorFeedInCsvFormat(IEnumerable<IInvestorFeedEntry> tape)
        {
            return Execute(() =>
            {
                var configuration = Configuration.DailyInvestorFeed;
                return UploadFileInCsvFormat("daily investor feed", tape, configuration.Projection, configuration.UploadLocation);
            });
        }



        private DateTimeOffset ParseDate(string str)
        {
            try
            {
                var date = DateTime.ParseExact(str, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                return TenantTime.FromDate(date);
            }
            catch(FormatException e)
            {
                throw new ArgumentException($"Invalid date: {str}", e);
            }
        }

        private DateTimeOffset EndOfDay(DateTimeOffset d)
        {
            return new DateTimeOffset(d.Year, d.Month, d.Day, 23, 59, 59, 999, d.Offset);
        }

        private void ValidateYearMonth(string yearAndMonth)
        {
            if (string.IsNullOrWhiteSpace(yearAndMonth))
                throw new ArgumentNullException($"Invalid {nameof(yearAndMonth)}");

            var regex = Regex.Match(yearAndMonth, @"^[0-9]{4}-(0[1-9]|[1-2][0-9]|3[0-1])$");

            if (!regex.Success)
                throw new ArgumentException($"Nota a valid period: #{nameof(yearAndMonth)}. Expected format: yyyy-MM");
        }

        private IActionResult ToCsvResult<T>(IEnumerable<T> entries, string filename, string projection)
        {
            return Execute(() =>
            {
                if (entries == null || !entries.Any())
                    throw new NotFoundException($"No entries found");

                return new CsvFileResult<T>(entries, filename, projection);
            });
        }
        
        public IActionResult UploadFileInCsvFormat<T>(string tapeName, IEnumerable<T> tape, string projection, UploadLocation location)
        {
            return Execute(() =>
            {
                if (tape.Any())
                {
                    var csv = Csv.ToCsv(tape, projection);
                    var content = new MemoryStream(Encoding.UTF8.GetBytes(csv));

                    try
                    {
                        Uploader.Upload(content, location);
                        EventHub.Publish(nameof(TapeUploaded), new TapeUploaded(tapeName));
                    }
                    catch(Exception cause)
                    {
                        EventHub.Publish(nameof(TapeUploadFailed), new TapeUploadFailed(tapeName));
                        throw new Exception($"Tape upload failed: {tapeName}", cause);
                    }

                    return new HttpOkObjectResult(new { Count = tape.Count() });
                }
                else
                {
                    return new HttpNotFoundResult();
                }
            });
        }

        private void Execute(Action expression)
        {
            Execute(() =>
            {
                expression();
                return null;
            });
        }

        private IActionResult Execute(Func<IActionResult> expression)
        {
            try
            {
                return expression();
            }
            catch (ArgumentNullException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return ErrorResult.NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                Logger.Error("Error when handling request in LoanTapeController", ex);
                return ErrorResult.InternalServerError("We couldn't process your request");
            }
        }
    }
}