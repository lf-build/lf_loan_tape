﻿using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LendFoundry.Loans.Tape.Api.Helper
{
    public class CsvFileResult<T> : FileResult
    {
        private char ValueSeparator = Settings.CsvSeparator;
        private const string DefaultDateFormat = "dd-MMM-yy";

        public CsvFileResult(IEnumerable<T> items, string fileName, string projection = null) : base("text/csv")
        {
            Items = items;
            FileDownloadName = $"{fileName}.csv";
            Projection = projection;
        }

        private IEnumerable<T> Items { get; }
        private string Projection { get; }

        protected override Task WriteFileAsync(HttpResponse response, CancellationToken cancellation)
        {
            return Task.Run(() => Csv.Write(Items, Projection, response.Body));
        }
    }
}
