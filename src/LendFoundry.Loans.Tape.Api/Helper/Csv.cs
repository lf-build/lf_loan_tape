﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace LendFoundry.Loans.Tape.Api.Helper
{
    public class Csv
    {
        private static readonly char ValueSeparator = Settings.CsvSeparator;
        private const string DefaultDateFormat = "dd-MMM-yy";

        public static string ToCsv<T>(IEnumerable<T> items, string columns)
        {
            var output = new MemoryStream();
            Write(items, columns, output);
            return Encoding.UTF8.GetString(output.GetBuffer());
        }

        public static void Write<T>(IEnumerable<T> items, string columns, Stream output)
        {
            var writer = new StreamWriter(output);
            var properties = typeof(T).GetProperties();
            var columnList = ParseColumns(columns);

            Func<T, Column, string> getValue = (item, column) =>
            {
                var property = properties.FirstOrDefault(p => p.Name.Equals(column.PropertyName));
                if (property == null)
                    return null;

                return GetPropertyValue(property, item, column.Format);
            };

            var columnNames = columnList.Select(c => c.Title).ToList();

            WriteLine(columnNames, writer);

            foreach (var item in items)
                WriteItem(item, columnList, writer, getValue);

            writer.Flush();
        }

        private static void WriteItem<I>(I item, List<Column> columns, TextWriter writer, Func<I, Column, string> getValue)
        {
            var values = columns.Select(column => getValue(item, column));
            WriteLine(values.ToList(), writer);
        }

        private static void WriteLine(List<string> values, TextWriter writer)
        {
            var lastIndex = values.Count - 1;

            for (var index = 0; index <= lastIndex; index++)
            {
                var value = values[index];
                
                if (!string.IsNullOrWhiteSpace(value))
                    writer.Write(value);

                if (index < lastIndex)
                    writer.Write(ValueSeparator);
            }

            writer.Write(Environment.NewLine);
        }

        private static List<Column> ParseColumns(string str)
        {
            var regex = new Regex(@"\s*,\s*");
            var fields = regex.Split(str);
            return fields.Select(s => new Column(s)).ToList();
        }

        private static string GetPropertyValue(PropertyInfo property, object item, string format)
        {
            var type = property.PropertyType;

            if ((type == typeof(DateTimeOffset)) || (type == typeof(DateTimeOffset?)))
            {
                var date = property.GetValue(item) as DateTimeOffset?;
                return date?.ToString(format ?? DefaultDateFormat) ?? "";
            }

            if ((type == typeof(DateTime)) || (type == typeof(DateTime?)))
            {
                var date = property.GetValue(item) as DateTime?;
                return date?.ToString(format ?? DefaultDateFormat) ?? "";
            }

            return property.GetValue(item)?.ToString() ?? "";
        }

        private class Column
        {
            public string PropertyName { get; }
            public string Title { get; }
            public string Format { get; }

            public Column(string fieldDefinition)
            {
                if (string.IsNullOrWhiteSpace(fieldDefinition))
                    throw new ArgumentException($"Argument is null or empty", nameof(fieldDefinition));

                var parts = fieldDefinition.Split(':');
                var part1 = parts[0];
                var part2 = parts.Length > 1 ? parts[1] : null;
                var indexOfFormatSeparator = part1.IndexOf('@');

                var propertyName = indexOfFormatSeparator > -1 ? part1.Substring(0, indexOfFormatSeparator) : part1;
                var format = indexOfFormatSeparator > -1 ? part1.Substring(indexOfFormatSeparator + 1) : null;
                var title = string.IsNullOrWhiteSpace(part2) ? propertyName : part2;

                if (string.IsNullOrWhiteSpace(propertyName))
                    throw new ArgumentException("Property name is empty", nameof(fieldDefinition));

                PropertyName = propertyName;
                Title = string.IsNullOrWhiteSpace(title) ? null : title;
                Format = string.IsNullOrWhiteSpace(format) ? null : format;
            }
        }
    }
}
