﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Loans.Tape.Api
{
    public class Settings
    {
        private const string Prefix = "LOAN_TAPE";

        public static string ServiceName { get; } = "loan-tape";

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static ServiceSettings Loan { get; } = new ServiceSettings($"{Prefix}_LOAN", "loans");

        public static ServiceSettings TransactionLog { get; set; } = new ServiceSettings($"{Prefix}_TRANSACTION_LOG", "transactionlog");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "loan-tape");

        //todo: create a new ServiceSettings option for single properties.
        public static char CsvSeparator { get; } = Convert.ToChar(Environment.GetEnvironmentVariable("CSV_SEPARATOR") ?? ",");
    }
}
