﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Tape.Configuration;
using LendFoundry.Loans.Tape.Daily.InvestorFeed;
using LendFoundry.Loans.Tape.Daily.Persistence;
using LendFoundry.Loans.Tape.Daily.Transactions;
using LendFoundry.Loans.Tape.Monthly;
using LendFoundry.Loans.Tape.Monthly.Formulas;
using LendFoundry.Loans.Tape.Monthly.Persistence;
using LendFoundry.Loans.Tape.Upload;
using LendFoundry.Loans.TransactionLog.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.Tape.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();
            services.AddConfigurationService<TapeServiceConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddLoanService(Settings.Loan.Host, Settings.Loan.Port);
            services.AddTransactionLog(Settings.TransactionLog.Host, Settings.TransactionLog.Port);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.ServiceName);            
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);

            services.AddTransient(p => p.GetTapeServiceConfiguration());
            services.AddTransient(p => p.GetMonthlyLoanTapeConfiguration());
            services.AddTransient(p => p.GetDailyTransactionTapeConfiguration());
            services.AddTransient(p => p.GetInvestorConfiguration());
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));

            services.AddTransient<ILoanTapeRepository, LoanTapeRepository>();
            services.AddTransient<ILoanTapeService, LoanTapeService>();
            services.AddTransient<ILoanTapeBuilder, LoanTapeBuilder>();
            services.AddTransient<ILoanTapeBuilderFactory, LoanTapeBuilderFactory>();
            services.AddTransient<ILoanTapeRepositoryFactory, LoanTapeRepositoryFactory>();
            services.AddTransient<ILoanTapeServiceFactory, LoanTapeServiceFactory>();
            services.AddTransient<ILoanTapeFormulaFactory, LoanTapeFormulaFactory>();
            services.AddTransient<IAverageDailyBalanceFormula, AverageDailyBalanceFormula>();
            services.AddTransient<IChargeOffFormula, ChargeOffFormula>();
            services.AddTransient<IFeesAssessedFormula, FeesAssessedFormula>();
            services.AddTransient<IInterestAccruedFormula, InterestAccruedFormula>();
            services.AddTransient<IInterestPaidFormula, InterestPaidFormula>();
            services.AddTransient<ILoanAgeFormula, LoanAgeFormula>();
            services.AddTransient<INetInterestAccruedFormula, NetInterestAccruedFormula>();
            services.AddTransient<IPastDueAmountFormula, PastDueAmountFormula>();
            services.AddTransient<IPrincipalPaidFormula, PrincipalPaidFormula>();
            services.AddTransient<IBeginningPrincipalBalanceFormula, BeginningPrincipalBalanceFormula>();
            services.AddTransient<IEndingPrincipalBalanceFormula, EndingPrincipalBalanceFormula>();
            services.AddTransient<ITotalBorrowerPaymentFormula, TotalBorrowerPaymentFormula>();
            services.AddTransient<FormulaLookup>();
            
            services.AddTransient<ITransactionTapeService, TransactionTapeService>();
            services.AddTransient<ITransactionEntryFactory, TransactionEntryFactory>();
            services.AddTransient<ITransactionTapeServiceFactory, TransactionTapeServiceFactory>();
            services.AddTransient<ITransactionTapeRepository, TransactionTapeRepository>();
            services.AddTransient<ITransactionTapeRepositoryFactory, TransactionTapeRepositoryFactory>();

            services.AddTransient(p => p.GetService<IInvestorFeedServiceFactory>().Create(p.GetService<ITokenReader>()));
            services.AddTransient<IInvestorFeedEntryFactory, InvestorFeedEntryFactory>();
            services.AddTransient<IInvestorFeedServiceFactory, InvestorFeedServiceFactory>();
            services.AddTransient<IInvestorFeedRepository, InvestorFeedRepository>();
            services.AddTransient<IInvestorFeedRepositoryFactory, InvestorFeedRepositoryFactory>();

            services.AddTransient<IUploader, BoxUploader>();
            services.AddSingleton<Cache>();

            services.AddTransient<EventSink>();

            // eventhub factory
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.ApplicationServices.GetRequiredService<EventSink>().Start();
            app.UseMvc();
        }
    }
}
