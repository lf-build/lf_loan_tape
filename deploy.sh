#!/usr/bin/env bash

# set the docker image name
IMAGE_NAME=""
if [ -n "$1" ]
then
  IMAGE_NAME="$1"
else
  echo "You need to specify the name of image. Example: ./deploy.sh myservice"
  exit 1
fi

# ensure is logged in into our registry
docker login -u lendfoundry -p lendfoundry -e "" registry.lendfoundry.com

# build the current project
docker build -t registry.lendfoundry.com/$IMAGE_NAME . #:dev-$BUILD_NUMBER .

# publish the image to the registry
docker push registry.lendfoundry.com/$IMAGE_NAME #:dev-$BUILD_NUMBER
