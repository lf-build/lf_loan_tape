﻿using LendFoundry.Foundation.Date;
using LendFoundry.Loans.Tape.Monthly.Formulas;
using System;
using Xunit;

namespace LendFoundry.Loans.Tape.Monthly.Formulas.Tests
{
    public class LoanAgeFormulaTest
    {
        private ILoanAgeFormula Formula { get; } = new LoanAgeFormula();
        private ILoanTerms Terms { get; } = new LoanTerms();
        private ITenantTime TenantTime { get; } = new UtcTenantTime();

        private DateTimeOffset Date(int year, int month, int day) => TenantTime.Create(year, month, day);

        [Fact]
        public void ZeroWhenDateIsEqualToOriginationDate()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Assert.Equal(0, Formula.GetLoanAge(Terms.OriginationDate, Terms));
        }

        [Fact]
        public void ZeroWhenDateIsBefore1stAnniversaryDate()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Assert.Equal(0, Formula.GetLoanAge(Date(2015, 1, 14), Terms));
        }

        [Fact]
        public void OneWhenDateIsOn1stAnniversaryDateOrPastIt()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Assert.Equal(1, Formula.GetLoanAge(Date(2015, 1, 15), Terms));
            Assert.Equal(1, Formula.GetLoanAge(Date(2015, 2, 14), Terms));
        }

        [Fact]
        public void TwoWhenDateIsOn2ndAnniversaryDateOrPastIt()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Assert.Equal(2, Formula.GetLoanAge(Date(2015, 2, 15), Terms));
            Assert.Equal(2, Formula.GetLoanAge(Date(2015, 3, 14), Terms));
        }

        [Fact]
        public void ThreeWhenDateIsOn3rdAnniversaryDateOrPastIt()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Assert.Equal(3, Formula.GetLoanAge(Date(2015, 3, 15), Terms));
            Assert.Equal(3, Formula.GetLoanAge(Date(2015, 4, 14), Terms));
        }

        [Fact]
        public void ThirtySixWhenDateIsOnThe36thAnniversaryDateOrPastIt()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Assert.Equal(36, Formula.GetLoanAge(Date(2017, 12, 15), Terms));
            Assert.Equal(36, Formula.GetLoanAge(Date(2018,  1, 14), Terms));
        }

        [Fact]
        public void WithOriginationDateStartingOnThe31st()
        {
            Terms.OriginationDate = Date(2014, 12, 31);
            Assert.Equal( 0, Formula.GetLoanAge(Date(2014, 12, 31), Terms));
            Assert.Equal( 0, Formula.GetLoanAge(Date(2015,  1, 30), Terms));
            Assert.Equal( 1, Formula.GetLoanAge(Date(2015,  1, 31), Terms));
            Assert.Equal( 1, Formula.GetLoanAge(Date(2015,  2, 27), Terms));
            Assert.Equal( 2, Formula.GetLoanAge(Date(2015,  2, 28), Terms));
            Assert.Equal( 2, Formula.GetLoanAge(Date(2015,  3, 30), Terms));
            Assert.Equal( 3, Formula.GetLoanAge(Date(2015,  3, 31), Terms));
            Assert.Equal( 3, Formula.GetLoanAge(Date(2015,  4, 29), Terms));
            Assert.Equal( 4, Formula.GetLoanAge(Date(2015,  4, 30), Terms));
            Assert.Equal( 4, Formula.GetLoanAge(Date(2015,  5, 30), Terms));
            Assert.Equal( 5, Formula.GetLoanAge(Date(2015,  5, 31), Terms));
            Assert.Equal( 5, Formula.GetLoanAge(Date(2015,  6, 29), Terms));
            Assert.Equal( 6, Formula.GetLoanAge(Date(2015,  6, 30), Terms));
            Assert.Equal( 6, Formula.GetLoanAge(Date(2015,  7, 30), Terms));
            Assert.Equal( 7, Formula.GetLoanAge(Date(2015,  7, 31), Terms));
            Assert.Equal( 7, Formula.GetLoanAge(Date(2015,  8, 30), Terms));
            Assert.Equal( 8, Formula.GetLoanAge(Date(2015,  8, 31), Terms));
            Assert.Equal( 8, Formula.GetLoanAge(Date(2015,  9, 29), Terms));
            Assert.Equal( 9, Formula.GetLoanAge(Date(2015,  9, 30), Terms));
            Assert.Equal( 9, Formula.GetLoanAge(Date(2015, 10, 30), Terms));
            Assert.Equal(10, Formula.GetLoanAge(Date(2015, 10, 31), Terms));
            Assert.Equal(10, Formula.GetLoanAge(Date(2015, 11, 29), Terms));
            Assert.Equal(11, Formula.GetLoanAge(Date(2015, 11, 30), Terms));
            Assert.Equal(11, Formula.GetLoanAge(Date(2015, 12, 30), Terms));
            Assert.Equal(12, Formula.GetLoanAge(Date(2015, 12, 31), Terms));
            Assert.Equal(12, Formula.GetLoanAge(Date(2016,  1, 30), Terms));
        }
    }
}
