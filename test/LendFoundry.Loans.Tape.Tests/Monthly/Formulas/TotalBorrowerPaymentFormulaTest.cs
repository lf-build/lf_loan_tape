﻿using LendFoundry.Foundation.Date;
using LendFoundry.Loans.Tape.Monthly.Tests;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace LendFoundry.Loans.Tape.Monthly.Formulas.Tests
{
    public class TotalBorrowerPaymentFormulaTest
    {
        private ITotalBorrowerPaymentFormula Formula;
        private ITenantTime TenantTime { get; } = new UtcTenantTime();
        private DateTimeOffset Date(int year, int month, int day) => TenantTime.Create(year, month, day);
        private Mock<IInstallment> Installment() => new Mock<IInstallment>();
        private IPaymentSchedule Schedule(IEnumerable<IInstallment> installments) => new Mock<IPaymentSchedule>().With(installments).Object;

        public TotalBorrowerPaymentFormulaTest()
        {
            Formula = new TotalBorrowerPaymentFormula(TenantTime);
        }

        [Fact]
        public void ZeroWhenNoPaymentHasBeenMadeInThePeriod()
        {
            var installments = new[]
            {
                Installment().Of(151.1)
                    .Open()
                    .AnniversaryOn(Date(2016, 1, 25))
                    .DueOn(Date(2016, 1, 27))
                    .PaidOn(Date(2016, 1, 30)).Object,

                Installment().Of(229.3)
                    .Paid()
                    .AnniversaryOn(Date(2016, 2, 25))
                    .DueOn(Date(2016, 2, 27))
                    .PaidOn(Date(2016, 2, 28)).Object,

                Installment().Of(313.7)
                    .Open()
                    .AnniversaryOn(Date(2016, 3, 25))
                    .DueOn(Date(2016, 3, 27))
                    .PaidOn(Date(2016, 3, 31)).Object
            };

            Assert.Equal(0.0, Formula.Apply(Schedule(installments), Date(2016, 1, 15)));
            Assert.Equal(0.0, Formula.Apply(Schedule(installments), Date(2016, 3, 15)));
        }

        [Fact]
        public void EqualToThePaymentAmountWhenOnePaymentsHasBeenMadeInThePeriod()
        {
            var installments = new[]
            {
                Installment().Of(151.1)
                    .Paid()
                    .AnniversaryOn(Date(2016, 1, 25))
                    .DueOn(Date(2016, 1, 27))
                    .PaidOn(Date(2016, 1, 30)).Object,

                Installment().Of(229.3)
                    .Paid()
                    .AnniversaryOn(Date(2016, 2, 25))
                    .DueOn(Date(2016, 2, 27))
                    .PaidOn(Date(2016, 2, 28)).Object,

                Installment().Of(313.7)
                    .Paid()
                    .AnniversaryOn(Date(2016, 3, 25))
                    .DueOn(Date(2016, 3, 27))
                    .PaidOn(Date(2016, 3, 31)).Object
            };
            Assert.Equal(229.3, Formula.Apply(Schedule(installments), Date(2016, 2, 15)));
        }

        [Fact]
        public void EqualToTheSumOfAllPaymentAmountsWhenMultiplePaymentsHaveBeenMadeInThePeriod()
        {
            var installments = new[]
            {
                Installment().Of(151.1)
                    .Paid()
                    .AnniversaryOn(Date(2016, 1, 25))
                    .DueOn(Date(2016, 1, 27))
                    .PaidOn(Date(2016, 1, 30)).Object,

                Installment().Of(211.17)
                    .Paid()
                    .DueOn(Date(2016, 2, 10))
                    .PaidOn(Date(2016, 2, 10)).Object,

                Installment().Of(229.31)
                    .Paid()
                    .AnniversaryOn(Date(2016, 2, 25))
                    .DueOn(Date(2016, 2, 26))
                    .PaidOn(Date(2016, 2, 27)).Object,

                Installment().Of(263.83)
                    .Paid()
                    .DueOn(Date(2016, 2, 27))
                    .PaidOn(Date(2016, 2, 28)).Object,

                Installment().Of(313.7)
                    .Paid()
                    .AnniversaryOn(Date(2016, 3, 25))
                    .DueOn(Date(2016, 3, 27))
                    .PaidOn(Date(2016, 3, 31)).Object
            };
            Assert.Equal(211.17 + 229.31 + 263.83, Formula.Apply(Schedule(installments), Date(2016, 2, 15)));
        }

        [Fact]
        public void DoNotIncludeOpenInstallmentsInThePeriod()
        {
            var installments = new[]
            {
                Installment().Of(151.1)
                    .Paid()
                    .AnniversaryOn(Date(2016, 1, 25))
                    .DueOn(Date(2016, 1, 27))
                    .PaidOn(Date(2016, 1, 30)).Object,

                Installment().Of(211.17)
                    .Paid()
                    .DueOn(Date(2016, 2, 8))
                    .PaidOn(Date(2016, 2, 9)).Object,

                Installment().Of(229.31)
                    .Open()
                    .AnniversaryOn(Date(2016, 2, 25))
                    .DueOn(Date(2016, 2, 26))
                    .PaidOn(Date(2016, 2, 27)).Object,

                Installment().Of(263.83)
                    .Paid()
                    .DueOn(Date(2016, 2, 27))
                    .PaidOn(Date(2016, 2, 28)).Object,

                Installment().Of(313.7)
                    .Paid()
                    .AnniversaryOn(Date(2016, 3, 25))
                    .DueOn(Date(2016, 3, 27))
                    .PaidOn(Date(2016, 3, 31)).Object
            };

            Assert.Equal(211.17 + 263.83, Formula.Apply(Schedule(installments), Date(2016, 2, 15)));
        }

        [Fact]
        public void ExcludeChargeOffInstallments()
        {
            var installments = new[]
            {
                Installment().Of(151.1)
                    .Paid()
                    .AnniversaryOn(Date(2016, 1, 25))
                    .DueOn(Date(2016, 1, 27))
                    .PaidOn(Date(2016, 1, 30)).Object,

                Installment().Of(211.17)
                    .Paid()
                    .DueOn(Date(2016, 2, 8))
                    .PaidOn(Date(2016, 2, 9)).Object,

                Installment().Of(229.31)
                    .Paid()
                    .Type(InstallmentType.ChargeOff)
                    .AnniversaryOn(Date(2016, 2, 25))
                    .DueOn(Date(2016, 2, 26))
                    .PaidOn(Date(2016, 2, 27)).Object,

                Installment().Of(263.83)
                    .Paid()
                    .DueOn(Date(2016, 2, 27))
                    .PaidOn(Date(2016, 2, 28)).Object,

                Installment().Of(313.7)
                    .Paid()
                    .AnniversaryOn(Date(2016, 3, 25))
                    .DueOn(Date(2016, 3, 27))
                    .PaidOn(Date(2016, 3, 31)).Object
            };

            Assert.Equal(211.17 + 263.83, Formula.Apply(Schedule(installments), Date(2016, 2, 15)));
        }

        [Fact]
        public void FindInstallmentsPaidInThePeriodByLookingAtThePaymentDate()
        {
            var installments = new[]
            {
                Installment().Of(151.1)
                    .Paid()
                    .AnniversaryOn(Date(2016, 1, 25))
                    .DueOn(Date(2016, 1, 27))
                    .PaidOn(Date(2016, 1, 30)).Object,

                Installment().Of(211.17)
                    .Paid()
                    .DueOn(Date(2016, 1, 30))
                    .PaidOn(Date(2016, 2, 1)).Object,

                Installment().Of(229.31)
                    .Paid()
                    .AnniversaryOn(Date(2016, 2, 25))
                    .DueOn(Date(2016, 2, 27))
                    .PaidOn(Date(2016, 2, 28)).Object,

                Installment().Of(263.83)
                    .Paid()
                    .DueOn(Date(2016, 2, 27))
                    .PaidOn(Date(2016, 3, 1)).Object,

                Installment().Of(313.7)
                    .Paid()
                    .AnniversaryOn(Date(2016, 3, 25))
                    .DueOn(Date(2016, 3, 27))
                    .PaidOn(Date(2016, 3, 31)).Object
            };

            Assert.Equal(211.17 + 229.31, Formula.Apply(Schedule(installments), Date(2016, 2, 15)));
        }
    }
}
