﻿using LendFoundry.Loans.Tape.Monthly.Tests;
using Moq;
using Xunit;

namespace LendFoundry.Loans.Tape.Monthly.Formulas.Tests
{
    public class PrincipalPaidFormulaTest : ScheduledBasedFormulaTest
    {
        private IPrincipalPaidFormula Formula { get; }
        private Mock<IInstallment> Installment() => new Mock<IInstallment>();

        public PrincipalPaidFormulaTest()
        {
            Formula = new PrincipalPaidFormula(TenantTime);
        }

        [Fact]
        public void NoPayment()
        {
            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Scheduled
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Scheduled
                2015-03-22      | 2015-03-24 | 2015-03-24  |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Scheduled
                2015-04-22      | 2015-04-24 | 2015-04-24  |       12655.61 |       4289.03 |   105.46 |   4183.57 |       8472.04 | Scheduled | Scheduled
                2015-05-22      | 2015-05-24 | 2015-05-24  |        8472.04 |       4289.03 |    70.60 |   4218.43 |       4253.61 | Scheduled | Scheduled
                2015-06-22      | 2015-06-24 | 2015-06-24  |        4253.61 |       4289.06 |    35.45 |   4253.61 |          0.00 | Scheduled | Scheduled
            ");

            Assert.Equal(0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
        }
        
        [Fact]
        public void PerfectPay()
        {
            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Completed
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Completed
                2015-03-22      | 2015-03-24 | 2015-03-24  |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Completed
                2015-04-22      | 2015-04-24 | 2015-04-24  |       12655.61 |       4289.03 |   105.46 |   4183.57 |       8472.04 | Scheduled | Completed
                2015-05-22      | 2015-05-24 | 2015-05-24  |        8472.04 |       4289.03 |    70.60 |   4218.43 |       4253.61 | Scheduled | Completed
                2015-06-22      | 2015-06-24 | 2015-06-24  |        4253.61 |       4289.06 |    35.45 |   4253.61 |          0.00 | Scheduled | Completed
            ");

            Assert.Equal(   0.00, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(4080.70, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(4114.70, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(4148.99, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(4183.57, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal(4218.43, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal(4253.61, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(   0.00, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
        }

        [Fact]
        public void FirstThreeInstallmentsPaid()
        {
            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Completed
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Completed
                2015-03-22      | 2015-03-24 | 2015-03-24  |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Completed
                2015-04-22      | 2015-04-24 |             |       12655.61 |       4289.03 |   105.46 |   4183.57 |       8472.04 | Scheduled | Scheduled
                2015-05-22      | 2015-05-24 |             |        8472.04 |       4289.03 |    70.60 |   4218.43 |       4253.61 | Scheduled | Scheduled
                2015-06-22      | 2015-06-24 |             |        4253.61 |       4289.06 |    35.45 |   4253.61 |          0.00 | Scheduled | Scheduled
            ");

            Assert.Equal(   0.00, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(4080.70, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(4114.70, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(4148.99, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(   0.00, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal(   0.00, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal(   0.00, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(   0.00, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
        }

        [Fact]
        public void ExtraPayments()
        {
            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type       | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled  | Completed
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled  | Completed
                                | 2015-03-09 | 2015-03-09  |       16804.60 |        999.00 |     0.00 |    999.00 |      15805.60 | Additional | Completed
                2015-03-22      | 2015-03-24 | 2015-03-24  |       15805.60 |       4289.03 |   136.43 |   4152.60 |      11653.00 | Scheduled  | Completed
                                | 2015-03-30 | 2015-03-30  |       11653.00 |        888.00 |     0.00 |    888.00 |      10765.00 | Additional | Completed
                2015-04-22      | 2015-04-24 | 2015-04-24  |       10765.00 |       4289.03 |    91.68 |   4197.35 |       6567.65 | Scheduled  | Completed
                2015-05-22      | 2015-05-24 | 2015-05-24  |        6567.65 |       4289.03 |    54.73 |   4234.30 |       2333.35 | Scheduled  | Completed
                2015-06-22      | 2015-06-24 | 2015-06-24  |        2333.35 |       2352.79 |    19.44 |   2333.35 |          0.00 | Scheduled  | Completed
            ");

            Assert.Equal(   0.00, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(4080.70, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(4114.70, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(6039.60, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(4197.35, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal(4234.30, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal(2333.35, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(   0.00, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
        }
        
        [Fact]
        public void ConsiderPaymentDatesOnly()
        {
            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type       | Status   
                2015-01-31      | 2015-02-02 | 2015-02-02  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled  | Completed
                2015-02-28      | 2015-03-02 | 2015-03-02  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled  | Completed
                                | 2015-03-09 | 2015-03-09  |       16804.60 |        777.07 |     0.00 |    777.07 |      16027.53 | Additional | Completed
                2015-03-31      | 2015-04-02 | 2015-04-02  |       16027.53 |       4289.03 |   139.96 |   4149.07 |      11878.46 | Scheduled  | Completed
                                | 2015-04-10 | 2015-04-10  |       11878.46 |        888.08 |     0.00 |    888.08 |      10990.38 | Additional | Completed
                2015-04-30      | 2015-05-02 | 2015-05-02  |       10990.38 |       4289.03 |    94.05 |   4194.98 |       6795.40 | Scheduled  | Completed
                                | 2015-04-30 | 2015-04-30  |        6795.40 |        999.09 |     0.00 |    999.09 |       5796.31 | Additional | Completed
                2015-05-31      | 2015-06-02 | 2015-06-02  |        5796.31 |       4289.03 |    48.30 |   4240.73 |       1555.58 | Scheduled  | Completed
                2015-06-30      | 2015-07-02 | 2015-07-02  |        1555.58 |       1568.54 |    12.96 |   1555.58 |          0.00 | Scheduled  | Completed
            ");
            
            Assert.Equal(   0.00, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(   0.00, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(4080.70, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(4891.77, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(6036.24, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal(4194.98, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal(4240.73, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(1555.58, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
            Assert.Equal(   0.00, Formula.Apply(Date(2015,  8, 31), Terms, Schedule));
        }

        [Fact]
        public void ExcludeChargeOffInstallments()
        {
            Schedule = Mock.Of<IPaymentSchedule>(s => s.Installments == new[]
            {
                Installment().Principal(211.17)
                    .Paid()
                    .DueOn(Date(2016, 2, 8))
                    .PaidOn(Date(2016, 2, 9)).Object,

                Installment().Principal(229.31)
                    .Paid()
                    .Type(InstallmentType.ChargeOff)
                    .AnniversaryOn(Date(2016, 2, 25))
                    .DueOn(Date(2016, 2, 26))
                    .PaidOn(Date(2016, 2, 27)).Object,

                Installment().Principal(263.83)
                    .Paid()
                    .DueOn(Date(2016, 2, 27))
                    .PaidOn(Date(2016, 2, 28)).Object,
            });

            Assert.Equal(475.00, Formula.Apply(Date(2016, 2, 28), Terms, Schedule));
        }
    }
}
