﻿using LendFoundry.Loans.Tape.Monthly.Formulas;
using Moq;
using Xunit;

namespace LendFoundry.Loans.Tape.Monthly.Formulas.Tests
{
    public class BeginningPrincipalBalanceFormulaTest : ScheduledBasedFormulaTest
    {
        private IBeginningPrincipalBalanceFormula Formula { get; }

        public BeginningPrincipalBalanceFormulaTest()
        {
            Formula = new BeginningPrincipalBalanceFormula(TenantTime);
        }

        [Fact]
        public void BalanceIsZeroWhenOriginationDateIsOnThe1stOfTheReferenceMonth()
        {
            Terms.OriginationDate = Date(2014, 12, 1);
            Terms.LoanAmount = 9999.99;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-01      | 2015-01-03 |             |        9999.99 |       2062.14 |   102.87 |   1959.27 |       8040.72 | Scheduled | Scheduled
                2015-02-01      | 2015-02-03 |             |        8040.72 |       2062.14 |    82.72 |   1979.42 |       6061.30 | Scheduled | Scheduled
                2015-03-01      | 2015-03-03 |             |        6061.30 |       2062.14 |    62.36 |   1999.78 |       4061.52 | Scheduled | Scheduled
                2015-04-01      | 2015-04-03 |             |        4061.52 |       2062.14 |    41.78 |   2020.36 |       2041.16 | Scheduled | Scheduled
                2015-05-01      | 2015-05-03 |             |        2041.16 |       2062.16 |    21.00 |   2041.16 |          0.00 | Scheduled | Scheduled");

            Assert.Equal(0.0, Formula.Apply(Date(2014, 12, 15), Terms, Schedule));
        }

        [Fact]
        public void BalanceIsZeroWhenOriginationDateIsInTheMiddleOfTheReferenceMonth()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9999.99;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-15      | 2015-01-17 |             |        9999.99 |       2062.14 |   102.87 |   1959.27 |       8040.72 | Scheduled | Scheduled
                2015-02-15      | 2015-02-17 |             |        8040.72 |       2062.14 |    82.72 |   1979.42 |       6061.30 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 |             |        6061.30 |       2062.14 |    62.36 |   1999.78 |       4061.52 | Scheduled | Scheduled
                2015-04-15      | 2015-04-17 |             |        4061.52 |       2062.14 |    41.78 |   2020.36 |       2041.16 | Scheduled | Scheduled
                2015-05-15      | 2015-05-17 |             |        2041.16 |       2062.16 |    21.00 |   2041.16 |          0.00 | Scheduled | Scheduled");

            Assert.Equal(0.0, Formula.Apply(Date(2014, 12, 15), Terms, Schedule));
        }

        [Fact]
        public void BalanceIsZeroWhenOriginationDateIsAtTheEndOfTheReferenceMonth()
        {
            Terms.OriginationDate = Date(2014, 12, 31);
            Terms.LoanAmount = 9999.99;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-31      | 2015-01-31 |             |        9999.99 |       2062.14 |   102.87 |   1959.27 |       8040.72 | Scheduled | Scheduled
                2015-02-28      | 2015-02-28 |             |        8040.72 |       2062.14 |    82.72 |   1979.42 |       6061.30 | Scheduled | Scheduled
                2015-03-31      | 2015-03-31 |             |        6061.30 |       2062.14 |    62.36 |   1999.78 |       4061.52 | Scheduled | Scheduled
                2015-04-30      | 2015-04-30 |             |        4061.52 |       2062.14 |    41.78 |   2020.36 |       2041.16 | Scheduled | Scheduled
                2015-05-31      | 2015-05-31 |             |        2041.16 |       2062.16 |    21.00 |   2041.16 |          0.00 | Scheduled | Scheduled");

            Assert.Equal(0.0, Formula.Apply(Date(2014, 12, 15), Terms, Schedule));
        }

        [Fact]
        public void BalanceIsEqualToLoanAmountWhenOriginationDateIsOnTheLastDayOfThePreviousMonth()
        {
            Terms.OriginationDate = Date(2014, 11, 30);
            Terms.LoanAmount = 9999.99;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-30      | 2015-01-30 |             |        9999.99 |       2062.14 |   102.87 |   1959.27 |       8040.72 | Scheduled | Scheduled
                2015-02-28      | 2015-02-28 |             |        8040.72 |       2062.14 |    82.72 |   1979.42 |       6061.30 | Scheduled | Scheduled
                2015-03-30      | 2015-03-30 |             |        6061.30 |       2062.14 |    62.36 |   1999.78 |       4061.52 | Scheduled | Scheduled
                2015-04-30      | 2015-04-30 |             |        4061.52 |       2062.14 |    41.78 |   2020.36 |       2041.16 | Scheduled | Scheduled
                2015-05-30      | 2015-05-30 |             |        2041.16 |       2062.16 |    21.00 |   2041.16 |          0.00 | Scheduled | Scheduled");

            Assert.Equal(9999.99, Formula.Apply(Date(2014, 12, 15), Terms, Schedule));
        }

        [Fact]
        public void BalanceIsEqualToLoanAmountWhenNoPaymentHasBeenMade()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9999.99;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-15      | 2015-01-17 |             |        9999.99 |       2062.14 |   102.87 |   1959.27 |       8040.72 | Scheduled | Scheduled
                2015-02-15      | 2015-02-17 |             |        8040.72 |       2062.14 |    82.72 |   1979.42 |       6061.30 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 |             |        6061.30 |       2062.14 |    62.36 |   1999.78 |       4061.52 | Scheduled | Scheduled
                2015-04-15      | 2015-04-17 |             |        4061.52 |       2062.14 |    41.78 |   2020.36 |       2041.16 | Scheduled | Scheduled
                2015-05-15      | 2015-05-17 |             |        2041.16 |       2062.16 |    21.00 |   2041.16 |          0.00 | Scheduled | Scheduled");

            Assert.Equal(9999.99, Formula.Apply(Date(2015, 1, 15), Terms, Schedule));
            Assert.Equal(9999.99, Formula.Apply(Date(2015, 2, 15), Terms, Schedule));
            Assert.Equal(9999.99, Formula.Apply(Date(2015, 3, 15), Terms, Schedule));
            Assert.Equal(9999.99, Formula.Apply(Date(2015, 4, 15), Terms, Schedule));
            Assert.Equal(9999.99, Formula.Apply(Date(2015, 5, 15), Terms, Schedule));
        }

        [Fact]
        public void BalanceIsTheEndingBalanceOfTheLastPaymentMadeBeforeTheBeginningOfTheMonth()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9999.99;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-15      | 2015-01-17 | 2015-01-17  |        9999.99 |       2062.14 |   102.87 |   1959.27 |       8040.72 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | 2015-02-17  |        8040.72 |       2062.14 |    82.72 |   1979.42 |       6061.30 | Scheduled | Completed
                                | 2015-02-28 | 2015-02-28  |        6061.30 |        900.09 |     0.00 |    900.09 |       5161.21 | Scheduled | Completed
                2015-03-15      | 2015-03-17 | 2015-03-17  |        5161.21 |       2062.14 |    53.10 |   2009.04 |       3152.17 | Scheduled | Completed
                2015-04-15      | 2015-04-17 | 2015-04-17  |        3152.17 |       2062.14 |    32.43 |   2029.71 |       1122.46 | Scheduled | Scheduled
                2015-05-15      | 2015-05-17 | 2015-05-17  |        1122.46 |       1134.01 |    11.55 |   1122.46 |          0.00 | Scheduled | Scheduled");

            Assert.Equal(9999.99, Formula.Apply(Date(2015, 1, 15), Terms, Schedule));
            Assert.Equal(8040.72, Formula.Apply(Date(2015, 2, 15), Terms, Schedule));
            Assert.Equal(5161.21, Formula.Apply(Date(2015, 3, 15), Terms, Schedule));
            Assert.Equal(3152.17, Formula.Apply(Date(2015, 4, 15), Terms, Schedule));
            Assert.Equal(3152.17, Formula.Apply(Date(2015, 5, 15), Terms, Schedule));
            Assert.Equal(3152.17, Formula.Apply(Date(2015, 6, 15), Terms, Schedule));
            Assert.Equal(3152.17, Formula.Apply(Date(2015, 7, 15), Terms, Schedule));
        }
                
        [Fact]
        public void TakeTheLowestEndingBalanceEvenWhenPaymentsAreNotProperlyOrdered()
        {
            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type       | Status   
                2015-01-15      | 2015-01-17 | 2015-01-17  |        9999.99 |       2062.14 |   102.87 |   1959.27 |       8040.72 | Scheduled  | Completed
                                | 2015-02-23 | 2015-02-23  |        6061.30 |        900.09 |     0.00 |    900.09 |       5161.21 | Additional | Completed
                2015-02-15      | 2015-02-17 | 2015-02-25  |        8040.72 |       2062.14 |    82.72 |   1979.42 |       6061.30 | Scheduled  | Completed
                2015-03-15      | 2015-03-17 | 2015-03-17  |        5161.21 |       2062.14 |    53.10 |   2009.04 |       3152.17 | Scheduled  | Scheduled
                2015-04-15      | 2015-04-17 | 2015-04-17  |        3152.17 |       2062.14 |    32.43 |   2029.71 |       1122.46 | Scheduled  | Scheduled
                2015-05-15      | 2015-05-17 | 2015-05-17  |        1122.46 |       1134.01 |    11.55 |   1122.46 |          0.00 | Scheduled  | Scheduled");

            Assert.Equal(5161.21, Formula.Apply(Date(2015, 3, 15), Terms, Schedule));
        }

        [Fact]
        public void AlwaysConsiderPaymentDate()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9999.99;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-30      | 2015-01-30 | 2015-02-01  |        9999.99 |       2062.14 |   102.87 |   1959.27 |       8040.72 | Scheduled | Completed
                2015-02-28      | 2015-02-28 | 2015-02-28  |        8040.72 |       2062.14 |    82.72 |   1979.42 |       6061.30 | Scheduled | Completed
                2015-03-30      | 2015-03-31 | 2015-04-01  |        6061.30 |       2062.14 |    62.36 |   1999.78 |       4061.52 | Scheduled | Completed
                2015-04-30      | 2015-04-30 |             |        4061.52 |       2062.14 |    41.78 |   2020.36 |       2041.16 | Scheduled | Scheduled
                2015-05-30      | 2015-05-30 |             |        2041.16 |       2062.16 |    21.00 |   2041.16 |          0.00 | Scheduled | Scheduled");

            Assert.Equal(9999.99, Formula.Apply(Date(2015, 1, 15), Terms, Schedule));
            Assert.Equal(9999.99, Formula.Apply(Date(2015, 2, 15), Terms, Schedule));
            Assert.Equal(6061.30, Formula.Apply(Date(2015, 3, 15), Terms, Schedule));
            Assert.Equal(6061.30, Formula.Apply(Date(2015, 4, 15), Terms, Schedule));
            Assert.Equal(4061.52, Formula.Apply(Date(2015, 5, 15), Terms, Schedule));
            Assert.Equal(4061.52, Formula.Apply(Date(2015, 6, 15), Terms, Schedule));
        }

        [Fact]
        public void ExtraPaymentBefore1stInstallment()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9999.99;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                                | 2014-12-31 | 2014-12-31  |        9999.99 |        543.21 |     0.00 |    543.21 |       9456.78 | Scheduled | Completed
                2015-01-15      | 2015-01-17 | 2015-01-17  |        9456.78 |       2062.14 |    97.29 |   1964.85 |       7491.93 | Scheduled | Scheduled
                2015-02-15      | 2015-02-17 | 2015-02-17  |        7491.93 |       2062.14 |    77.07 |   1985.07 |       5506.86 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | 2015-03-17  |        5506.86 |       2062.14 |    56.65 |   2005.49 |       3501.37 | Scheduled | Scheduled
                2015-04-15      | 2015-04-17 | 2015-04-17  |        3501.37 |       2062.14 |    36.02 |   2026.12 |       1475.25 | Scheduled | Scheduled
                2015-05-15      | 2015-05-17 | 2015-05-17  |        1475.25 |       1490.43 |    15.18 |   1475.25 |          0.00 | Scheduled | Scheduled");

            Assert.Equal(9456.78, Formula.Apply(Date(2015, 1, 15), Terms, Schedule));
            Assert.Equal(9456.78, Formula.Apply(Date(2015, 2, 15), Terms, Schedule));
            Assert.Equal(9456.78, Formula.Apply(Date(2015, 3, 15), Terms, Schedule));
            Assert.Equal(9456.78, Formula.Apply(Date(2015, 4, 15), Terms, Schedule));
            Assert.Equal(9456.78, Formula.Apply(Date(2015, 5, 15), Terms, Schedule));
            Assert.Equal(9456.78, Formula.Apply(Date(2015, 6, 15), Terms, Schedule));
        }

        [Fact]
        public void PaymentsOnTheSameDate()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9999.99;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type       | Status   
                2015-01-15      | 2015-01-15 | 2015-01-09  |        9999.99 |       2062.14 |   102.87 |   1959.27 |       8040.72 | Scheduled  | Completed
                                | 2015-01-09 | 2015-01-09  |        8040.72 |        101.01 |     0.00 |    101.01 |       7939.71 | Additional | Completed
                2015-02-15      | 2015-02-15 | 2015-02-13  |        7939.71 |       2062.14 |    81.68 |   1980.46 |       5959.25 | Scheduled  | Completed
                                | 2015-02-13 | 2015-02-13  |        5959.25 |        202.02 |     0.00 |    202.02 |       5757.23 | Additional | Completed
                2015-03-15      | 2015-03-15 | 2015-03-15  |        5757.23 |       2062.14 |    59.23 |   2002.91 |       3754.32 | Scheduled  | Completed
                2015-04-15      | 2015-04-15 | 2015-04-10  |        3754.32 |       2062.14 |    38.62 |   2023.52 |       1730.80 | Scheduled  | Completed
                2015-05-15      | 2015-05-15 | 2015-05-15  |        1730.80 |       1748.61 |    17.81 |   1730.80 |          0.00 | Scheduled  | Completed");

            Assert.Equal(   0.00, Formula.Apply(Date(2014, 12, 15), Terms, Schedule));
            Assert.Equal(9999.99, Formula.Apply(Date(2015,  1, 15), Terms, Schedule));
            Assert.Equal(7939.71, Formula.Apply(Date(2015,  2, 15), Terms, Schedule));
            Assert.Equal(5757.23, Formula.Apply(Date(2015,  3, 15), Terms, Schedule));
            Assert.Equal(3754.32, Formula.Apply(Date(2015,  4, 15), Terms, Schedule));
            Assert.Equal(1730.80, Formula.Apply(Date(2015,  5, 15), Terms, Schedule));
            Assert.Equal(   0.00, Formula.Apply(Date(2015,  6, 15), Terms, Schedule));
        }

        [Fact]
        public void BeginningBalanceIsEqualToPreviousTapeEndingBalanceWhenAvailable()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9999.99;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type       | Status   
                2015-01-15      | 2015-01-15 | 2015-01-09  |        9999.99 |       2062.14 |   102.87 |   1959.27 |       8040.72 | Scheduled  | Completed
                                | 2015-01-09 | 2015-01-09  |        8040.72 |        101.01 |     0.00 |    101.01 |       7939.71 | Additional | Completed
                2015-02-15      | 2015-02-15 | 2015-02-13  |        7939.71 |       2062.14 |    81.68 |   1980.46 |       5959.25 | Scheduled  | Completed
                                | 2015-02-13 | 2015-02-13  |        5959.25 |        202.02 |     0.00 |    202.02 |       5757.23 | Additional | Completed
                2015-03-15      | 2015-03-15 | 2015-03-15  |        5757.23 |       2062.14 |    59.23 |   2002.91 |       3754.32 | Scheduled  | Completed
                2015-04-15      | 2015-04-15 | 2015-04-10  |        3754.32 |       2062.14 |    38.62 |   2023.52 |       1730.80 | Scheduled  | Completed
                2015-05-15      | 2015-05-15 | 2015-05-15  |        1730.80 |       1748.61 |    17.81 |   1730.80 |          0.00 | Scheduled  | Completed");

            var previousTape = Mock.Of<ILoanTape>(t => t.EndingPrincipalBalance == 9999.99);

            Assert.Equal(7939.71, Formula.Apply(Date(2015,  2, 28), Terms, Schedule, null));
            Assert.Equal(9999.99, Formula.Apply(Date(2015,  2, 28), Terms, Schedule, previousTape));
        }
    }
}
