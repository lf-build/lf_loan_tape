﻿using LendFoundry.Loans.Tape.Monthly.Formulas;
using System;
using Xunit;

namespace LendFoundry.Loans.Tape.Monthly.Formulas.Tests
{
    public class FeesAssessedFormulaTest : TransactionBasedFormulaTest
    {
        private const string FeeCode1 = "001";
        private const string FeeCode2 = "002";
        private const string FeeCode3 = "003";

        private IFeesAssessedFormula Formula { get; }

        public FeesAssessedFormulaTest()
        {
            Formula = new FeesAssessedFormula(TenantTime);
        }

        private void AssertFeesAssessed(double expectedFeesAssessed, DateTimeOffset referenceDate)
        {
            var feeCodes = new[] { FeeCode1, FeeCode2, FeeCode3 };
            Assert.Equal(expectedFeesAssessed, Formula.Apply(referenceDate, Transactions, feeCodes));
        }

        [Fact]
        public void ZeroWhenNoTransactionIsFound()
        {
            AssertFeesAssessed(0.0, Date(2015, 1, 1));
        }

        [Fact]
        public void ZeroWhenNoLateFeeTransactionIsFound()
        {
            AddTransaction(Date(2014, 12, 31), AnyTransactionCode, 10.0);
            AddTransaction(Date(2015,  1,  1), AnyTransactionCode, 20.0);
            AddTransaction(Date(2015,  1, 15), AnyTransactionCode, 30.0);
            AddTransaction(Date(2015,  1, 31), AnyTransactionCode, 40.0);
            AddTransaction(Date(2015,  2,  1), AnyTransactionCode, 50.0);

            AssertFeesAssessed(0.0, Date(2014, 12, 31));
            AssertFeesAssessed(0.0, Date(2015,  1, 31));
            AssertFeesAssessed(0.0, Date(2015,  2, 28));
        }

        [Fact]
        public void ZeroWhenNoLateFeeTransactionIsFoundInTheReferencePeriod()
        {
            AddTransaction(Date(2014, 12, 31), FeeCode1, 10.0);
            AddTransaction(Date(2015,  1,  1), AnyTransactionCode, 20.0);
            AddTransaction(Date(2015,  1, 15), AnyTransactionCode, 30.0);
            AddTransaction(Date(2015,  1, 31), AnyTransactionCode, 40.0);
            AddTransaction(Date(2015,  2,  1), FeeCode2, 50.0);
            AddTransaction(Date(2015,  2,  1), FeeCode3, 60.0);

            AssertFeesAssessed(0.0, Date(2015,  1, 31));
        }

        [Fact]
        public void LateFeeTransactionIsFoundInTheReferencePeriod()
        {
            AddTransaction(Date(2014, 12, 10), FeeCode1, 11.01);
            AddTransaction(Date(2014, 12, 20), AnyTransactionCode, 22.02);
            AddTransaction(Date(2014, 12, 31), FeeCode2, 33.03);

            // Beginnig of reference period
            AddTransaction(Date(2015,  1,  1), FeeCode1, 44.04);
            AddTransaction(Date(2015,  1, 10), AnyTransactionCode, 55.05);
            AddTransaction(Date(2015,  1, 15), FeeCode2, 66.06);
            AddTransaction(Date(2015,  1, 20), AnyTransactionCode, 77.07);
            AddTransaction(Date(2015,  1, 31), FeeCode3, 88.08);
            // End of reference period

            AddTransaction(Date(2015,  2,  1), FeeCode1, 99.09);
            AddTransaction(Date(2015,  2,  1), AnyTransactionCode, 101.01);
            AddTransaction(Date(2015,  2,  1), FeeCode2, 202.02);

            AssertFeesAssessed(198.18, Date(2015, 1, 31));
        }

        [Fact]
        public void RoundOffToTwoDigits()
        {
            AddTransaction(Date(2015, 1,  1), FeeCode1, 44.041);
            AddTransaction(Date(2015, 1, 15), FeeCode2, 66.062);
            AddTransaction(Date(2015, 1, 31), FeeCode3, 88.083);

            AssertFeesAssessed(198.19, Date(2015, 1, 31));
        }
    }
}
