﻿using LendFoundry.Loans.Schedule;
using LendFoundry.Loans.Tape.Monthly.Formulas;
using Moq;
using Xunit;

namespace LendFoundry.Loans.Tape.Monthly.Formulas.Tests
{
    public class PastDueAmountFormulaTest
    {
        private IPastDueAmountFormula Formula = new PastDueAmountFormula();
        private Mock<IPaymentScheduleSummary> Summary = new Mock<IPaymentScheduleSummary>();

        [Fact]
        public void ZeroWhenNotPastDue()
        {
            Summary.Setup(s => s.CurrentDue).Returns(999.99);
            Summary.Setup(s => s.DaysPastDue).Returns(0);

            Assert.Equal(0.0, Formula.Apply(Summary.Object));
        }

        [Fact]
        public void SameAsCurrentDueWhenPastDue()
        {
            Summary.Setup(s => s.CurrentDue).Returns(999.99);
            Summary.Setup(s => s.DaysPastDue).Returns(1);

            Assert.Equal(999.99, Formula.Apply(Summary.Object));
        }
    }
}
