﻿using LendFoundry.Foundation.Date;
using LendFoundry.Loans.Tape.Monthly.Tests;
using Moq;
using System;

namespace LendFoundry.Loans.Tape.Monthly.Formulas.Tests
{
    public class FormulaTestBase
    {
        protected ITenantTime TenantTime { get; } = new UtcTenantTime();
        protected DateTimeOffset Date(int year, int month, int day) => TenantTime.Create(year, month, day);
        protected Mock<ILoan> Loan() => new Mock<ILoan>();
        protected Mock<IInstallment> Installment() => new Mock<IInstallment>().Type(InstallmentType.Scheduled);
        protected Mock<IInstallment> ExtraPayment() => new Mock<IInstallment>().Type(InstallmentType.Additional);
        protected IPaymentSchedule Schedule(params IInstallment[] installments) => new Mock<IPaymentSchedule>().With(installments).Object;
        protected double Round(double v) => Math.Round(v, 2, MidpointRounding.AwayFromZero);
    }
}
