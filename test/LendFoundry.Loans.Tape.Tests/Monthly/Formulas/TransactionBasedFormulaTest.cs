﻿using LendFoundry.Foundation.Date;
using Moq;
using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Tape.Monthly.Formulas.Tests
{
    public abstract class TransactionBasedFormulaTest
    {
        protected const string AnyTransactionCode = "99999999";

        protected ITenantTime TenantTime { get; } = new UtcTenantTime();
        protected List<ITransaction> Transactions { get; } = new List<ITransaction>();

        protected DateTimeOffset Date(int year, int month, int day) { return TenantTime.Create(year, month, day); }

        protected void AddTransaction(DateTimeOffset date, string code, double amount)
        {
            Transactions.Add(Mock.Of<ITransaction>(t =>
                t.Date == date &&
                t.Code == code &&
                t.Amount == amount
            ));
        }
    }
}
