﻿using LendFoundry.Loans.Tape.Configuration;
using Xunit;

namespace LendFoundry.Loans.Tape.Monthly.Formulas.Tests
{
    public class ChargeOffFormulaTest : TransactionBasedFormulaTest
    {
        private IChargeOffFormula Formula { get; }
        private TapeServiceConfiguration Configuration { get; }

        public ChargeOffFormulaTest()
        {
            Configuration = new TapeServiceConfiguration { TransactionCodes = new Configuration.TransactionCodes { ChargeOff = "999" } };
            Formula = new ChargeOffFormula(Configuration);

            AddTransaction(Date(2015, 1, 15), "111", 111.11);
            AddTransaction(Date(2015, 2, 15), "222", 222.22);
            AddTransaction(Date(2015, 3, 15), "333", 333.33);
        }

        [Fact]
        public void NoChargeOffTransaction()
        {
            ChargeOff chargeOff = Formula.Apply(Transactions);
            Assert.NotNull(chargeOff);
            Assert.Null(chargeOff.Date);
            Assert.Equal(0.0, chargeOff.Amount);
        }

        [Fact]
        public void GetChargeOffInformationFromChargeOffTransaction()
        {
            AddTransaction(Date(2015, 2, 15), "999", 2500.0);

            ChargeOff chargeOff = Formula.Apply(Transactions);
            Assert.NotNull(chargeOff);
            Assert.Equal(Date(2015, 2, 15), chargeOff.Date);
            Assert.Equal(2500.0, chargeOff.Amount);
        }
    }
}
