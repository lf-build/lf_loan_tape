﻿using LendFoundry.Foundation.Testing.Date;
using LendFoundry.Loans.Tape.Configuration;
using Moq;
using System;
using System.Globalization;
using Xunit;

namespace LendFoundry.Loans.Tape.Monthly.Tests
{
    public class LoanTapeFormulaTest
    {
        private TenantTimeMock TenantTime { get; set; } = new TenantTimeMock();
        private Mock<ILoanDetails> MockLoanDetail { get; set; } = new Mock<ILoanDetails>();

        private LoanTapeFormula Formula(DateTimeOffset referenceDate)
        {
            return new LoanTapeFormula
            (
                MockLoanDetail.Object,
                referenceDate,
                Configuration,
                TenantTime
            );
        }

        private LoanTapeConfiguration Configuration => new LoanTapeConfiguration
        {
            FeeTransactions = TransactionFilters
        };

        private TransactionInfo[] TransactionFilters
        {
            get
            {
                return new[]
                {
                    new TransactionInfo(){ Code = "140", Name = "Fee (Other)" },
                    new TransactionInfo(){ Code = "141", Name = "Reverse Fee (Other)"},
                    new TransactionInfo(){ Code = "142", Name = "Origination Fee"},
                    new TransactionInfo(){ Code = "143", Name = "Reverse Origination Fee"},
                    new TransactionInfo(){ Code = "150", Name = "Late Fee"},
                    new TransactionInfo(){ Code = "151", Name = "Reverse Late Fee"},
                    new TransactionInfo(){ Code = "180", Name = "ACH Returned Fee"},
                    new TransactionInfo(){ Code = "181", Name = "Reverse ACH Returned Fee"},
                    new TransactionInfo(){ Code = "182", Name = "Check Returned Fee"},
                    new TransactionInfo(){ Code = "183", Name = "Reverse Check Returned Fee"}
                };
            }
        }

        private DateTimeOffset Today
        {
            get { return TenantTime.Today; }
            set { TenantTime.Today = value; }
        }

        private DateTimeOffset Date(int year, int month, int day)
        {
            return TenantTime.Create(year, month, day);
        }

        [Fact]
        public void FundedAmount()
        {
            // arrange
            var fees = new[]
            {
                new Fee { Amount = 60.00 }
            };
            MockLoanDetail.Setup(s => s.LoanInfo.Loan.Terms.Fees).Returns(fees);
            MockLoanDetail.Setup(s => s.LoanInfo.Loan.Terms.LoanAmount).Returns(2000.00);

            // assert            
            Assert.Equal(1940.00, Formula(DateTimeOffset.Now).FundedAmount);
        }

        [Fact]
        public void OriginationFeePercent_FromFee()
        {
            // arrange
            var fees = new[]
            {
                new Fee { Amount = 60.00, Code = "142" }
            };
            MockLoanDetail.Setup(s => s.LoanInfo.Loan.Terms.Fees).Returns(fees);
            MockLoanDetail.Setup(s => s.LoanInfo.Loan.Terms.LoanAmount).Returns(2000.00);

            // assert            
            Assert.Equal(3, Formula(DateTimeOffset.Now).OriginationFeePercent);
        }

        [Fact]
        public void OriginationFeePercent_FromTransaction()
        {
            // arrange
            var transactions = new[]
            {
                new Transaction { Code = "140", Amount = 60.00},
                new Transaction { Code = "141", Amount = 60.00},
                new Transaction { Code = "142", Amount = 60.00},
            };
            MockLoanDetail.Setup(s => s.Transactions).Returns(transactions);
            MockLoanDetail.Setup(s => s.LoanInfo.Loan.Terms.LoanAmount).Returns(2000.00);

            // assert            
            Assert.Equal(3, Formula(DateTimeOffset.Now).OriginationFeePercent);
        }

        [Fact]
        public void OriginationFeePercent_WhenHasUnmatchedFeeCode()
        {
            // arrange
            var fees = new[]
            {
                new Fee { Amount = 60.00, Code = "" }
            };
            MockLoanDetail.Setup(s => s.LoanInfo.Loan.Terms.Fees).Returns(fees);
            MockLoanDetail.Setup(s => s.LoanInfo.Loan.Terms.LoanAmount).Returns(2000.00);

            // assert            
            Assert.Equal(0.0, Formula(DateTimeOffset.Now).OriginationFeePercent);
        }

        [Fact]
        public void OriginationFeeAmount_FromFee()
        {
            // arrange
            var fees = new[]
            {
                new Fee { Amount = 60.00, Code = "142" }
            };
            MockLoanDetail.Setup(s => s.LoanInfo.Loan.Terms.Fees).Returns(fees);
            MockLoanDetail.Setup(s => s.LoanInfo.Loan.Terms.LoanAmount).Returns(2000.00);

            // assert            
            Assert.Equal(1200.00, Formula(DateTimeOffset.Now).OriginationFeeAmount);
        }

        [Fact]
        public void OriginationFeeAmount_FromTransaction()
        {
            // arrange
            var transactions = new[]
           {
                new Transaction { Code = "140", Amount = 100.00},
                new Transaction { Code = "141", Amount = 200.00},
                new Transaction { Code = "142", Amount = 300.00},
            };
            MockLoanDetail.Setup(s => s.Transactions).Returns(transactions);
            MockLoanDetail.Setup(s => s.LoanInfo.Loan.Terms.LoanAmount).Returns(2000.00);

            // assert            
            Assert.Equal(300.00, Formula(DateTimeOffset.Now).OriginationFeeAmount);
        }

        [Fact]
        public void OriginationFeeAmount_WhenFeeCodeIsNot142()
        {
            // arrange
            var fees = new[]
            {
                new Fee { Amount = 60.00, Code = "" }
            };
            MockLoanDetail.Setup(s => s.LoanInfo.Loan.Terms.Fees).Returns(fees);
            MockLoanDetail.Setup(s => s.LoanInfo.Loan.Terms.LoanAmount).Returns(2000.00);

            // assert            
            Assert.Equal(0.0, Formula(DateTimeOffset.Now).OriginationFeeAmount);
        }

        [Fact]
        public void FeesThisMonth()
        {
            // arrange
            Today = Date(2015, 11, 30);
            
            var transactions = new[]
                {
                new Transaction { Code = "140", Amount = 100.00, Date = Today },
                new Transaction { Code = "141", Amount = 200.00, Date = Today },
                new Transaction { Code = "142", Amount = 300.00, Date = Today.AddMonths(-1) },
            };
            MockLoanDetail.Setup(s => s.Transactions).Returns(transactions);

            // assert            
            Assert.Equal(300.00, Formula(Today).FeesThisMonth);
        }

        [Fact]
        public void LastPaymentDueDate()
        {
            // arrange
            Today = Date(2015, 11, 30);

            var installments = new[]
            {
                new Installment { DueDate = Today.AddMonths(-3) },
                new Installment { DueDate = Today.AddMonths(-2) },
                new Installment { DueDate = Today.AddMonths(-1) }
            };
            
            MockLoanDetail.Setup(s => s.PaymentSchedule.Installments).Returns(installments);

            // assert            
            Assert.Equal("2015-10", Formula(Today).LastPaymentDueDate.Value.ToString("yyyy-MM"));
        }

        [Fact]
        public void LastPaymentDueDate_WhenHasNoLastPayment()
        {
            // arrange
            Today = Date(2015, 11, 30);

            var installments = new[]
            {
                new Installment { DueDate = Date(2016, 1, 10) },
                new Installment { DueDate = Date(2016, 2, 10) },
                new Installment { DueDate = Date(2016, 3, 10) }
            };
            
            MockLoanDetail.Setup(s => s.PaymentSchedule.Installments).Returns(installments);

            // assert            
            Assert.Null(Formula(Today).LastPaymentDueDate);
        }
        
        [Fact]
        public void CumulativeInterestPaid()
        {
            // arrange            
            var transactions = new[]
            {
                new Transaction { Code = "124", Amount = 100.00 },
                new Transaction { Code = "124", Amount = 200.00 },
                new Transaction { Code = "124", Amount = 300.00 },
                new Transaction { Code = "140", Amount = 1200.00 },
            };
            MockLoanDetail.Setup(s => s.Transactions).Returns(transactions);

            // assert            
            Assert.Equal(600.00, Formula(DateTimeOffset.Now).CumulativeInterestPaid);
        }

        [Fact]
        public void SchedulePaymentDue()
        {
            // arrange
            Today = Date(2015, 11, 30);
            var installments = new[]
            {
                new Installment { DueDate = Date(2015, 9, 10)  },
                new Installment { DueDate = Date(2015, 10, 10) },
                new Installment { DueDate = Date(2015, 11, 10) }
            };
            
            MockLoanDetail.Setup(s => s.PaymentSchedule.Installments).Returns(installments);

            // assert            
            Assert.Equal(Date(2015, 11, 10), Formula(Today).SchedulePaymentDue);
        }

        [Fact]
        public void NextPaymentDueDate()
        {
            // arrange
            Today = Date(2015, 11, 30);
            var installments = new[]
            {
                new Installment { DueDate = Date(2015, 09, 10) },
                new Installment { DueDate = Date(2015, 10, 10), Status = InstallmentStatus.Completed },
                new Installment { DueDate = Date(2016, 01, 10), Status = InstallmentStatus.Completed }
            };
            
            MockLoanDetail.Setup(s => s.PaymentSchedule.Installments).Returns(installments);

            // assert            
            Assert.Equal(Date(2016, 01, 10), Formula(Today).NextPaymentDueDate);
        }

        [Fact]
        public void NextPaymentDueDate_WhenHasNoInstallment()
        {
            // arrange
            Today = Date(2015, 11, 30);
            var installments = new[]
            {
                new Installment { DueDate = Date(2014, 06, 10) },
                new Installment { DueDate = Date(2014, 07, 10) },
                new Installment { DueDate = Date(2014, 08, 10) }
            };
            
            MockLoanDetail.Setup(s => s.PaymentSchedule.Installments).Returns(installments);

            // assert            
            Assert.Null(Formula(Today).NextPaymentDueDate);
        }

        [Fact]
        public void LastPaymentReceivedDate()
        {
            // arrange
            Today = Date(2015, 11, 30);
            var installments = new[]
            {
                new Installment { PaymentDate = Date(2015, 09, 10), Status = InstallmentStatus.Completed },
                new Installment { PaymentDate = Date(2015, 10, 10), Status = InstallmentStatus.Completed },
                new Installment { PaymentDate = Date(2015, 11, 10), Status = InstallmentStatus.Completed },
                new Installment { PaymentDate = Date(2015, 12, 10), Status = InstallmentStatus.Scheduled },
            };
            
            MockLoanDetail.Setup(s => s.PaymentSchedule.Installments).Returns(installments);

            // assert            
            Assert.Equal(Date(2015, 11, 10), Formula(Today).LastPaymentReceivedDate);
        }

        [Fact]
        public void LastPaymentReceivedDate_WhenHasNoPayment()
        {
            // arrange
            Today = Date(2015, 11, 30);
            var installments = new[]
            {
                new Installment { PaymentDate = Date(2015, 09, 10), Status = InstallmentStatus.Scheduled },
                new Installment { PaymentDate = Date(2015, 10, 10), Status = InstallmentStatus.Scheduled },
                new Installment { PaymentDate = Date(2015, 11, 10), Status = InstallmentStatus.Scheduled },
                new Installment { PaymentDate = Date(2015, 12, 10), Status = InstallmentStatus.Scheduled },
            };
            
            MockLoanDetail.Setup(s => s.PaymentSchedule.Installments).Returns(installments);

            // assert            
            Assert.Null(Formula(Today).LastPaymentReceivedDate);
        }

        [Fact]
        public void MonthlyPayment()
        {
            // arrange
            MockLoanDetail.Setup(s => s.LoanInfo.Loan.Terms.PaymentAmount).Returns(80.00);

            // assert            
            Assert.Equal(80.00, Formula(DateTimeOffset.Now).MonthlyPayment);
        }

        [Fact]
        public void MonthOfServicingReport()
        {
            // arrange
            Today = Date(2015, 11, 30);

            // assert            
            Assert.Equal(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Today.ToString("MMM-yy")), Formula(Today).MonthOfServicingReport);
        }

        [Fact]
        public void FeesPaidDuringThePeriod()
        {
            // arrange
            Today = Date(2015, 11, 30);
            var transactions = new[]
            {
                new Transaction { Code = "140", Amount = 100.00, Date = Date(2015, 09, 30) },
                new Transaction { Code = "141", Amount = 200.00, Date = Date(2015, 10, 30) },
                new Transaction { Code = "142", Amount = 300.00, Date = Date(2015, 12, 30) },
            };
            
            MockLoanDetail.Setup(s => s.Transactions).Returns(transactions);

            // assert            
            Assert.Equal(300.00, Formula(Today).FeesPaidDuringThePeriod);
        }

        [Fact]
        public void SchedulePaymentDueInPeriod()
        {
            // arrange
            Today = Date(2015, 11, 30);
            var installments = new[]
            {
                new Installment { DueDate = Date(2015, 09, 10), PaymentAmount = 100.00, Status = InstallmentStatus.Scheduled },
                new Installment { DueDate = Date(2015, 10, 10), PaymentAmount = 200.00, Status = InstallmentStatus.Scheduled },
                new Installment { DueDate = Date(2015, 11, 10), PaymentAmount = 300.00, Status = InstallmentStatus.Scheduled },
                new Installment { DueDate = Date(2015, 12, 10), PaymentAmount = 400.00, Status = InstallmentStatus.Completed },
            };
            
            MockLoanDetail.Setup(s => s.PaymentSchedule.Installments).Returns(installments);

            // assert            
            Assert.Equal(600.00, Formula(Today).SchedulePaymentDueInPeriod);
        }

        [Fact]
        public void DaysDelinquent()
        {
            // arrange
            MockLoanDetail.Setup(s => s.LoanInfo.Summary.DaysPastDue).Returns(10);

            // assert            
            Assert.Equal(10, Formula(DateTimeOffset.Now).DaysDelinquent);
        }
    }
}
