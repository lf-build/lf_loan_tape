﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace LendFoundry.Loans.Tape.Monthly.Tests
{
    public class LoanTapeServiceTest
    {
        private Mock<ILoanTapeRepository> Repository { get; } = new Mock<ILoanTapeRepository>();

        private Mock<ILogger> Logger { get; } = new Mock<ILogger>();

        private ILoanTapeService GetService(ILoanTapeRepository repository, ILogger logger)
        {
            return new LoanTapeService
            (
                Mock.Of<ILoanTapeBuilder>(),
                repository,
                new UtcTenantTime(),
                logger
            );
        }

        [Fact]
        public void GetTape()
        {
            // arrange
            Repository.Setup(s => s.GetByLoan(It.IsAny<string>()))
                .Returns(new List<ILoanTape>().ToArray());

            Logger.Setup(s => s.Info(It.IsAny<string>()));

            // act
            GetService(Repository.Object, Logger.Object)
                .GetByLoan("loan001");

            // assert
            Repository.Verify(v => v.GetByLoan(It.IsAny<string>()));
        }

        [Fact]
        public void GetTape_WhenHasNoLoanReferenceNumber_ReturnArgumentException()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                GetService
                (
                    repository: Mock.Of<ILoanTapeRepository>(),
                    logger: Mock.Of<ILogger>()
                ).GetByLoan(null);
            });
        }

        [Fact]
        public void GetTapeByLoanReferenceNumberAndDate()
        {
            // arrange
            Repository.Setup(s => s.GetByLoan(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<ILoanTape>());

            Logger.Setup(s => s.Info(It.IsAny<string>()));

            // act
            GetService(Repository.Object, Logger.Object)
                .GetByLoan("loan001", "2016-01");

            // assert
            Repository.Verify(v => v.GetByLoan(It.IsAny<string>(), It.IsAny<string>()));
        }

        [Fact]
        public void GetTape_WhenHasNoDate_ReturnArgumentException()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                GetService
                (
                    repository: Mock.Of<ILoanTapeRepository>(),
                    logger: Mock.Of<ILogger>()
                ).GetByLoan("loan001", null);
            });
        }

        [Fact]
        public void GenerateTapeExportFileName_WhenHasValidInfo()
        {
            // arrange
            var repository = Mock.Of<ILoanTapeRepository>();
            var logger = Mock.Of<ILogger>();

            // act
            var service = GetService(Repository.Object, Logger.Object);
            var fileName = service.GenerateFileName("2016-01", "loan-tape");

            // assert
            Assert.NotEqual(string.Empty, fileName);
            Assert.NotEqual("", fileName);
            Assert.Equal("Jan-2016 loan-tape.csv", fileName);
        }

        [Fact]
        public void GenerateTapeExportFileName_WhenHasInvalidInfo()
        {
            // arrange
            var repository = Mock.Of<ILoanTapeRepository>();
            var logger = Mock.Of<ILogger>();

            // act
            var service = GetService(Repository.Object, Logger.Object);
            var fileName = service.GenerateFileName("2016/01", "loan-tape");

            // assert
            Assert.NotEqual(string.Empty, fileName);
            Assert.NotEqual("", fileName);
            Assert.Equal("loan-tape.csv", fileName);
        }
    }
}