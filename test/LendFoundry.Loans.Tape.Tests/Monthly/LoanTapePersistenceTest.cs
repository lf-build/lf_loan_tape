﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Testing.Mongo;
using LendFoundry.Loans.Tape.Monthly.Persistence;
using LendFoundry.Tenant.Client;
using MongoDB.Driver;
using Moq;
using System;
using System.Linq;
using Xunit;

namespace LendFoundry.Loans.Tape.Monthly.Tests
{
    public class LoanTapePersistenceTest : InMemoryObjects
    {
        private string tenantId { get; } = "my-tenant";

        private TenantInfo tenantInfo;

        private Mock<ITenantService> tenantServiceMock;

        public LoanTapePersistenceTest()
        {
            tenantInfo = new TenantInfo() { Id = tenantId };

            var tenantTimeMock = new Mock<ITenantTime>();

            tenantServiceMock = new Mock<ITenantService>();
            tenantServiceMock.Setup(tenantService => tenantService.Current)
                             .Returns(tenantInfo);

            tenantTimeMock.Setup(t => t.Now).Returns(DateTime.Now);            
        }

        private ILoanTapeRepository CreateRepository(IMongoConfiguration config)
        {
            return new LoanTapeRepository
            (
                configuration: config,
                tenantService: tenantServiceMock.Object,
                logger: Mock.Of<ILogger>()
            );
        }

        [MongoFact]
        public void AddOrUpdateTest()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                var repository = CreateRepository(config);
                var loanReferenceNumber = "loan01";
                var view = Tape;
                view.LoanReferenceNumber = loanReferenceNumber;

                // act
                repository.AddOrUpdate(view);

                // assert
                var loans = repository.GetByLoan(view.LoanReferenceNumber).ToList();

                Assert.NotNull(loans);
                Assert.Equal(loans.First().TenantId, tenantId);
                Assert.False(string.IsNullOrWhiteSpace(loans.First().Id));
                Assert.Equal(loans.First().LoanReferenceNumber, loanReferenceNumber);
            });
        }

        [MongoFact]
        public void AddOrUpdate_WithManyTape_Test()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                var repository = CreateRepository(config);
                var loanReferenceNumber = "loan01";
                var view = Tape;
                view.LoanReferenceNumber = loanReferenceNumber;
                repository.AddOrUpdate(view);

                loanReferenceNumber = "loan02";
                view = Tape;
                view.LoanReferenceNumber = loanReferenceNumber;
                repository.AddOrUpdate(view);

                loanReferenceNumber = "loan03";
                view = Tape;
                view.LoanReferenceNumber = loanReferenceNumber;
                repository.AddOrUpdate(view);

                // assert
                var loans = repository.GetByLoan("loan01").ToList();

                Assert.NotNull(loans);
                Assert.Equal(loans.FirstOrDefault().TenantId, tenantId);
                Assert.False(string.IsNullOrWhiteSpace(loans.FirstOrDefault().Id));
                Assert.Equal(loans.FirstOrDefault().LoanReferenceNumber, "loan01");

                loans = repository.GetByLoan("loan02").ToList();

                Assert.NotNull(loans);
                Assert.Equal(loans.FirstOrDefault().TenantId, tenantId);
                Assert.False(string.IsNullOrWhiteSpace(loans.FirstOrDefault().Id));
                Assert.Equal(loans.FirstOrDefault().LoanReferenceNumber, "loan02");

                loans = repository.GetByLoan("loan03").ToList();

                Assert.NotNull(loans);
                Assert.Equal(loans.FirstOrDefault().LoanReferenceNumber, "loan03");
            });
        }

        [MongoFact]
        public void AddOrUpdate_MoreThanOneForSameLoan_Test()
        {
            // More than one month for the same loan
            MongoTest.Run((config) =>
            {
                // arrange
                var repository = CreateRepository(config);
                var loanReferenceNumber = "loan01";
                var view = Tape;

                view.LoanReferenceNumber = loanReferenceNumber;
                view.CreationDate = new TimeBucket(DateTime.Now.AddMonths(1));
                view.BorrowerFirstName = view.CreationDate.Month;
                repository.AddOrUpdate(view);

                view = Tape;
                view.LoanReferenceNumber = loanReferenceNumber;
                view.CreationDate = new TimeBucket(DateTime.Now.AddMonths(2));
                view.BorrowerFirstName = view.CreationDate.Month;
                repository.AddOrUpdate(view);

                view = Tape;
                view.LoanReferenceNumber = loanReferenceNumber;
                view.CreationDate = new TimeBucket(DateTime.Now.AddMonths(3));
                view.BorrowerFirstName = view.CreationDate.Month;
                repository.AddOrUpdate(view);

                // assert
                var loans = repository.GetByLoan("loan01").ToList();

                Assert.NotNull(loans);
                Assert.Equal(loans.Count, 3);
                Assert.Equal(loans.FirstOrDefault().LoanReferenceNumber, loanReferenceNumber);
            });
        }

        [MongoFact]
        public void AddOrUpdate_MoreThanOneForSameLoan_WithUpdate_Test()
        {
            // More than one month for the same loan
            MongoTest.Run((config) =>
            {
                // arrange
                var repository = CreateRepository(config);
                var loanReferenceNumber = "loan01";
                var view = Tape;

                view.LoanReferenceNumber = loanReferenceNumber;
                view.CreationDate = new TimeBucket(DateTime.Now.AddMonths(1));
                view.BorrowerFirstName = view.CreationDate.Month;
                repository.AddOrUpdate(view);

                var creationDate = new TimeBucket(DateTime.Now.AddMonths(2));

                view = Tape;
                view.LoanReferenceNumber = loanReferenceNumber;
                view.CreationDate = creationDate;
                view.BorrowerFirstName = view.CreationDate.Month;
                repository.AddOrUpdate(view);
                

                view = Tape;
                view.LoanReferenceNumber = loanReferenceNumber;
                view.CreationDate = new TimeBucket(DateTime.Now.AddMonths(3));
                view.BorrowerFirstName = view.CreationDate.Month;
                repository.AddOrUpdate(view);

                // assert
                var loans = repository.GetByLoan(loanReferenceNumber).ToList();

                Assert.NotNull(loans);
                Assert.Equal(loans.Count, 3);
                Assert.Equal(loans.FirstOrDefault().LoanReferenceNumber, "loan01");

                // update
                view = Tape;
                view.LoanReferenceNumber = loanReferenceNumber;
                view.CreationDate = creationDate;
                view.BorrowerFirstName = view.CreationDate.Month;
                view.BorrowerLastName = "updated";
                repository.AddOrUpdate(view);

                loans = repository.GetByLoan(loanReferenceNumber).ToList();

                Assert.NotNull(loans);
                Assert.Equal(loans.Count, 3);
                Assert.True(loans.Any(t => t.BorrowerLastName == "updated"));
            });
        }

        [MongoFact]
        public void WhenAddOrUpdateTape_HasNoTape_ReturnArgumentException()
        {
            MongoTest.Run((config) => 
            {
                var repository = CreateRepository(config);
                Assert.Throws<ArgumentNullException>(() =>
                    repository.AddOrUpdate(null)
                );
            });
        }

        [MongoFact]
        public void WhenAddOrUpdateTape_HasNoLoanReferenceNumber_ReturnArgumentException()
        {
            MongoTest.Run((config) =>
            {
                var repository = CreateRepository(config);
                Assert.Throws<ArgumentException>(() =>
                    repository.AddOrUpdate(new LoanTape
                    {
                        LoanReferenceNumber = string.Empty
                    })
                );
            });
        }

        [MongoFact]
        public void GetByLoanTest()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                var repository = CreateRepository(config);
                var loanReferenceNumber = "loan01";
                var view = Tape;
                view.LoanReferenceNumber = loanReferenceNumber;
                repository.AddOrUpdate(view);

                // loan002
                var loan002 = "loan02";
                view.LoanReferenceNumber = loan002;
                repository.AddOrUpdate(view);

                // assert
                var loans = repository.GetByLoan(loan002).ToList();

                Assert.NotNull(loans);
                Assert.Equal(loans.First().TenantId, tenantId);
                Assert.False(string.IsNullOrWhiteSpace(loans.First().Id));
                Assert.Equal(loans.First().LoanReferenceNumber, loan002);
            });
        }

        [MongoFact]
        public void WhenGetByLoan_HasNoLoanReferenceNumber_ReturnArgumentException()
        {
            MongoTest.Run((config) =>
            {
                var repository = CreateRepository(config);
                Assert.Throws<ArgumentNullException>(() =>
                    repository.GetByLoan(null)
                );
            });
        }

        [MongoFact]
        public void GetByLoanAndDate_Test()
        {
            // More than one month for the same loan
            MongoTest.Run((config) =>
            {
                // arrange
                var repository = CreateRepository(config);
                var loanReferenceNumber = "loan01";
                var yearAndMonth = new TimeBucket(DateTime.Now.AddMonths(2)).Month.Replace("-m", "");
                var view = Tape;

                view.LoanReferenceNumber = loanReferenceNumber;
                view.CreationDate = new TimeBucket(DateTime.Now.AddMonths(1));
                view.BorrowerFirstName = view.CreationDate.Month;
                repository.AddOrUpdate(view);


                view = Tape;
                view.LoanReferenceNumber = loanReferenceNumber;
                view.CreationDate = new TimeBucket(DateTime.Now.AddMonths(2));
                view.BorrowerFirstName = view.CreationDate.Month;
                view.BorrowerLastName = "searched";
                repository.AddOrUpdate(view);

                // assert
                var loan = repository.GetByLoan(loanReferenceNumber, yearAndMonth);

                Assert.NotNull(loan);
                Assert.Equal(loan.LoanReferenceNumber, loanReferenceNumber);
                Assert.Equal("searched", loan.BorrowerLastName);
            });
        }

        [MongoFact]
        public void WhenGetByLoanAndDate_HasNoDate_ReturnArgumentException()
        {
            MongoTest.Run((config) =>
            {
                var repository = CreateRepository(config);
                Assert.Throws<ArgumentNullException>(() =>
                    repository.GetByLoan("loan01", null)
                );
            });
        }
    }
}