﻿//using LendFoundry.Foundation.Logging;
//using LendFoundry.Foundation.Services;
//using LendFoundry.Loans.Tape.Api.Controllers;
//using LendFoundry.Loans.Tape.Helper;
//using LendFoundry.Security.Tokens;
//using Microsoft.AspNet.Mvc;
//using Moq;
//using System;
//using System.Collections.Generic;
//using Xunit;

//namespace LendFoundry.Loans.Tape.Monthly.Tests
//{
//    public class LoanTapeControllerTest : InMemoryObjects
//    {
//        Mock<ILogger> Logger;
//        Mock<ITapeService> TapeService;
//        Mock<ILoanTapeFormula> TapeFormula;
//        Mock<ITokenReader> TokenReader;

//        public LoanTapeControllerTest()
//        {
//            Logger = new Mock<ILogger>();
//            TapeService = new Mock<ITapeService>();
//            TapeFormula = new Mock<ILoanTapeFormula>();
//        }

//        public LoanTapeController CreateController { get { return new LoanTapeController(Logger.Object, TapeService.Object, , TokenReader); } }

//        [Fact]
//        public void WhenInitController_HasNoService_ReturnException()
//        {
//            Assert.Throws<ArgumentNullException>(() =>
//            {
//                new LoanTapeController(null, null);
//            });
//        }

//        [Fact]
//        public void GetTape_HasLoanReferenceNumber_ReturnITapeList()
//        {
//            //TapeService.Setup(s => s.GetByLoan(It.IsAny<string>())).Returns(new List<ITape>().ToArray());

//            //var ctrl = CreateController;
//            //var result = (ObjectResult)ctrl.GetLoanTape_OLD("123");

//            //Assert.Equal(200, result.StatusCode);
//            //TapeService.Verify(v => v.GetByLoan(It.IsAny<string>()), Times.Once);
//        }

//        [Fact]
//        public void WhenGetTape_HasNoLoanReferenceNumber_ReturnBadRequest()
//        {
//            //TapeService.Setup(s => s.GetByLoan(It.IsAny<string>()))
//            //        .Throws(new ArgumentNullException("#referenceNumber cannot be null"));

//            //var ctrl = CreateController;
//            //var result = (ErrorResult)ctrl.GetLoanTape_OLD(string.Empty);

//            //Assert.Equal(400, result.StatusCode);
//        }

//        [Fact]
//        public void GetInvestorTape_HasLoanReferenceNumber_ReturnIInvestorTapeList()
//        {
//            //TapeService.Setup(s => s.GetByLoan(It.IsAny<string>())).Returns(new List<ITape>().ToArray());

//            //var ctrl = CreateController;
//            //var result = (ObjectResult)ctrl.GetInvestorTape_OLD("456");

//            //Assert.Equal(200, result.StatusCode);
//            //TapeService.Verify(v => v.GetByLoan(It.IsAny<string>()), Times.Once);
//        }

//        [Fact]
//        public void WhenGetInvestorTape_HasNoLoanReferenceNumber_ReturnBadRequest()
//        {
//            //TapeService.Setup(s => s.GetByLoan(It.IsAny<string>()))
//            //        .Throws(new ArgumentNullException("#referenceNumber cannot be null"));

//            //var ctrl = CreateController;
//            //var result = (ErrorResult)ctrl.GetInvestorTape_OLD(string.Empty);

//            //Assert.Equal(400, result.StatusCode);
//        }

//        [Fact]
//        public void GetTape_HasLoanReferenceNumberAndDate_ReturnITapeList()
//        {
//            //TapeService.Setup(s => s.GetByLoan(It.IsAny<string>(), It.IsAny<string>()))
//            //    .Returns(new List<ITape>().ToArray());

//            //var ctrl = CreateController;
//            //var result = (ObjectResult)ctrl.GetLoanTape_OLD("123", "2016-01");

//            //Assert.Equal(200, result.StatusCode);
//            //TapeService.Verify(v => v.GetByLoan(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
//        }

//        [Fact]
//        public void WhenGetTape_HasNoDate_ReturnBadRequest()
//        {
//            //TapeService.Setup(s => s.GetByLoan(It.IsAny<string>(), It.IsAny<string>()))
//            //        .Throws(new ArgumentNullException("#yearAndMonth cannot be null"));

//            //var ctrl = CreateController;
//            //var result = (ErrorResult)ctrl.GetLoanTape_OLD("loan001", string.Empty);

//            //Assert.Equal(400, result.StatusCode);
//        }

//        [Fact]
//        public void WhenGetTape_DateFormatInvalid_ReturnBadRequest()
//        {
//            //TapeService.Setup(s => s.GetByLoan(It.IsAny<string>(), It.IsAny<string>()))
//            //    .Returns(new List<ITape>().ToArray());

//            //var ctrl = CreateController;
//            //var result = (ErrorResult)ctrl.GetLoanTape_OLD("loan001", "2016+01");

//            //Assert.Equal(400, result.StatusCode);
//        }
//    }
//}