﻿using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.Loans.Tape.Monthly.Tests
{
    public abstract class InMemoryObjects
    {
        public ILoanTape Tape
        {
            get
            {
                return new LoanTape
                {
                    EntityName = "EF CL 2015-RKT",
                    LoanReferenceNumber = "",

                    CreationDate = new TimeBucket(DateTimeOffset.Now),

                    OriginationDate = DateTimeOffset.Now,
                    FundingDate = DateTimeOffset.Now,
                    PurchaseDate = DateTimeOffset.Now,
                    MaturityDate = DateTimeOffset.Now,
                    BorrowerLastName = "BorrowerLastName",
                    BorrowerFirstName = "BorrowerFirstName",
                    State = "State",
                    ZipCode = "ZipCode",
                    OriginationFicoScore = "OriginationFicoScore",
                    UpdatedFicoScore = "UpdatedFicoScore",
                    OriginationFeePercent = double.MinValue,
                    OriginationFeeAmount = double.MinValue,
                    LoanAmount = 10.0,
                    FundedAmount = 10.0,
                    InterestRate = 10.0,
                    Term = 2,
                    MonthlyPayment = double.MinValue,
                    RiskGrade = "RiskGrade",
                    DTI = 10.0,
                    MonthlyIncome = 10.0,
                    LoanPurpose = "LoanPurpose",
                    HomeOwnership = "HomeOwnership",
                    CampaignCode = "CampaignCode",
                    Principal = 10.0,
                    Interest = 10.0,
                    FeesThisMonth = double.MinValue,
                    SchedulePaymentDue = DateTimeOffset.Now,
                    LastPaymentReceivedDate = DateTimeOffset.Now,
                    LoanStatus = "100.03",
                    DaysPastDue = 2,
                    LoanAge = int.MinValue,
                    PrincipalRecovery = 0,
                    LastPaymentDueDate = DateTimeOffset.Now,
                    NextPaymentDueDate = DateTimeOffset.Now,
                    IsLoanModified = null,
                    ServicingFeeAccrued = null,
                    FraudAmount = null,
                    PrincipalAdjustment = null,
                    InterestAdjustment = null,
                    LateFeeAssessed = 0.0,
                    LateFeePaid = 0.0,
                    NSFFeeAssessed = 0.0,
                    NSFFeePaid = 0.0,
                    PastDueAmount = 10.0,
                    AverageDailyBalance = 10.0,
                    SimpleInterestAccrued = double.MinValue,
                    SimpleInterestAccruedLastMonth = double.MinValue,
                    CumulativeInterestPaid = double.MinValue
                };
            }
        }

        public ILoanDetails GenerateLoanDetails(string _loanReferenceNumber)
        {
            return new LoanDetails
            {
                LoanInfo = new Client.Adapters.LoanInfoAdapter(new Client.Models.LoanInfo
                {
                    ReferenceNumber = _loanReferenceNumber,
                    FundingSource = "FundingSource",
                    Purpose = "Purpose",
                    Investor = new LoanInvestor
                    {
                        Id = "1001",
                        LoanPurchaseDate = DateTimeOffset.Now
                    },
                    Scores = new[]
                    {
                        new Score
                        {
                            Name = "FICO",
                            InitialDate = DateTimeOffset.Now,
                            InitialScore = "",
                            UpdatedDate = DateTimeOffset.Now,
                            UpdatedScore = ""
                        }
                    },
                    Grade = "Grade",
                    PreCloseDti = 100,
                    PostCloseDti = 100,
                    MonthlyIncome = 100,
                    HomeOwnership = "HomeOwnership",
                    CampaignCode = "1",
                    Borrowers = new[]
                    {
                        new Client.Models.Borrower
                        {
                            ReferenceNumber = _loanReferenceNumber,
                            IsPrimary = true,
                            FirstName = "John",
                            MiddleInitial = "Z",
                            LastName = "Doe",
                            DateOfBirth = new DateTimeOffset(new DateTime(1977, 7, 20)),
                            SSN = "721-07-4426",
                            Email = "john@gmail.com",
                            Addresses = new[]
                            {
                                new Client.Models.Address
                                {
                                    IsPrimary = true,
                                    Line1 = "196 25th Avenue",
                                    City = "San Francisco",
                                    State = "CA",
                                    ZipCode = "94121"
                                }
                            },
                            Phones = new[]
                            {
                                new Client.Models.Phone
                                {
                                    Type = PhoneType.Home,
                                    IsPrimary = true,
                                    Number = "65202108"
                                }
                            }
                        }
                    },
                    Terms = new Client.Models.LoanTerms()
                    {
                        ApplicationDate = DateTimeOffset.Now,
                        APR = 100,
                        FundedDate = DateTimeOffset.Now,
                        LoanAmount = 99,
                        OriginationDate = DateTimeOffset.Now,
                        Term = 1,
                        Rate = 13.72,
                        RateType = RateType.Percent,
                        Fees = new[]
                        {
                            new Client.Models.Fee
                            {
                                Amount = 100.00,
                                Code = "Code",
                                Type = FeeType.Fixed
                            }
                        }
                    },
                    BankAccounts = new[] {
                        new Client.Models.BankAccount
                        {
                            AccountNumber = "100",
                            AccountType = BankAccountType.Checking,
                            RoutingNumber = "100"
                        }
                    },
                    PaymentMethod = PaymentMethod.ACH,
                }),
                Transactions = new[]
                {
                    new Transaction { }
                },
                PaymentSchedule = new PaymentSchedule
                {
                    LoanReferenceNumber = _loanReferenceNumber,
                    Installments = new[]
                    {
                        new Installment { },
                    }
                },
            };
        }
    }
}