FROM registry.lendfoundry.com/base:beta8

ADD ./src/LendFoundry.Loans.Tape.Abstractions /app/LendFoundry.Loans.Tape.Abstractions
WORKDIR /app/LendFoundry.Loans.Tape.Abstractions
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Tape.Persistence /app/LendFoundry.Loans.Tape.Persistence
WORKDIR /app/LendFoundry.Loans.Tape.Persistence
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Tape /app/LendFoundry.Loans.Tape
WORKDIR /app/LendFoundry.Loans.Tape
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Tape.Upload /app/LendFoundry.Loans.Tape.Upload
WORKDIR /app/LendFoundry.Loans.Tape.Upload
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Tape.Api /app/LendFoundry.Loans.Tape.Api
WORKDIR /app/LendFoundry.Loans.Tape.Api
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel